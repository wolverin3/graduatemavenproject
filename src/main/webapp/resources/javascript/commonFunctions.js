/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Generic Ajax function.
 * Perform an Ajax request using the designated fetchMethod,
 * at the serverURL,
 * passing the requestParams
 * 
 * @param fetchMethod
 * @param serverURL
 * @param requestParams
 * @returns
 */
function requestForData(fetchMethod, serverURL, requestParams, isFormSubmit) {
    // Create a Deferred
    var deferred = $.Deferred();

    $.ajax({
        type: fetchMethod,
        url: serverURL,
        data: isFormSubmit ? requestParams : JSON.stringify(requestParams),
        contentType: isFormSubmit ? 'application/x-www-form-urlencoded; charset=UTF-8' : 'application/json',
        success: function (data) {
            // this callback will be called asynchronously
            // when the response is available
            deferred.resolve(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            deferred.reject("An error occured while fetching items")
        }
    });

    //Returning the promise object
    return deferred.promise();
}

/**
 * Generic Ajax function.
 * Perform an Ajax file upload request using the designated fetchMethod,
 * at the serverURL.
 * 
 * @param {String} fetchMethod
 * @param {String} serverURL
 * @param {FormData} filesData
 * @returns {unresolved}
 */
function uploadFiles(fetchMethod, serverURL, filesData) {
    // Create a Deferred
    var deferred = $.Deferred();

    $.ajax({dataType : 'json',
        url : serverURL,
        data : filesData,
        type : fetchMethod,
        enctype: 'multipart/form-data',
        processData: false, 
        contentType:false,
        success: function (data) {
            // this callback will be called asynchronously
            // when the response is available
            deferred.resolve(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            deferred.reject("An error occured while fetching items")
        }
    });

    //Returning the promise object
    return deferred.promise();
}

/**
 * Makes use of jquery.fileDownload.js plugin
 * Internally creates an iframe and submits the form inside the iframe
 * The expected response of the server contains the file 
 * along with a cookie "fileDownload=true" in order to mark the success of the download
 * In case of an error the server responds with an empty page which contains only a json
 * with the exception message. This message is displayed to the user and the control is 
 * passed to the callee function.
 * 
 * @param fetchMethod
 * @param serverURL
 * @param requestParams
 */
function downloadFile(fetchMethod, serverURL, requestParams ){
	var deferred = $.Deferred();
	$.fileDownload(serverURL, {
        httpMethod: fetchMethod,
        data: requestParams,
        successCallback: function (url) {
        	deferred.resolve();
        },
        failCallback: function (html, url) {
        	var parsedContent;
        	try {
        		// in case of an error, display the error message to the user
        		parsedContent = JSON.parse(html);
            	if(parsedContent.success == false && parsedContent.data) {
            		messageDisplay[parsedContent.message](parsedContent.data.translate());
            	}
			} catch (e) {
				parsedContent = html;
			}
        	
        	deferred.reject(parsedContent);
        }
    });
    
	return deferred.promise();
	
}

//format dateboxes values to dd/mm/yyyy
$.fn.datebox.defaults.formatter = function (date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return (d < 10 ? '0' + d : d) + '/' + (m < 10 ? '0' + m : m) + '/' + y;
};

$.fn.datebox.defaults.parser = function (s) {
    if (s) {
        var a = s.split('/');
        var d = new Number(a[0]);
        var m = new Number(a[1]);
        var y = new Number(a[2]);
        var dd = new Date(y, m - 1, d);
        return dd;
    } else {
        return new Date();
    }
};

/**
 * Scroll page to the specified element
 *
 * @param {} element
 */
function goToByScroll(element) {

    // Scroll
    $('html,body').animate({
        scrollTop: element.offset().top - 10},
    'slow');
}


// End =============================== Message and mask Functions =================================

// Start ==================================== Value Check Functions ===============================
/**
 * Checks if the incoming parameter is an integer number
 *
 * @param variable
 */
function checkIsInteger(variable) {
    return /^\+?(0|[1-9]\d*)$/.test(variable);
}

/**
 * Checks if the incoming parameter is a decimal number
 *
 * @param variable
 */
function checkIsDecimal(variable) {
    return /^[-+]?([0-9]*\.[0-9]+|[0-9]+)$/.test(variable);
}

/**
 * Checks if the incoming parameter is a currency number
 *
 * @param variable
 */
function checkIsCurrency(variable) {
    return /^[0-9]+(\.[0-9][0-9]?)?$/.test(variable);
}

/**
 * Checks if the incoming parameter is empty
 *
 * @param variable
 */
function checkIsEmpty(variable) {
    if (variable === "") {
        return true;
    }
    return false;
}

/**
* Monitor keypresses on every input field with class intField.
* - removes non numeric characters
*/
$(document).on( "keypress", ".intField", function(event) {
    var code = event.charCode || event.keyCode;
    if (event.keyCode == 8) {
        return true;
    } else {
        return /\d/.test(String.fromCharCode(code));
    }
});
