/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$('#areaToDisplayMessages').hide();
$(function () {
    //add new user into the system
    $("#submitNewUser").click(function () {
        
        $("#areaToDisplayMessages").hide();
        
        //get forms data
        var formData = $("#addUserForm").serializeFormJSON();
        //then make the post request back on controller
        requestForData("POST", "/winr/admin/addUser.action", formData, true).then(
                function (response) {
                    var res = JSON.parse(response);
                    if (res.success) {
                        
                        $("#resetFormBtn").click();
                        
                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("success");
                        $('#areaToDisplayMessages').html(res.data);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                    } else {
                        var i;
                        var messages = '';
                        for (i = 0; i < res.data.length; i++) {
                            messages = messages + res.data[i] + '<br>';
                        }
                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("error");
                        $('#areaToDisplayMessages').html(messages);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                    }
                }, function (reject) {
            console.log(reject);
            $('#areaToDisplayMessages').addClass("warning");
            $('#areaToDisplayMessages').html("A server error occured, please try again later.");
            $('#areaToDisplayMessages').show();
            goToByScroll($("#areaToDisplayMessages"));
        });
    })
});
