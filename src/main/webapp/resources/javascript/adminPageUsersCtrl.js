/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function intializeAdminUsersPage(contextPath) {
    
    $('#userConfDisplayError').hide();
    
    //initialize context path
    adminUsersPageCtrlr.contextPath = contextPath;
    
    //populate data-grid
    adminUsersPageCtrlr.initlializeUserDataGrid();
    
    //reset user udate data button
    $("#resetConfUserForm").click(function() {
        $('.userConfigForm')[0].reset();
    });
    
    $(".userUpdateTriggerForm").click(function() {
        $("#userConfigModal").leanModal();
    });
    
    $(".userUpdateTriggerForm").leanModal({ closeButton: "#closeUserForm" }); 
    
    $("#submitUserConfigureForm").click(function() {
        var form = $(".userConfigForm");
        var data = form.serializeFormJSON();
        
        $.ajax({
            type: "POST",
            url : adminUsersPageCtrlr.contextPath + "/admin/configureUser.action",
            data : data,
            success : function(response) {
                var res = JSON.parse(response);
                if(res.success) {
                    //hide any error message
                    $('#userConfDisplayError').hide();
                    //reset the form
                    $('.userConfigForm')[0].reset();
                    //and close it
                    $("#closeUserForm").click();
                    alert (data.selectedUser + " data, updated successfuly!")
                } else if (!res.success && res.message === 'error') {
                    //define the variables
                    var messages = '', i;
                    //pass the error messages in messages variable as a string str1 <br> str2 <br> ...
                    for(i = 0; i < res.data.length; i++) {
                        messages = messages + res.data[i] + '<br>';
                    }
                    //set the error messages
                    $('#userConfDisplayError').html(messages);
                    //display the error box
                    $('#userConfDisplayError').show();  
                } else if (!res.success && res.message === 'warning') {
                    //set the error messages
                    $('#userConfDisplayError').html('An error ocured');
                    //display the error box
                    $('#userConfDisplayError').show();  
                } else {
                    
                }
             },
            error : function() {
                console.log("Error");
            }
        }); 
    });
    
    //search user
    $("#searchUserInAdminForm").click(function() {
        var searchUserFormData = $(".searchUserForm").serializeFormJSON();
        searchUserFormData.search = true;
        $("#usersDataGrid").datagrid({
            queryParams: searchUserFormData,
            scrollbarSize: 0
        });
    })
    
}

var adminUsersPageCtrlr = {
    
    selectedUsersDataGridRows : [],
    
    contextPath: "",
    
    initlializeUserDataGrid: function() {
        var dataGrid = $("#usersDataGrid");
        var url = this.contextPath + "/admin/getUsersInfos.action";

        dataGrid.datagrid({
            url:url,
            scrollbarSize:0,
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            singleSelect : true,
            striped:true,
            pagination : true,
            pageSize: 10,
            loadFilter: function(data) {
              var i = 0;
              for (i = 0; i < data.rows.length; i++) {
                  if (data.rows[i].role === "ROLE_ADMIN") {
                      data.rows[i].role = "Administrator";
                  } else {
                      data.rows[i].role = "User";
                  }

                  if (data.rows[i].enabled === "true") {
                      data.rows[i].enabled = "Enabled";
                  } else {
                      data.rows[i].enabled = "Disabled";
                  }
              }
              return data;
            },
            onBeforeLoad : function(data){

            },
            onLoadSuccess : function(data) {
                dataGrid.show();
            },
            onLoadError : function(result) {
                // empty grid
                dataGrid.datagrid("loadData", {
                    patents: {totalElements : 0, content : []}
                });
                //Remove Refresh button from datagrid pagination 
                $(".pagination-load").closest("td").remove();
            },
            // highlight previously selected row
            rowStyler : function(rowIndex, row) {
                    // do stuff here
            },
            onSelectPage: function(pageNumber, pageSize){
                dataGrid.panel("refresh", url + "page="+pageNumber+pageSize);
            },
            onSelect: function(index,row) {
                adminUsersPageCtrlr.selectedUsersDataGridRows = $('#usersDataGrid').datagrid('getSelections');  // get all selected rows
            }
        });
    },
    
    reloadDataGrid: function(dataGridID) {
        $(dataGridID).datagrid("reload");
    },
    
    enabledLink: function(value,row, index) {
        return "<div class='userStatus' id='userStatusField' onclick=\"adminUsersPageCtrlr.changeUserStatus('" + value + "','" + row.userName + "','" + index + "')\">" + value + "</div>";
    },
    
    deleteUserFormatter: function(value,row, index) {
        return "<span class='deleteUser' onclick=\"adminUsersPageCtrlr.deleteUser('" + row.userName + "','" + index + "')\"><img src='resources/img/delete_user.png' alt='Delete user' /></span>";
    },
    
    configureUserFormatter: function(value,row, index) {
        return "<span onclick=\"adminUsersPageCtrlr.fireUpModalWindow('" + row.userName + "')\" ><img src='resources/img/pencil.png' id='configureUser'alt='Configure user'/></span>";
    },
    
    changeUserStatus: function(value, username, rowNum) {
    
        //display the pop up window
        var answer = confirm ("You are going to change a user's state, are you sure?")

        if (answer) {
            //do the post request
            var jsonResponseData = {username:username, status:value};
            
            requestForData("POST", this.contextPath + "/admin/changeUserStatus.action", jsonResponseData, true).then(
                    function(response) {
                    var res = JSON.parse(response);
                    if(res.success) {
                        var row = $("#usersDataGrid").datagrid("getRows")[rowNum];
                        row.enabled = res.data;
                       $("#usersDataGrid").datagrid("updateRow", {index:rowNum, row:row});
                    }
                 },function(reject) {
                    console.log(reject);
                }
            );
        }
    },
    
    deleteUser: function(username, index) {
    
        var answer = confirm ("You are about to delete a user, are you sure?");

        if (answer) {
            //make the appropriate transformation
            var deleteUserRequest = {username: username};

            //ajax request
            
            requestForData("POST", this.contextPath + "/admin/deleteUser.action", deleteUserRequest, true).then(
                    function(response) {
                    console.log(response);
                    var res = JSON.parse(response);
                    console.log(res);
                    if(response) {
                        for(var i=adminUsersPageCtrlr.selectedUsersDataGridRows.length-1; i>= 0; i--){
                            var index = $('#usersDataGrid').datagrid('getRowIndex',adminUsersPageCtrlr.selectedUsersDataGridRows.id);  // get the row index
                            $('#usersDataGrid').datagrid('deleteRow',index);
                         }
                        $("#usersDataGrid").datagrid("reload");
                    
                    }
                 },function(reject) {
                    console.log(reject);
                }
            );
        }
    },
    
    fireUpModalWindow: function(username) {
        $("#userConfigModalTrigger").trigger("click");
        $("#hiddenUserNameField").val(username);
    }
    
};
