
var selectedDataGridRows;

var contectPath = "";

function adminMeasurementsPageInitializer(appPath) {
    contectPath = appPath;
    
    //initialize data-grid
    adminMeasurementsPageCntrlr.adminMeasurementsDataGrid();
    
    //search measurement
    $("#searchMeasuresInAdminForm").click(function() {
        var searchMeasuresFormData = $(".searchMeasurementsForm").serializeFormJSON();
        searchMeasuresFormData.search = true;
        $("#viewAllMeasurementsDataGrid").datagrid({
            queryParams: searchMeasuresFormData,
            scrollbarSize: 0
        });
    });
    
}

var adminMeasurementsPageCntrlr = {
    
    adminMeasurementsDataGrid: function() {
        //initialize data-grid
        var dataGrid = $('#viewAllMeasurementsDataGrid');
        var url = contectPath + "/admin/getAllMeasurements.action";
        dataGrid.show();

        dataGrid.datagrid({
            url:url,
            scrollbarSize:0,
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            singleSelect : true,
            striped:true,
            pagination : true,
            total: 2000,
            pageSize: 10,
            onBeforeLoad : function(data){

            },
            onLoadSuccess : function(data) {
                dataGrid.show();
            },
            onLoadError : function(result) {
                // empty grid
                dataGrid.datagrid("loadData", {
                    patents: {totalElements : 0, content : []}
                });
                //Remove Refresh button from datagrid pagination 
                $(".pagination-load").closest("td").remove();
            },
            // highlight previously selected row
            rowStyler : function(rowIndex, row) {
                    // do stuff here
            },
            onSelectPage: function(pageNumber, pageSize){
                dataGrid.panel("refresh", url + "page="+pageNumber+pageSize);
            },
            onSelect: function(index,row) {
                selectedDataGridRows = $('#viewAllMeasurementsDataGrid').datagrid('getSelections');  // get all selected rows
            }
        });
    }, 
    
    reloadDataGrid: function(dataGridID) {
        $(dataGridID).datagrid("reload");
    },
    
    //this display the delete icon under delete user column
    deleteMeasurementFormatter: function(value,row, index) {
        return "<span class='deleteMeasure' onclick=\"adminMeasurementsPageCntrlr.deleteMeasure('" + row.measuremenetID + "','" + index + "')\"><img src='" + contectPath + "/resources/img/delete.png' alt='Delete user' /></span>";
    },
    
    //this function makes an post ajax request to delete a specific measurement
    deleteMeasure: function(measureId, index) {
    
        //confirmation pop-up window
        var answer = confirm ("You are about to delete a measurement, are you sure?");

        if (answer) {
            //make the appropriate transformation
            var selectedMeasurement = {measurementId: measureId};
            requestForData("POST", contectPath + "/admin/deleteMeasurement.action", selectedMeasurement, true).then(
                    function(response){
                        if (response === "true") {
                            for(var i=selectedDataGridRows.length-1; i>= 0; i--){
                                var index = $('#viewAllMeasurementsDataGrid').datagrid('getRowIndex',selectedDataGridRows.id);  // get the row index
                                $('#viewAllMeasurementsDataGrid').datagrid('deleteRow',index);
                             }
                            $("#viewAllMeasurementsDataGrid").datagrid("reload");
                        }
                    }, function(reject) {
                        console.log(reject);
                    });
        }
    }
    
};
