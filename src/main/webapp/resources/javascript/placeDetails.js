

var placeDetailsCtrlr = {
    
    initializer: function(userRights, appContext) {
        
        /************** Variables ************/
        
        var imagesList = new Array();
        
        
        
        /************** Action events ************/
        
        $(".inputField").prop('disabled', !userRights);
        
        //initilize modal
        $('.addNewPlaceImagesTriggerForm').leanModal({closeButton: ".modal_close" });
        
        $("#updateMeasurementPlaceDetails").click(function() {
            
            $('#areaToDisplayMessages').hide();
            
            //get the form data
            var formData = $("#placeDetailsForm").serializeFormJSON();
            
            
            //then make the request back to server
            requestForData("POST", appContext + "/user/measurement/place/" + $("#placeId").val() + "/update.action", formData, true).then(
                function(response) {
                    response = JSON.parse(response);
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass(response.message);
                    $('#areaToDisplayMessages').html(response.data);
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    
                }, function(reject) {
                    console.log(reject);
                });
        });
        
        //when the user click's on Add button on measurement images area then:
        $("#addPlaceImages").click(function(){
            //click on link that triggers the modal
            $("#addNewPlaceImagesModalTrigger").trigger("click");
        });
        
        //images events / load images
        $("#image1").on('change',function(event) {
            prepareLoadImage(event, 1);
        });
        $("#image2").on('change',function() {
            prepareLoadImage(event, 2);
        });
        $("#image3").on('change',function() {
            prepareLoadImage(event, 3);
        });
        $("#image4").on('change',function() {
            prepareLoadImage(event, 4);
        });
        $("#image5").on('change',function() {
            prepareLoadImage(event, 5);
        });
        
        //submit images
        $("#uploadMeasurementImagessBtn").click(function(){
            
            //disable upload button while the action commited
            $('#uploadMeasurementImagessBtn').attr("disabled", true);
        
            $("#areaToDisplayMessages").hide();
            
            if ($("#image1").val() == "" && $("#image2").val() == "" && $("#image3").val() == "" &&
                    $("#image4").val() == "" && $("#image5").val() == "") {
                return true;
            }

            var i = 0;

            var oMyForm = new FormData();

            for (i = 0; i < imagesList.length; i++) {
                oMyForm.append("file" + i, imagesList[i]);
            }
            
            var uploadImagesUrl =  appContext + "/user/measurement/place/" + $("#placeId").val() + "/uploadImages.action";
            uploadFiles("POST", uploadImagesUrl, oMyForm).then(
                function(response) {
                    $(".modal_close").click();
                    //clear all data from the fields
                    $("#image1").val("");
                    $("#image2").val("");
                    $("#image3").val("");
                    $("#image4").val("");
                    $("#image5").val("");
                    
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass(response.message);
                    $('#areaToDisplayMessages').html(response.data);
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    
                    
                    $('#uploadMeasurementImagessBtn').attr("disabled", false);
                    
                    if (response.success) {
                        setTimeout(location.reload(), 1000);
                    }
                    
                }, function(reject) {
                    $(".modal_close").click();
                    setTimeout(function() {
                        //clear all data from the fields
                        $("#image1").val("");
                        $("#image2").val("");
                        $("#image3").val("");
                        $("#image4").val("");
                        $("#image5").val("");
                        
                        $('#uploadMeasurementImagessBtn').attr("disabled", false);
                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("warning");
                        $('#areaToDisplayMessages').html("A server error occured");
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                    }, 800);
                }
            );
            
        });
        
        //when the buton delete in "Delete images" area is being pressed then:
        $("#deleteImages").click(function() {
            
             $("#areaToDisplayMessages").hide();
            //get the selected images values
            var selectedImages = $("#deleteImagesForm").serializeFormJSON().selectedImages;
            console.log(selectedImages.toString());
            
            //and make the request
            requestForData("POST",  appContext + "/user/measurement/place/" + $("#placeId").val() + "/deleteImage.action", {selections:selectedImages.toString()}, true).then(
                function(response) {
                    
                    response = JSON.parse(response);
                    
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass(response.message);
                    $('#areaToDisplayMessages').html(response.data);
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    
                    if (response.success) {
                        setTimeout(location.reload(), 1000);
                    }
                    
                }, function(reject) {
                    console.log(reject);
                    $('#uploadMeasurementImagessBtn').attr("disabled", false);
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("warning");
                    $('#areaToDisplayMessages').html("A server error occured");
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                }
            );
        });
        
        /**
         * When a user click "Return" button to return on "Measurement details" 
         * page, then:
         */
        $("#rtrnBackOnMeasurementBtn").click(function() {
            window.location.href = appContext + "/user/measurement/" + $("#rtrnBackOnMeasurementBtn").attr("measurementId") + ".action";
        });
        
        /************** Functions ************/
        
        /**
         * Load image data in imagesList array
         * @param {type} event
         * @param {type} i
         * @returns {undefined}
         */
        function prepareLoadImage(event, i)
        {
            imagesList[i - 1] = event.target.files[0];
        }
    }
};


