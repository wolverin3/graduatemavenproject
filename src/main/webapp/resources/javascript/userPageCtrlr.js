var selectedDataGridRows;

/*********************User Page****************************/

/**
 * Intitialize user page
 * 
 * @param {type} contextPath
 * @returns {undefined}
 */
function initializeUserMeasurementsPage(contextPath) {
    //initialize user measurements data-grid
    userMeasurementsCtrl.initiateUserMeasurementsDataGrid(contextPath);
    
    //search measurement
    $("#searchMeasuresInUserForm").click(function() {
        var searchMeasuresFormData = $("#searchMeasurements").serializeFormJSON();
    
        searchMeasuresFormData.search = true;
        
        console.log(searchMeasuresFormData);
    
        $("#userMeasurementsDataGrid").datagrid({
            queryParams: searchMeasuresFormData,
            scrollbarSize: 0
        });
    });
}

var userMeasurementsCtrl = {
    
    urlPrefix : "",
    
    initiateUserMeasurementsDataGrid: function(contextPath) {
        
        urlPrefix = contextPath;
        
        //initialize data-grid
        var dataGrid = $('#userMeasurementsDataGrid');
        var url = contextPath + "/user/getUserMeasurements.action";
        dataGrid.show();

        dataGrid.datagrid({
            url:url,
            scrollbarSize:0,
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            singleSelect : true,
            striped:true,
            pagination : true,
            pageSize: 10,
            onBeforeLoad : function(data){

            },
            onLoadSuccess : function(data) {
                dataGrid.show();
            },
            onLoadError : function(result) {
                // empty grid
                dataGrid.datagrid("loadData", {
                    patents: {totalElements : 0, content : []}
                });
                //Remove Refresh button from datagrid pagination 
                $(".pagination-load").closest("td").remove();
            },
            // highlight previously selected row
            rowStyler : function(rowIndex, row) {
                    // do stuff here
            },
            onSelectPage: function(pageNumber, pageSize){
                dataGrid.panel("refresh", url + "page="+pageNumber+pageSize);
            },
            onSelect: function(index,row) {
                selectedDataGridRows = $('#userMeasurementsDataGrid').datagrid('getSelections');  // get all selected rows
            }
        });
    },
    
    reloadDataGrid: function(dataGridID) {
        $(dataGridID).datagrid("reload");
    },
    
    //make the data store under the Nanem column link to measurement data page
    linkToMeasurementDetailsFormatter: function(value, row, index) {
        return "<a href='" + urlPrefix + "/user/measurement/" + row.measuremenetID + "' >" + row.measurementName + "</a>";
    },
    
    deleteMeasurementFormatter: function(value, row, index) {
         if (row.userRole) {
            return "<span class='deleteMeasure' onclick=\"userMeasurementsCtrl.deleteMeasure('" + row.measuremenetID + "','" + index + "')\"><img src='" + urlPrefix + "/resources/img/delete.png' alt='Delete measurement' /></span>";
        } else {
            return "<span class='deleteMeasure'><img src='" + urlPrefix + "/resources/img/not-delete.png' alt='Not authorized to delete measurement' /></span>";
        }
    },
    
    //this function makes an post ajax request to delete a specific measurement
    deleteMeasure: function(measureId, index) {
        //confirmation pop-up window
        var answer = confirm ("You are about to delete a measurement, are you sure?");

        if (answer) {
            //make the appropriate transformation
            var selectedMeasurement = {measurementId: measureId};     
            requestForData("POST", urlPrefix + "/user/deleteMeasurement.action", selectedMeasurement, true).then(
                    function(response){
                        if (response === "true") {
                            for(var i=selectedDataGridRows.length-1; i>= 0; i--){
                                var index = $('#userMeasurementsDataGrid').datagrid('getRowIndex',selectedDataGridRows.id);  // get the row index
                                $('#userMeasurementsDataGrid').datagrid('deleteRow',index);
                             }
                            $("#userMeasurementsDataGrid").datagrid("reload");
                        }
                    }, function(reject) {
                        console.log(reject);
                    });
        }
    }
};

/*********************New measurement page****************************/

/**
 * 
 * @param {type} contextPath
 * @returns {undefined}
 */
function initializeCreateNewMeasurementPage(contextPath) {
    
    //initialize dara-grid
    createNewMeasurementPageCtrl.initNewMeasurementUsersDG(contextPath);
    
    /**
     * Triggred when a user try to create a new measurement
     * @param {type} param
     */
    $("#submitNewMeasurement").click(function() {
        //make message arrea dissapear
        $("#areaToDisplayMessages").hide();
        //get selected users from data-grid
        var selectedUsers = $("#newMeasurementUsers").datagrid("getSelections");

        //get only the username value
        var i = 0, users = [];
        for (i = 0; i < selectedUsers.length; i++) {
            users[i] = selectedUsers[i].userName;
        } 
        $("#usersIDs").val(users);
        //set the array with usernames values on usersIDs of the form
        var newMeasurementFormData = $("#newMeasurementForm").serializeFormJSON();

        //then make the post request back on controller
        requestForData("POST", contextPath + "/user/addProject.action", newMeasurementFormData, true).then(
            function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("success");
                    $('#areaToDisplayMessages').html("New measurement has been created.");
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));

                    setTimeout(function() {
                        document.location.href = response.message;
                    }, 800);

                } else {
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("error");
                    $('#areaToDisplayMessages').html(response.message);
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                }
            }, function(reject) {
                console.log(reject);
            }); 
    });
    
    //when a user clicks on search button:
    $("#searchNewMeasurementUserBtn").click(function() {
        //get search filed input
        var searchData = $("#searchUserForNewMeasurementId").val();

        $("#newMeasurementUsers").datagrid({
            queryParams: {searchData:searchData, search:true},
            scrollbarSize: 0
        });

    });
    
};

var createNewMeasurementPageCtrl = {
    
    initNewMeasurementUsersDG: function(contextPath) {
        var datagrid = $("#newMeasurementUsers");
        var url = contextPath + "/user/findClientsForNewMeasurement.action";

        datagrid.datagrid({
            url:url,
            scrollbarSize:0,
            fitColumns : true,
            multiSelect : true,
            striped:true,
            pagination : true,
            pageSize: 10
        });
    },
    
    newMeasurementsComfiramtionsMessages:function(errorMessage) {
        alert(errorMessage);
    }
};
