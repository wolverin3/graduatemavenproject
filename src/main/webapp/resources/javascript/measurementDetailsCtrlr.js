//////////////////////////////////////
//Measurements Details functions
//////////////////////////////////////

var measurementDetailsCtrlr = {
    firstPopUpLoad: true,
    reloadfirstPop: false,
    loadedFile: "",
    appContext: "",
    initilaMeasurementValues: "",//a value to store initial measurements data (name, location, providers, etc)
    
    initializer: function(appContext) {
        
        measurementDetailsCtrlr.appContext = appContext;
        
        //search measurement
        $("#updateMeasurementDetails").click(function() {
            //make message arrea dissapear
            $("#areaToDisplayMessages").hide();
            //get formData
            var formData = $("#measurementForm").serializeFormJSON();

            //if there are chenges on form data then commit the changes back to server
            if (JSON.stringify(formData) !== JSON.stringify(initilaMeasurementValues)) {
                requestForData("POST", measurementDetailsCtrlr.appContext + "/user/updateMeasurement.action", formData, true).then(
                    function(response) {
                        response = JSON.parse(response);
                        if (response.success) {
                            $("#measurementName").val(response.data.measurementName);
                            $("#city").val(response.data.city);
                            $("#locationName").val(response.data.locationName);
                            $("#coordinatesX").val(response.data.coordinatesX);
                            $("#coordinatesY").val(response.data.coordinatesY);
                            $("#providers").val(response.data.providers);
                            $("#measurementDateCreatedField").datebox('setValue', response.data.measurementDate);

                            $('#areaToDisplayMessages').removeClass();
                            $('#areaToDisplayMessages').addClass(response.message);
                            $('#areaToDisplayMessages').html("Data have updated successfully.");
                            $('#areaToDisplayMessages').show();
                            goToByScroll($("#areaToDisplayMessages"));

                        } else {
                            $('#areaToDisplayMessages').removeClass();
                            $('#areaToDisplayMessages').addClass(response.message);
                            $('#areaToDisplayMessages').html(response.data);
                            $('#areaToDisplayMessages').show();
                            goToByScroll($("#areaToDisplayMessages"));
                        }
                    },function(reject) {
                        $('#areaToDisplayMessages').addClass("warning");
                        $('#areaToDisplayMessages').html("A server error occured, please try again later.");
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                    }
                );
            } else {
                //else display an error message
                $('#areaToDisplayMessages').removeClass();
                $('#areaToDisplayMessages').addClass("error");
                $('#areaToDisplayMessages').html("There are no any changes on measurements data!");
                $('#areaToDisplayMessages').show();
                goToByScroll($("#areaToDisplayMessages"));
            }

        });

        //when the user click's on Add button on measurement users area then:
        $("#addMeasurementUsers").click(function(){

           $("#areaToDisplayMessages").hide();
           $("#searchNotRegisteredUsersId").val("");

           if(measurementDetailsCtrlr.firstPopUpLoad) {
                setTimeout(function(){
                    measurementDetailsCtrlr.loadRestOfUsers();
                }, 1000);
                measurementDetailsCtrlr.firstPopUpLoad = false;
            } else {
                setTimeout(function() {
                    $("#searchAllUsers").click();
                }, 700);
                measurementDetailsCtrlr.reloadfirstPop = false;
            }

             //click on link
           $("#addNewMeasurementUsersModalTrigger").trigger("click");
       }); 

       //when the user click's on Add button on measurement images area then:
       $("#addMeasurementPlace").click(function(){
           //click on link that triggers the modal
           $("#addNewMeasurementPlaceModalTrigger").trigger("click");
       });

       //when the user press the add button in modal window to upload the images then:
       $("#addMeasurementImagessBtn").click(function() {

       });

       //when a user click on the "Search" button of the modal window to search a 
       //specific user to add
       $("#searchAllUsers").click(function(){
           //get input's field value:
           var searchData = $("#searchNotRegisteredUsersId").val();

           //then send the data back with an ajax request and let the system search
           //for the users and return the data
           $('#restOfUsers').datagrid({
               queryParams: {currentMeasurementId: $("#measuremenetID").val(), searchData: searchData}
           });

       });

       //when a user press the add button of modal window, to add users on a measurement
       $("#addMeasurementUsersBtn").click(function() {

           $('#addMeasurementUsersBtn').attr("disabled", true);

           $("#areaToDisplayMessages").hide();
           //get the users selected
           var selectedUsers = $('#restOfUsers').datagrid("getChecked");

           var usersSelect = "", i = 0;

           for (i = 0; i < selectedUsers.length; i++) {
               usersSelect += (i === selectedUsers.length - 1 ? selectedUsers[i].userName : selectedUsers[i].userName + ",");
           }

           //make an ajax request and send them back
           requestForData("POST", measurementDetailsCtrlr.appContext + "/user/updateMeasurementUsers.action", {usersSelect: usersSelect, currentMeasurementId: $("#measuremenetID").val()}, true).then(
                function(response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        $('#addMeasurementUsersBtn').attr("disabled", false);
                        //close modal
                        $(".modal_close").click();
                        //update measurement user data-grid
                        $("#measurementParticipantsDataGrid").datagrid("reload");
                        //update rest of users data-grid
                        $("#restOfUsers").datagrid("reload");
                    } else {
                        $('#addMeasurementUsersBtn').attr("disabled", false);
                        $('#addMeasurementUserDisplayError').removeClass();
                        $('#addMeasurementUserDisplayError').addClass("error");
                        $('#addMeasurementUserDisplayError').html(response.message);
                        $('#addMeasurementUserDisplayError').show();
                    }
                }, function(reject) {
                    $('#addMeasurementUsersBtn').attr("disabled", false);
                    console.log(reject);
                });

       });

       //when a user clicks on  delete button, to delete users from a measurement
       $("#deleteMeasurementUsers").click(function() {

           $('#deleteMeasurementUsers').attr("disabled", true);

           $("#areaToDisplayMessages").hide();
           //get the users selected
           var selectedUsers = $('#measurementParticipantsDataGrid').datagrid("getChecked");

           var usersSelect = "", i = 0;

           for (i = 0; i < selectedUsers.length; i++) {
               usersSelect += (i === selectedUsers.length - 1 ? selectedUsers[i].userName : selectedUsers[i].userName + ",");
           }


           //make an ajax request and send them back
           requestForData("POST", measurementDetailsCtrlr.appContext + "/user/deleteMeasurementUsers.action", {usersSelect: usersSelect, currentMeasurementId: $("#measuremenetID").val()}, true).then(
                function(response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        //update measurement users data-grid
                        $("#measurementParticipantsDataGrid").datagrid("reload");
                        $('#deleteMeasurementUsers').attr("disabled", false);
                        if (response.data) {
                            $("#homeButton")[0].click();
                        }
                    } else {

                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("error");
                        $('#areaToDisplayMessages').html(response.message);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#deleteMeasurementUsers').attr("disabled", false);
                    }
                }, function(reject) {
                    $('#deleteMeasurementUsers').attr("disabled", false);
                    console.log(reject);
                });

       });

        //submit images
        $("#savePLaceBtn").click(function(){

            //disable save button
            $('#savePLaceBtn').attr("disabled", true);

            $("#areaToDisplayMessages").hide();

            //get formData
            var placeData = $("#addPlaces").serializeFormJSON();

            if (placeData.placeName == "") {
                $(".modal_close").click();
                $("#coordinatesX").val("");
                $("#coordinatesY").val("");
                $('#areaToDisplayMessages').removeClass();
                $('#areaToDisplayMessages').addClass("error");
                $('#areaToDisplayMessages').html("Place name is a mandatory field");
                $('#areaToDisplayMessages').show();
                goToByScroll($("#areaToDisplayMessages"));
                $('#savePLaceBtn').attr("disabled", false);
                return;
            }

            if (placeData.coordinatesX != "" && placeData.coordinatesY != "") {
                if (!checkIsDecimal(placeData.coordinatesX) || !checkIsDecimal(placeData.coordinatesY)) {
                    $(".modal_close").click();
                    $("#coordinatesX").val("");
                    $("#coordinatesY").val("");
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("error");
                    $('#areaToDisplayMessages').html("Coordinates fields should be decimals!");
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    $('#savePLaceBtn').attr("disabled", false);
                    return;
                }
            }

            if ((placeData.coordinatesX != "" && placeData.coordinatesY == "") || (placeData.coordinatesX == "" && placeData.coordinatesY != "")) {
                $(".modal_close").click();
                $("#coordinatesX").val("");
                $("#coordinatesY").val("");
                $('#areaToDisplayMessages').removeClass();
                $('#areaToDisplayMessages').addClass("error");
                $('#areaToDisplayMessages').html("Coordinates fields should be both empty or contain coordinates parameters!");
                $('#areaToDisplayMessages').show();
                goToByScroll($("#areaToDisplayMessages"));
                $('#savePLaceBtn').attr("disabled", false);
                return;
            }

            //else make the request
            requestForData("POST", measurementDetailsCtrlr.appContext + "/user/measurement/" + $("#measuremenetID").val() + "/newPlace.action", placeData, true).then(
                function(response) {
                    response = JSON.parse(response);
                    $(".modal_close").click();
                    //update measurement users data-grid
                    $("#measurementPlacesDataGrid").datagrid("reload");

                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass(response.message);
                    $('#areaToDisplayMessages').html(response.data);
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    $('#savePLaceBtn').attr("disabled", false);
                }, function(reject) {
                    console.log(reject);
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("warning");
                    $('#areaToDisplayMessages').html("A server error occured");
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                    $('#savePLaceBtn').attr("disabled", false);
                }
            );


        });

        //when a user clicks on delete buton to delete a place then: 
        $("#deleteMeasurementPlaces").click(function() {

            $('#areaToDisplayMessages').hide();

            $('#deleteMeasurementPlaces').attr("disabled", true);

            //get the places from the data-grid that have been clicked for delete
            var placesToDelete = $("#measurementPlacesDataGrid").datagrid("getChecked");

            if (placesToDelete.length === 0) {
                $('#areaToDisplayMessages').removeClass();
                $('#areaToDisplayMessages').addClass("error");
                $('#areaToDisplayMessages').html("you should shoose at least on place to delete");
                $('#areaToDisplayMessages').show();
                goToByScroll($("#areaToDisplayMessages"));
                $('#deleteMeasurementPlaces').attr("disabled", false);
                return true;
            }

            //confirm mesage
            if (confirm ("Are you sure you want to delete the selected places?")) {

                var selectedPlaces = "";

                var i = 0;

                for (i = 0; i < placesToDelete.length; i++) {
                    selectedPlaces += placesToDelete[i].placeId;

                    if (i < (placesToDelete.length - 1)) {
                        selectedPlaces += ",";
                    }
                }

                requestForData("POST", measurementDetailsCtrlr.appContext + "/user/measurement/deletePlaces.action", {selectedPlaces:selectedPlaces}, true).then(
                    function(response) {
                        response = JSON.parse(response);
                        //update measurement users data-grid
                        $("#measurementPlacesDataGrid").datagrid("reload");

                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass(response.message);
                        $('#areaToDisplayMessages').html(response.data);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#deleteMeasurementPlaces').attr("disabled", false);
                    }, function(reject) {
                        console.log(reject);
                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("warning");
                        $('#areaToDisplayMessages').html("A server error occured");
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#deleteMeasurementPlaces').attr("disabled", false);
                    }
                );

            }
            $('#deleteMeasurementPlaces').attr("disabled", false);

        });

        //add new measurement data (file)
        $("#addMeasurementData").click(function() {

            //trigger link
            $("#addNewMeasurementDataModalTrigger").trigger("click");
        });

        $("#file").on('change',function(event) {
            measurementDetailsCtrlr.prepareLoadFile(event);
        });

        //upload measurement file
        $("#uploadMeasurementDataBtn").click(function() {

            //disable upload button while the action commited
                $('#uploadMeasurementDataBtn').attr("disabled", true);

                $("#areaToDisplayMessages").hide();

                if ($("#file").val() == "") {
                    return true;
                }

                var oMyForm = new FormData();

                oMyForm.append($("#file").val(), measurementDetailsCtrlr.loadedFile);

                var fileType = $("#fileTypeSelection").val();

                var uploadFileUrl = measurementDetailsCtrlr.appContext + "/user/measurement/" + $("#measuremenetID").val() + "/dataUploadFile/" + fileType + ".action";

                uploadFiles("POST", uploadFileUrl, oMyForm).then(
                    function(response) {
                         //update measurement users data-grid
                        //$("#measurementDataDataGrid").datagrid("reload");
                        $(".modal_close").click();

                        $("#measurementDocumentsDataGrid").datagrid("reload");

                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass(response.message);
                        $('#areaToDisplayMessages').html(response.data);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));

                        //clear form
                        $("#clearMeasurementDocsForm").click();

                        $('#uploadMeasurementDataBtn').attr("disabled", false);

                    }, function(reject) {
                        $(".modal_close").click();

                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("warning");
                        $('#areaToDisplayMessages').html("A server error occured");
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#uploadMeasurementDataBtn').attr("disabled", false);
                    }
                );

        });

        //when a user press the delete button on measurement-documents section then:
        $("#deleteMeasurementData").click(function() {

            $('#deleteMeasurementData').attr("disabled", true);

            $('#areaToDisplayMessages').hide();

            //get the places from the data-grid that have been clicked for delete
            var DocumentsToDelete = $("#measurementDocumentsDataGrid").datagrid("getChecked");

            if (DocumentsToDelete.length === 0) {
                $('#areaToDisplayMessages').removeClass();
                $('#areaToDisplayMessages').addClass("error");
                $('#areaToDisplayMessages').html("you should shoose at least on place to delete");
                $('#areaToDisplayMessages').show();
                goToByScroll($("#areaToDisplayMessages"));
                $('#deleteMeasurementData').attr("disabled", false);
                return true;
            }

            if (confirm ("Are you sure you want to delete the selected document(s)?")) {
                var selectedDocs = "";

                var i = 0;

                for (i = 0; i < DocumentsToDelete.length; i++) {
                    selectedDocs += DocumentsToDelete[i].id;

                    if (i < (DocumentsToDelete.length - 1)) {
                        selectedDocs += ",";
                    }
                }

                requestForData("POST", measurementDetailsCtrlr.appContext + "/user/measurement/deleteDocuments.action", {selectedDocs:selectedDocs}, true).then(
                    function(response) {
                        response = JSON.parse(response);
                        //update measurement users data-grid
                        $("#measurementDocumentsDataGrid").datagrid("reload");

                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass(response.message);
                        $('#areaToDisplayMessages').html(response.data);
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#deleteMeasurementData').attr("disabled", false);
                    }, function(reject) {
                        console.log(reject);
                        $('#areaToDisplayMessages').removeClass();
                        $('#areaToDisplayMessages').addClass("warning");
                        $('#areaToDisplayMessages').html("A server error occured");
                        $('#areaToDisplayMessages').show();
                        goToByScroll($("#areaToDisplayMessages"));
                        $('#deleteMeasurementData').attr("disabled", false);
                    }
                );

            } else {
                $('#deleteMeasurementData').attr("disabled", false);
                return;
            }
        });
    },
    
    //set default datebox value on measurements details
    setDateMeasurementMade: function(dateValue) {
        $("#measurementDateCreatedField").datebox('setValue', dateValue);
    },
    //get initial form data in measurement details page
    getInitialMeasurementValues: function() {
        initilaMeasurementValues = $("#measurementForm").serializeFormJSON();
    },
    
    //create and initialize data-grid with measurement users
    participantsInMeasurementDataGrid: function() {
        var participantsDataGrid = $("#measurementParticipantsDataGrid");
        var url = measurementDetailsCtrlr.appContext + "/user/getMeasurementUsers.action";
        var formData = $("#measurementForm").serializeFormJSON();
        participantsDataGrid.show();

        participantsDataGrid.datagrid({
            url:url,
            queryParams: formData,
            scrollbarSize:0,
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            multiSelect : true,
            striped:true,
            pagination : true,
            checkOnSelect: false,
            selectOnCheck: false,
            onBeforeLoad: function(param){
                if (param.page <= 0) {
                    return false;
                }
            },
            onLoadSuccess : function(data) {
                participantsDataGrid.show();
            },
            onLoadError : function(result) {
                // empty grid
                participantsDataGrid.datagrid("loadData", {
                    users: {totalElements : 0, content : []}
                });
                //Remove Refresh button from datagrid pagination 
                $(".pagination-load").closest("td").remove();
            },
            // highlight previously selected row
            rowStyler : function(rowIndex, row) {
                    // do stuff here
            },
            onSelectPage: function(pageNumber, pageSize){
                participantsDataGrid.panel("refresh", url + "page="+pageNumber+pageSize);
            },
            onSelect: function(index,row) {
                selectedDataGridRows = participantsDataGrid.datagrid('getSelections');  // get all selected rows
            }
        })
    },
    
    getRowIndex: function(target){
        var tr = $(target).closest('tr.datagrid-row');
        return parseInt(tr.attr('datagrid-row-index'));
    },
    
    userMeasurementRights: function(value, row, index) {
        return "<a id='measurementUserRightLinkId' onclick='measurementDetailsCtrlr.changeUserMeasurementRights(this)' >" + value + "</a>"
    },
    
    //change user rights on a measurement
    changeUserMeasurementRights: function(target) {
        //display the pop up window
        var answer = confirm ("You are going to change a user's rights, are you sure?");

        if (answer) {
            var rowIndex = this.getRowIndex(target);

            var row = $("#measurementParticipantsDataGrid").datagrid("getRows")[rowIndex];


            //do the post request
            var jsonResponseData = {username:row.userName, currentRights:row.hasRightsOnMeasurement, currentMeasurementId: $("#measuremenetID").val()};

            requestForData("POST", measurementDetailsCtrlr.appContext + "/user/changeUserMeasurementRights.action", jsonResponseData, true).then(
                    function(response) {
                        response = JSON.parse(response);
                        if (response.success) {
                            row.hasRightsOnMeasurement = response.data;
                            $("#measurementParticipantsDataGrid").datagrid("updateRow", {index:rowIndex, row:row});

                            if ($("#currentSystemUser").html() == jsonResponseData.username) {
                                location.reload();
                            }

                        }
                    }, function(reject) {
                        console.log(reject);
                    });
        }
    },
    
    //load the rest of the users who are not bound with the measurement
    loadRestOfUsers: function() {
        //load all users for data-grid on modal window
        var dataGrid = $('#restOfUsers'); //declare data-grid
        var url = measurementDetailsCtrlr.appContext + "/user/getRestOfTheUsers.action"; //declare url to get the data


        //get current measurements id
        var currentMeasurementId = $("#measuremenetID").val();
        //get measurement users data-grid data:
        var listOfMeasurementUsers = $("#measurementParticipantsDataGrid").datagrid('getRows');

        var i = 0, measurementUsers = "";

        for(i = 0; i < listOfMeasurementUsers.length; i++) {
            measurementUsers += (i === listOfMeasurementUsers.length - 1 ? listOfMeasurementUsers[i].userName : listOfMeasurementUsers[i].userName + ",");
        }

        //display datagrid
        dataGrid.show();

        dataGrid.datagrid({
            url:url,
            queryParams: {currentMeasurementId: currentMeasurementId},
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            striped:true,
            pagination : true,
            pageSize: 10

        });
    },
    
    //this display the delete icon under delete user column
    deleteMeasurementParticipantFormatter: function(value, row, index) {
        return "<span class='deleteMeasure' onclick=\"deleteMeasure('" + row.measuremenetID + "','" + index + "')\"><img src='" + measurementDetailsCtrlr.appContext + "/resources/img/delete.png' alt='Delete user' /></span>";
    },
    
    loadMeasurementPlaces: function() {
        //load all users for data-grid on modal window
        var placesDataGrid = $('#measurementPlacesDataGrid'); //declare data-grid
        var url = measurementDetailsCtrlr.appContext + "/user/getMeasurementPlaces.action"; //declare url to get the data


        //get current measurements id
        var currentMeasurementId = $("#measuremenetID").val();

        //display datagrid
        placesDataGrid.show();

        placesDataGrid.datagrid({
            url:url,
            queryParams: {currentMeasurementId: currentMeasurementId},
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            striped:true,
            pagination : true,
            checkOnSelect: false,
            selectOnCheck: false
        });
    },
    
    placeDetailsLink: function (value, row, index) {
        return "<a id='measurementUserRightLinkId' onclick='measurementDetailsCtrlr.gotoPlaceDetails(" + row.placeId + ")' >" + value + "</a>"
    },
    
    mapsIconLink: function(value, row, index) {
        if (row.coordinatesX != 0 && row.coordinatesY != 0) {
            return "<img onclick='measurementDetailsCtrlr.openLocationOnMaps(" + row.coordinatesX + ", " + row.coordinatesY + ")' src='" + measurementDetailsCtrlr.appContext + "/resources/img/map_marker.png' id='mapAnchor'/>";
        } else {
            return "No coordinates have been placed yet";
        }

    },
    
    //when the user click on place name link then:
    gotoPlaceDetails: function(placeData) {
        document.location.href = measurementDetailsCtrlr.appContext + "/user/measurement/" + $("#measuremenetID").val() + "/place/" + placeData + ".action";
    },
    
    //when the user click the maps icon to see where the measurement took place then:
    openLocationOnMaps: function(coordX, coordY) {
        //close message
        $("#areaToDisplayMessages").hide();

        if (coordX !== "0.0" && coordX !== "" && coordY !== "0.0" && coordY !== "") {
            sessionStorage.setItem("coordinatesX", coordX);
            sessionStorage.setItem("coordinatesY", coordY);
            window.open(measurementDetailsCtrlr.appContext + "/resources/html/locationOnMaps.html", "maps", "height=350,width=450");
        } else {
            $("#areaToDisplayMessages").html("<ul><li>No valid coordinates data</li></ul>");
            $("#areaToDisplayMessages").show(true);
            goToByScroll($("#areaToDisplayMessages"));
        }
    },
    
    /**
    * This function, depending on user's authority on measurement make input fields
    * disabled or enabled to modified
    * @param {type} authority
    * @returns {undefined}
    */
   checkIfUserCanEditInputFields: function(authority) {
       if (!authority) {
           $(".inputField").not("button, input[type='hidden']").attr("disabled", true);
       }
   },
   
   prepareLoadFile: function(event)
    {
        measurementDetailsCtrlr.loadedFile = event.target.files[0];
    },
    
    //create and initialize data-grid with measurement documents
    measurementDocumentsDataGrid: function() {
        var documentsDataGrid = $("#measurementDocumentsDataGrid");
        var url = measurementDetailsCtrlr.appContext + "/user/measurement/" + $("#measuremenetID").val() + "/getMeasurementDocs.action";

        documentsDataGrid.show();

        documentsDataGrid.datagrid({
            url:url,
            scrollbarSize:0,
            autoRowHeight:true,
            width:"100%",
            fitColumns : true,
            striped:true,
            pagination : true,
            checkOnSelect: false,
            selectOnCheck: false,
            onBeforeLoad: function(param){
                if (param.page <= 0) {
                    return false;
                }
            },
            onLoadSuccess : function(data) {
                documentsDataGrid.show();
            },
            onLoadError : function(result) {
                // empty grid
                documentsDataGrid.datagrid("loadData", {
                    users: {totalElements : 0, content : []}
                });
                //Remove Refresh button from datagrid pagination 
                $(".pagination-load").closest("td").remove();
            },
            // highlight previously selected row
            rowStyler : function(rowIndex, row) {
                    // do stuff here
            },
            onSelectPage: function(pageNumber, pageSize){
                documentsDataGrid.panel("refresh", url + "page="+pageNumber+pageSize);
            },
            onSelect: function(index,row) {
                selectedDataGridRows = documentsDataGrid.datagrid('getSelections');  // get all selected rows
            }
        });

    },
    
    fileTypeIcon: function(value, row, index) {
        if (value == "pdf") {
            return "<img onclick='measurementDetailsCtrlr.downloadMeasurDocument(" + row.id + ")' src='" + measurementDetailsCtrlr.appContext + "/resources/img/pdf.png' class='cursorPointer'/>";
        } else if (value == "doc") {
            return "<img onclick='measurementDetailsCtrlr.downloadMeasurDocument(" + row.id + ")' src='" + measurementDetailsCtrlr.appContext +  "/resources/img/doc.png' class='cursorPointer'/>";
        } else {
            return "<img onclick='measurementDetailsCtrlr.downloadMeasurDocument(" + row.id + ")' src='" + measurementDetailsCtrlr.appContext +  "/resources/img/xls.png' class='cursorPointer'/>";
        }
    },
    
    downloadMeasurDocument: function(documentId) {
        //make the request to download the appropriate file
        downloadFile("POST", measurementDetailsCtrlr.appContext +  "/user/measurement/downloadDocument/" + documentId + ".action").then(
                function(response) {
                    console.log(1);
                }, function(reject) {
                    $('#areaToDisplayMessages').removeClass();
                    $('#areaToDisplayMessages').addClass("warning");
                    $('#areaToDisplayMessages').html("A server error occured");
                    $('#areaToDisplayMessages').show();
                    goToByScroll($("#areaToDisplayMessages"));
                });
    }
};
