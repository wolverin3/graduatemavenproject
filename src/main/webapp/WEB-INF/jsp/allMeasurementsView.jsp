<%-- 
    Document   : allMeasurementsView
    Created on : Jul 27, 2014, 9:18:21 PM
    Author     : wolverine
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Measurements Details</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/userPage.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/addForm.css">
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        
        
    </head>
    <body>
        <div class="transparentBackground">
            <%@include file="header.jsp" %>
            
            <div class="bodyArea">
            
                <div class="mainbodyHeader">
                    Measurements Details 
                </div>

                <div id="searchMeasurementsForm">
                    <span searchUserHeader>Search Measurement</span>
                    <br>
                    <hr>
                    <form:form modelAttribute="searchMeasurements" class="searchMeasurementsForm">
                         <table style="width:100%">
                            <tr class="searchFormRow">
                                <td>
                                   <label id="searchFormLabel">Measurement name:</label>
                                   <form:input path="measurementName" class="inputField" type="text" value="" />
                                </td>
                                <td>
                                   <label id="searchFormLabel">Location:</label>
                                   <form:input path="locationName" class="inputField" type="text" value="" />
                                </td>
                                <td>
                                   <label id="searchFormLabel">City:</label>
                                   <form:input path="city" class="inputField" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="searchFormLabel">Providers:</label>
                                    <form:input path="providers" class="inputField" type="text" value="" />
                                </td>
                                <td>
                                    <label id="searchFormLabel">Creation date from:</label>
                                    <form:input path="dateCreatedFrom" type="text" class="easyui-datebox"/>
                                </td>
                                <td>
                                    <label id="searchFormLabel" class="datelabel">Creation date to:</label>
                                    <form:input path="dateCreatedTo" type="text" class="easyui-datebox" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="searchFormLabel">Measurement date from:</label>
                                    <form:input path="measurementDateFrom" type="text" class="easyui-datebox"/>
                                </td>
                                <td>
                                    <label id="searchFormLabel" class="datelabel">Measurement date to:</label>
                                    <form:input path="measurementDateTo" type="text" class="easyui-datebox"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="formButtons">
                                        <input type="button" id="searchMeasuresInAdminForm" value="Search" class="button" />
                                        <input type="reset" value="Clear" class="button" />
                                    </div>
                                </td>
                            </tr>
                         </table>
                    </form:form>
                </div>

                <!-- Data-grid -->
                <div class="datagridLayer">
                        <table id="viewAllMeasurementsDataGrid" class="easyui-datagrid" style="display:none;">
                        <thead>
                            <tr>
                                <th data-options="field:'measuremenetID', width:80, sortable:true">ID</th>
                                <th data-options="field:'measurementName', width:100, sortable:true">Name</th>
                                <th data-options="field:'dateCreated', width:100, sortable:true">Created on</th>
                                <th data-options="field:'measurementDate', width:100, sortable:true">Measurement date</th>
                                <th data-options="field:'locationName', width:100, sortable:true">Location</th>
                                <th data-options="field:'city', width:100, sortable:true">City</th>
                                <th data-options="field:'providers', width:100, sortable:true">Providers</th>
                                <th data-options="field:'deleteUser', width:80, align:'center', formatter:adminMeasurementsPageCntrlr.deleteMeasurementFormatter">Delete measurement</th>                            
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
                
        </div>
            
        <div class="footerArea">About</div>
            
        <script src="<%=request.getContextPath()%>/resources/javascript/adminMeasurementsCtrlr.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
        <script type="text/javascript">
            adminMeasurementsPageInitializer("<%=request.getContextPath()%>");
        </script>
    </body>
</html>
