<%-- 
    Document   : addUser
    Created on : Jan 15, 2014, 12:09:05 AM
    Author     : sotiris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Append User</title>         
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/addForm.css">

        <!--And this javascript code below, is for the slide panel. It make's panel slide up and down -->

    </head>
    <body>

        <%@include file="header.jsp" %>
        
        <div class="bodyArea">
            
            <div id="areaToDisplayMessages" class="infoDisplayArea" style="display: none;">

            </div>
            
            <div class="mainbodyHeader">
                Add User Form 
            </div>

            <!-- Form -->
            <div id="formElement">
                <div class="tablediv" style="width: 50%;">
                    <form:form id="addUserForm" modelAttribute="newUser" style="width:100%" >
                        <table style="width:100%">
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 72px">Username<span class="mandatorySign">*</span>: </span>  
                                        <span class="newMeasurementsInputFormFields"><form:input path="selectedUser" class="inputField" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 75px">Password<span class="mandatorySign">*</span>: </span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="newPassword" class="inputField" type="password" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 10px">Re-Type Password<span class="mandatorySign">*</span>: </span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="reTypePassword" class="inputField" type="password" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 80px">User Role: </span>
                                        <span class="newMeasurementsInputFormFields">
                                            <form:select path="userRole" class="inputField">
                                                <form:option value="ROLE_USER" label="User" />
                                                <form:option value="ROLE_ADMIN" label="Admin" />
                                            </form:select>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td id="formTd">
                                    <div id="addMeasurementFormButtons">
                                        <input type="button" id="submitNewUser" value="Submit" class="button" />
                                        <input type="reset" id="resetFormBtn" value="Clear" class="button" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </div>
            </div>
        </div>
        <!--/Form -->
        
        <div class="footerArea">About</div>

        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/addUserCtrlr.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>

    </body>
</html>
