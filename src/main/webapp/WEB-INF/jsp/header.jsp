<%-- 
    Document   : header
    Created on : Mar 3, 2015, 10:31:27 AM
    Author     : Spyropoulos Sotiris <sotiris.spyr at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        
        <div class="headerArea">
            <!-- header contains the image logo and the log out button -->
            <div id="header">
                <img src="<%=request.getContextPath()%>/resources/img/gradLogIn2.png" alt="UniPi WiNR"/>

                <div class="usermsg"><a id="currentSystemUser" >${user_name}</a><img src="<%=request.getContextPath()%>/resources/img/user.png" alt="user Image" style="vertical-align:middle;"/></div>
            </div>  


            <!-- The main body of the page contains the menu, a hello-user message and a border with the projects where 
            user participates -->
            <div class="menuBar" >
                <ul>
                    <c:choose>
                        <c:when test="${user_role == 'admin'}" >
                            <li><a href="<%=request.getContextPath()%>/userPage" class="options" id="homeButton"><img src="<%=request.getContextPath()%>/resources/img/home_green.png" class="image"/></a></li>
                            <li><a href="<%=request.getContextPath()%>/admin/addUser.action" class="options">Append User</a></li>
                            <li><a href="<%=request.getContextPath()%>/admin/viewAllMeasurements.action" class="options">View All Measurements</a></li>
                            <li><a  href=<c:url value="/j_spring_security_logout"/> class="logout" >Logout</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<%=request.getContextPath()%>/userPage" class="options" id="homeButton"><img src="<%=request.getContextPath()%>/resources/img/home_green.png" class="image"/></a></li>
                            <li><a href="<%=request.getContextPath()%>/user/addProject.action"  class="options"> New Measurement </a></li>
                            <li><a  href=<c:url value="/j_spring_security_logout"/> class="logout" >Logout</a></li>
                        </c:otherwise>
                    </c:choose>
                    
                </ul>
                <div class="floatr"></div>
            </div>
        </div>
    </body>
</html>
