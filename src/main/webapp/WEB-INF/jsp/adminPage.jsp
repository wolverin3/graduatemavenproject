<%-- 
    Document   : userPage
    Created on : Jun 21, 2014, 8:31:45 PM
    Author     : wolverine
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrator Page</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/userPage.css">
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery.leanModal.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>

    </head>
    <body>

        <%@include file="header.jsp" %>


        <div class="bodyArea">
            <div class="mainbodyHeader">
                User Data 
            </div>

            <div id="searchUserForm">
                <span searchUserHeader>Search User</span>
                <br>
                <hr>
                <form:form modelAttribute="searchUser" class="searchUserForm">
                    <label id="searchFormLabel">User name:</label>
                    <form:input path="userName" class="inputField" type="text" value="" />
                    <label id="searchFormLabel">Status:</label>
                    <form:select path="enabled" class="inputField" type="text" >
                        <form:option value="" label="Any" />
                        <form:option value="Enabled" label="Enabled" />
                        <form:option value="Disabled" label="Disabled" />
                    </form:select>
                    <label id="searchFormLabel">User role:</label>
                    <form:select path="role" class="inputField" type="text" >
                        <form:option value="" label="All" />
                        <form:option value="ROLE_USER" label="User" />
                        <form:option value="ROLE_ADMIN" label="Administrator" />
                    </form:select>
                    <br><br>
                    <input type="button" id="searchUserInAdminForm" value="Search" class="button" />
                    <input type="reset" value="Clear" class="button" />
                </form:form>
            </div>

            <!-- Data-grid -->
            <div class="datagridLayer">
                <table id="usersDataGrid" class="easyui-datagrid" style="display:none;">
                    <thead>
                        <tr>
                            <th data-options="field:'userName', width:80, sortable:true">User</th>
                            <th data-options="field:'enabled', width:100, sortable:true, formatter:adminUsersPageCtrlr.enabledLink">Status</th>
                            <th data-options="field:'role', width:100, sortable:true">Role</th>
                            <th data-options="field:'deleteUser', width:80, align:'center', formatter:adminUsersPageCtrlr.deleteUserFormatter">Delete user</th>
                            <th data-options="field:'configureUser', width:80, align:'center', formatter:adminUsersPageCtrlr.configureUserFormatter">Configure user</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <a href='#userConfigModal'  id='userConfigModalTrigger' class='userUpdateTriggerForm'></a>

            <!-- Modal window -->     
            <div id="userConfigModal" class="dialog">
                <div id="modaleHeader">
                    <h1>Configure Users Data</h1>
                </div>
                <div class="error" id="userConfDisplayError">

                </div>
                <div class="madatoryMessage">
                    Update user password, role or both.
                </div>
                <form:form modelAttribute="configUser" class="userConfigForm">
                    <div class="modal_fields">
                        <label>Old password</label>
                        <form:input path="oldPassword"  type="password" class="inputField"  id="oldPassword" value=""  />
                    </div>

                    <form:input path="selectedUser" type="hidden" id="hiddenUserNameField" />

                    <div class="modal_fields">
                        <label>New password:</label>
                        <form:input path="newPassword" type="password" class="inputField"  id="newPassword" value=""  />
                    </div>

                    <div class="modal_fields">
                        <label> Re-type password:</label>
                        <form:input path="reTypePassword" type="password" class="inputField"  id="reTypePassword" value="" />
                    </div>

                    <div class="modal_fields">
                        <label>User role: </label>
                        <form:select path="userRole"  class="inputField"  id="role">
                            <form:option value="" label=" " />
                            <form:option value="ROLE_USER" label="User" />
                            <form:option value="ROLE_ADMIN" label="Admin" />
                        </form:select>
                    </div>

                    <div class="modal_fields">
                        <input type="button" class="button" value="Submit" id="submitUserConfigureForm" />
                        <input type="reset" class="button" value="Reset" id="resetConfUserForm" />
                        <input type="button" value="close" id="closeUserForm" class="button" />
                    </div>

                </form:form>  
            </div>
        </div>

        <div class="footerArea">About</div>

        <script src="<%=request.getContextPath()%>/resources/javascript/adminPageUsersCtrl.js"></script>
        <script type="text/javascript">
            intializeAdminUsersPage("<%=request.getContextPath()%>");
            $('.userUpdateTriggerForm').leanModal();
        </script>
    </body>
</html>
