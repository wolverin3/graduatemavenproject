<%-- 
    Document   : measurementPlaceDetails
    Created on : Feb 11, 2015, 12:28:42 PM
    Author     : Sammy Guergachi <sguergachi at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Place details page</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/fotorama.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/addForm.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/userPage.css">
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery.leanModal.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/fotorama/fotorama.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/placeDetails.js"></script>
    </head>
    <body>
        <%@include file="header.jsp" %>

        <div>

            <div id="areaToDisplayMessages" class="infoDisplayArea" style="display: none;">

            </div>

            <div class="mainbodyHeader">
                Place details 
            </div>

            <!-- Measurement data -->
            <div id="formElement">
                <div class="tablediv" style="width: 50%;">
                    <form:form id="placeDetailsForm" modelAttribute="placeDetailsForm">
                        <table style="width:100%">
                            <form:hidden path="placeId" />
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 87px">Location<span class="mandatorySign">*</span>:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="placeName" class="inputField"/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 53px" >Coordinates X:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="coordinatesX" type="text" class="inputField"/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 53px" >Coordinates Y:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="coordinatesY" type="text" class="inputField"/></span>
                                    </div>
                                </td>
                            </tr>
                            <c:if test="${userMeasurementRights}" >
                                <tr>
                                    <td id="formTd">
                                        <div id="addMeasurementFormButtons">
                                            <input type="button" value="Update" id="updateMeasurementPlaceDetails" class="button"  />
                                            <input type="reset" value="Clear" class="button" />
                                        </div>
                                    </td>
                                </tr>
                            </c:if>
                        </form:form>
                    </table>
                </div>
            </div>

            <!-- Pictures -->
            <div class="sectionHeaders">
                Pictures 
            </div>
            <c:if test="${userMeasurementRights}" >
                <div class="sectionButtons">
                    <input type="button" id="addPlaceImages" class="button" value="Add" />
                </div>
                <br>
            </c:if>

                
            <c:if test="${hasImages}">
                <table class="picturesTable">
                    <tr>
                        <td align="center">
                            <div class="deleteImageArea">
                                <form:form id="deleteImagesForm" modelAttribute="deleteImageFormObjct">
                                    <label>Delete images</label>
                                    <form:select multiple="true" size="10" id="selectedImages" path="selectedImages">
                                        <form:options items="${imagesData}" itemValue="id" itemLabel="imageName"/>
                                    </form:select>
                                    <br><br>
                                    <input type="button" id="deleteImages" class="button" value="Delete" />
                                    <br>
                                </form:form>
                            </div>
                        </td>
                        <td align="center">
                             <div class="displayPicturesSpot" >
                                <div class="fotorama imageDisplayArea" data-allowfullscreen="native" data-fit="cover" data-arrows="true" data-click="false" 
                                     data-swipe="false" data-nav="thumbs" data-width="632.888888888889px" data-height="356px" styele: ="left: 0.0555555555555998px; top: 0px;">
                                    <c:forEach var="imageId" items="${imagesIds}" varStatus="counter" >
                                        <img id="image${imageId}" src="<%=request.getContextPath()%>/user/measurement/place/image/${imageId}.action" data-fit="contain" data-caption="${imageName[counter.index]}"/>
                                    </c:forEach>>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </c:if>
        </div>

        <div class="footerArea">About</div>

        <!-- Add images modal link -->
        <a href='#addNewPlaceImagesModal'  id='addNewPlaceImagesModalTrigger' class='addNewPlaceImagesTriggerForm' style="display: none"></a>

        <!------------------------------------- Modal part ------------------------------------>

        <!-- Measurements images modal -->
        <div id="addNewPlaceImagesModal" class="dialog">
            <div id="modaleHeader">
                <h1>Configure Images Data</h1>
                <a class="modal_close" href="#"></a>
            </div>
            <div class="error" id="addMeasurementImagesDisplayError" style="display: none;">
            </div>
            <div id="usersSearchForm">  
                <form id="addImages">
                    <span style="padding-bottom: 5px;">
                        <label> Image 1: </label>
                        <input name="files[0]" id="image1" type="file" />
                    </span>
                    <hr style="border-top: 1px solid #ccc;">
                    <br/>
                    <span style="padding-bottom: 5px;">
                        <label> Image 2: </label>
                        <input name="files[1]" id="image2" type="file" />
                    </span>
                    <hr style="border-top: 1px solid #ccc;">
                    <br/>
                    <span style="padding-bottom: 5px;">
                        <label> Image 3: </label>
                        <input name="files[2]" id="image3" type="file" />
                    </span>
                    <hr style="border-top: 1px solid #ccc;">
                    <br/>
                    <span style="padding-bottom: 5px;">
                        <label> Image 4: </label>
                        <input name="files[3]" id="image4" type="file" />
                    </span>
                    <hr style="border-top: 1px solid #ccc;">
                    <br/>
                    <span style="padding-bottom: 5px;">
                        <label> Image 5: </label>
                        <input name="files[4]" id="image5" type="file" />
                    </span>
                    <hr style="border-top: 1px solid #ccc;">
                    <input type="button" id="uploadMeasurementImagessBtn" class="button" value="Save" />
                    <input type="reset" class="button" value="Clear" />   
                </form> 
            </div>
        </div> 


        <script type="text/javascript">
            placeDetailsCtrlr.initializer(${userMeasurementRights}, "<%=request.getContextPath()%>");
        </script>
    </body>
</html>
