<%-- 
    Document   : userPage
    Created on : Aug 4, 2014, 10:18:15 PM
    Author     : Spyropoulos Sotiris <sotiris.spyr at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>User Page</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/userPage.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/forms/forms-min.css">
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery.leanModal.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
    </head>
    <body>
        
        <%@include file="header.jsp" %>
        
        <div class="bodyArea">
        
            <div class="mainbodyHeader">
                Measurements Data 
            </div>

            <div id="searchMeasurementsForm">
                <span searchUserHeader>Search Measurement</span>
                <br>
                <hr>
                <form:form modelAttribute="searchMeasurements" class="pure-form">
                    <table style="width:100%">
                        <tr class="searchFormRow">
                            <td>
                                <label id="searchFormLabel">Measurement name:</label>
                                <form:input path="measurementName" class="inputField" type="text" value="" />
                            </td>
                            <td>
                                <label id="searchFormLabel">Location:</label>
                                <form:input path="locationName" class="inputField" type="text" value="" />
                            </td>
                            <td>
                                <label id="searchFormLabel">City:</label>
                                <form:input path="city" class="inputField" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label id="searchFormLabel">Providers:</label>
                                <form:input path="providers" class="inputField" type="text" value="" />
                            </td>
                            <td>
                                <label id="searchFormLabel">Creation date from:</label>
                                <form:input path="dateCreatedFrom" type="text" class="easyui-datebox"/>
                            </td>
                            <td>
                                <label id="searchFormLabel">Creation date to:</label>
                                <form:input path="dateCreatedTo" type="text" class="easyui-datebox" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label id="searchFormLabel">Measurement date from:</label>
                                <form:input path="measurementDateFrom" type="text" class="easyui-datebox"/>
                            </td>
                            <td>
                                <label id="searchFormLabel" class="datelabel">Measurement date to:</label>
                                <form:input path="measurementDateTo" type="text" class="easyui-datebox"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="formButtons">
                                    <input type="button" id="searchMeasuresInUserForm" value="Search" class="button" />
                                    <input type="reset" value="Clear" class="button" />
                                </div>
                            </td>
                        </tr>
                     </table>
                </form:form>
            </div>

            <!-- Data-grid -->
            <div class="datagridLayer">
                <table id="userMeasurementsDataGrid" class="easyui-datagrid" style="display:none; width: 73%">
                    <thead>
                        <tr>
                            <th data-options="field:'measuremenetID', width:80, sortable:true">ID</th>
                            <th data-options="field:'measurementName', width:100, sortable:true, formatter:userMeasurementsCtrl.linkToMeasurementDetailsFormatter">Name</th>
                            <th data-options="field:'dateCreated', width:100, sortable:true">Created on</th>
                            <th data-options="field:'measurementDate', width:100, sortable:true">Measurement date</th>
                            <th data-options="field:'providers', width:100, sortable:true">Providers</th>
                            <th data-options="field:'deleteMeasur', width:80, align:'center', formatter:userMeasurementsCtrl.deleteMeasurementFormatter">Delete measurement</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        
        <div class="footerArea">About</div>
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/userPageCtrlr.js"></script>
        <script type="text/javascript">
            initializeUserMeasurementsPage("<%=request.getContextPath()%>");
        </script>
    </body>
</html>
