<%-- 
    Document   : index
    Created on : Jun 15, 2014, 5:52:28 PM
    Author     : wolverine
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to WiNR project</title>
        <link href="resources/css/logInPage.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="login-wrapper">
            <div id="login-top">
                <img src="resources/img/gradLogIn1.png" alt="UniPi WiNR"/>
            </div>
            
            <c:if test="${not empty param.error}"> 
                <div class="error">
                    Login error : Please try again.<br />Root Cause:
                    ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} or user disabled.
                </div>
            </c:if>
        

            <div id="login-content">
                <form method="POST" action="<c:url value="/j_spring_security_check" />">
                    <p>
                        <label>Username</label>
                        <input type="text" name="j_username" class="text-input"/>
                    </p>
                    <br style="clear: both;">
                    <p>
                        <label>Password</label>
                        <input type="password" name="j_password" class="text-input" />
                    </p>
                    <br style="clear: both;">
                    <p>
                        <label>Remember me</label>
                        <input type="checkbox" name="_spring_security_remember_me" class="radiobutton" />
                    </p>         
                    <br style="clear: both;">
                    <input type="reset" value="Reset" class="button" />  
                    <input type="submit" value="Login" class="button" />
                </form>
            </div>
        </div>
        <div id="dummy"></div>
        <div id="dummy2"></div>
    </body>
</html>
