<%-- 
    Document   : newMeasurement
    Created on : Aug 7, 2014, 11:24:19 AM
    Author     : Spyropoulos Sotiris <sotiris.spyr at gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New measurement</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/addForm.css">
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        
        
    </head>
     <body>
         
        <%@include file="header.jsp" %>
        
        <div class="bodyArea">
            
            <div id="areaToDisplayMessages" class="infoDisplayArea" style="display: none;">

            </div>

            <div class="mainbodyHeader">
                New measurement
            </div>

            <!-- Form -->
            <div id="formElement">
                <div class="tablediv" style="width: 50%;">
                    <form:form id="newMeasurementForm" modelAttribute="newMeasurement" style="width:100%" >
                        <form:hidden path="usersIDs" />
                        <table style="width:100%">
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 5px">Measurement name<span class="mandatorySign">*</span>:</span>  
                                        <span class="newMeasurementsInputFormFields"><form:input path="measurementName" class="inputField" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 18px">Measurement date<span class="mandatorySign">*</span>:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="measurementDate" type="text" editable="false" class="easyui-datebox"/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 121px">City<span class="mandatorySign">*</span>:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="city" class="inputField" type="text" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 87px">Location<span class="mandatorySign">*</span>:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="locationName" class="inputField" type="text" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 53px" >Coordinates X:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="coordinatesX" class="inputField" type="text" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 53px">Coordinates Y:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="coordinatesY" class="inputField" type="text" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <div class="formField">
                                        <span class="formLabelSpan" style="padding-right: 80px">Providers<span class="mandatorySign">*</span>:</span>
                                        <span class="newMeasurementsInputFormFields"><form:input path="providers" class="inputField" type="text" value=""/></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="formTd">
                                    <span class="formLabelSpanWithUsers">Append users:</span>
                                    <p>
                                       
                                        <!-- Search field -->
                                        <label class="formLabelSpan" style="padding-right: 66px" >Search User: </label><input id="searchUserForNewMeasurementId" class="inputField" type="text" value="" />
                                        <hr style="border-top: 1px solid #ccc;">
                                        <input type="button" id="searchNewMeasurementUserBtn" value="Search" class="button"></button>
                                    </p>
                                    <p>
                                        <!-- Data-grid -->
                                        <div class="datagridLayer">
                                            <table id="newMeasurementUsers" class="easyui-datagrid" style="display:none;">
                                                <thead>
                                                    <tr>
                                                        <th data-options="field:'userName', width:80, sortable:true, align:'center'">Select Users</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td id="formTd">
                                    <div id="addMeasurementFormButtons">
                                        <input type="button" value="Submit" id="submitNewMeasurement" class="button"  />
                                        <input type="reset" value="Clear" class="button" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </div>
            </div>
        </div>
        
        <div class="footerArea">About</div>
        
        <!-- /Form -->
        <script src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/userPageCtrlr.js"></script>
        <script type="text/javascript"> 
            initializeCreateNewMeasurementPage("<%=request.getContextPath()%>");
            if (${isNotNew && hasError}) {
                createNewMeasurementPageCtrl.newMeasurementsComfiramtionsMessages("${infoMessage}");
            }
        </script>
    </body>
</html>
