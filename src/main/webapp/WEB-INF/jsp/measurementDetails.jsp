<%-- 
    Document   : measurementDetails
    Created on : Oct 29, 2014, 1:03:25 PM
    Author     : Spyropoulos Sotiris
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Measurement details page</title>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/gray/easyui.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/themes/icon.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/maintheme.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/addForm.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/userPage.css">
        <script src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery-1.11.1.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/javascript/easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery.leanModal.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/jquery.fileDownload.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/jquery/serailizaFormToJSON.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/commonFunctions.js"></script>
        
        
    </head>
    <body>
        <%@include file="header.jsp" %>

        <div id="areaToDisplayMessages" class="infoDisplayArea" style="display: none;">

        </div>

        <div class="mainbodyHeader">
            Measurement details 
        </div>
        <!-- Measurement data -->
        <div id="formElement">
            <div class="tablediv" style="width: 50%;">
                <form:form id="measurementForm" modelAttribute="measurement">

                    <form:hidden path="measuremenetID" />

                    <table style="width:100%">
                        <tr>
                            <td class="formTd">
                                <div class="formField">
                                    <span class="formLabelSpan" style="padding-right: 5px">Measurement name<span class="mandatorySign">*</span>:</span>  
                                    <span class="newMeasurementsInputFormFields"><form:input path="measurementName" class="inputField"/></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="formTd">
                                <div class="formField">
                                    <span class="formLabelSpan" style="padding-right: 63px">Date created:</span>
                                    <span class="newMeasurementsInputFormFields"><form:input path="dateCreated" type="text" disabled="true" class="inputField"/></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="formTd">
                                <div class="formField">
                                    <span class="formLabelSpan" style="padding-right: 18px">Measurement date<span class="mandatorySign">*</span>:</span>
                                    <span class="newMeasurementsInputFormFields"><form:input path="measurementDate" type="text" editable="false" id="measurementDateCreatedField" class="easyui-datebox inputField"/></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="formTd">
                                <div class="formField">
                                    <span class="formLabelSpan" style="padding-right: 121px">City<span class="mandatorySign">*</span>:</span>
                                    <span class="newMeasurementsInputFormFields"><form:input path="city" class="inputField" type="text"/></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="formTd">
                                <div class="formField">
                                    <span class="formLabelSpan" style="padding-right: 80px">Providers<span class="mandatorySign">*</span>:</span>
                                    <span class="newMeasurementsInputFormFields"><form:input path="providers" class="inputField" type="text"/></span>
                                </div>
                            </td>
                        </tr>
                        <c:if test="${measurement.isUserRole()}" >
                            <tr>
                                <td id="formTd">
                                    <div id="addMeasurementFormButtons">
                                        <input type="button" value="Save" id="updateMeasurementDetails" class="button"  />
                                        <input type="reset" value="Clear" class="button" />
                                    </div>
                                </td>
                            </tr>
                        </c:if>
                    </table>


                </form:form>
            </div>
        </div>

        <!-- Update measurement users section -->
        <div class="sectionHeaders">
            Measurement users 
        </div>

        <!--measurement participants-->
        <c:if test="${measurement.isUserRole()}" >
            <div class="sectionButtons">
                <input type="button" id="addMeasurementUsers" class="button" value="Add" />
                <input type="button" id="deleteMeasurementUsers" class="button" value="Delete" />
            </div>
            <br>
        </c:if>

        <div class="measurementDetailsDatagridLayer" >
            <table id="measurementParticipantsDataGrid" class="easyui-datagrid" style="display:none;">
                <thead>
                    <tr>
                        <c:if test="${measurement.isUserRole()}">
                            <th data-options="field:'check', checkbox:'true'"></th>
                            </c:if>
                        <th data-options="field:'userName', width:80, sortable:true">User</th>
                            <c:choose>
                                <c:when test="${measurement.isUserRole()}" >
                                <th data-options="field:'hasRightsOnMeasurement', width:80, sortable:true, formatter:measurementDetailsCtrlr.userMeasurementRights">Measurement rights</th>
                                </c:when>
                                <c:otherwise>
                                <th data-options="field:'hasRightsOnMeasurement', width:80, sortable:true">Measurement rights</th>
                                </c:otherwise>
                            </c:choose>
                    </tr>
                </thead>
            </table>
        </div>

        <!-- Images section -->
        <div class="sectionHeaders">
            Measurement places 
        </div>

        <c:if test="${measurement.isUserRole()}" >
            <div class="sectionButtons">
                <input type="button" id="addMeasurementPlace" class="button" value="Add" />
                <input type="button" id="deleteMeasurementPlaces" class="button" value="Delete" />
            </div>
            <br>
        </c:if>

        <div class="measurementDetailsDatagridLayer measurementPlacesDG" >
            <table id="measurementPlacesDataGrid" class="easyui-datagrid" style="display:none;">
                <thead>
                    <tr>
                        <c:if test="${measurement.isUserRole()}">
                            <th data-options="field:'check', checkbox:'true'"></th>
                            </c:if>
                        <th data-options="field:'placeName', width:80, sortable:true, formatter:measurementDetailsCtrlr.placeDetailsLink">Location name</th>
                        <th data-options="field:'coordinates', width:50, sortable:false, align:'center', formatter:measurementDetailsCtrlr.mapsIconLink">Location on map</th>
                    </tr>
                </thead>
            </table>
        </div>

        <!-- Data section -->
        <div class="sectionHeaders">
            Measurement Documents
        </div>

        <c:if test="${measurement.isUserRole()}" >
            <div class="sectionButtons">
                <input type="button" id="addMeasurementData" class="button" value="Add" />
                <input type="button" id="deleteMeasurementData" class="button" value="Delete" />
            </div>
            <br>
        </c:if>

        <div class="measurementDetailsDatagridLayer measurementDocsDG" >
            <table id="measurementDocumentsDataGrid" class="easyui-datagrid" style="display:none;">
                <thead>
                    <tr>
                        <c:if test="${measurement.isUserRole()}">
                            <th data-options="field:'check', checkbox:'true'"></th>
                        </c:if>
                        <th data-options="field:'name', width:40, sortable:true">Name</th>
                        <th data-options="field:'documentType', width:40, sortable:false, align:'center'">Type</th>
                        <th data-options="field:'fileType', width:20, sortable:false, align:'center', formatter:measurementDetailsCtrlr.fileTypeIcon">File</th>
                    </tr>
                </thead>
            </table>
        </div>

        <div class="footerArea">About</div>

        <!-- Add users modal link -->
        <a href='#addNewMeasurementUsersModal'  id='addNewMeasurementUsersModalTrigger' class='addNewMeasurementUsersTriggerForm' style="display: none"></a>
        <!-- Add images modal link -->
        <a href='#addNewMeasurementPlaceModal'  id='addNewMeasurementPlaceModalTrigger' class='addNewMeasurementPlaceTriggerForm' style="display: none"></a>
        <!-- Add measurement data modal link -->
        <a href='#addNewMeasurementDataModal'  id='addNewMeasurementDataModalTrigger' class='addNewMeasurementDataTriggerForm' style="display: none"></a>


        <!----------------------- Modals --------------------------->

        <!-- Add users modal window -->     
        <div id="addNewMeasurementUsersModal" class="dialog">
            <div id="modaleHeader">
                <h1>Configure Users Data</h1>
                <a class="modal_close" href="#"></a>
            </div>
            <div class="error" id="addMeasurementUserDisplayError" style="display: none;">
            </div>
            <div id="usersSearchForm">
                <form id="allUsers">
                    Users: 
                    <input type="text" id="searchNotRegisteredUsersId" value=""/>
                    <br>
                    <hr>
                    <input type="button" id="searchAllUsers" class="button" value="Search" />
                    <input type="button" id="addMeasurementUsersBtn" class="button" value="Add" />   
                </form> 
            </div>
            <p>
            <div id="allUsersDataGrid" style="z-index: 2;">
                <table id="restOfUsers" class="easyui-datagrid" style="display:none;">
                    <thead>
                        <tr>
                            <th data-options="field:'check', checkbox:'true'"></th>
                            <th data-options="field:'userName', width:80, sortable:true, align:'center'">Select Users</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </p>
    </div> 

</div>

<!-- Measurements Place modal -->
<div id="addNewMeasurementPlaceModal" class="dialog">
    <div id="modaleHeader">
        <h1>New measurement Place</h1>
        <a class="modal_close" href="#"></a>
    </div>
    <div class="error" id="addMeasurementPLaceDisplayError" style="display: none;">
    </div>
    <div id="usersSearchForm">  
        <form id="addPlaces">
            <span style="padding-bottom: 5px;">
                <label> Place name<span class="mandatorySign">*</span>: </label>
                <input name="placeName" id="placeName" type="text" />
            </span>
            <hr style="border-top: 1px solid #ccc;">
            <br/>
            <span style="padding-bottom: 5px;">
                <label> CoordinatesX: </label>
                <input name="coordinatesX" id="coordinatesX" type="text" class="floatField" />
            </span>
            <hr style="border-top: 1px solid #ccc;">
            <br/>
            <span style="padding-bottom: 5px;">
                <label> CoordinatesY: </label>
                <input name="coordinatesY" id="coordinatesX" type="text" class="floatField" />
            </span>
            <hr style="border-top: 1px solid #ccc;">
            <input type="button" id="savePLaceBtn" class="button" value="Save" />
            <input type="reset" class="button" value="Clear" />   
        </form> 
    </div>
</div> 

<!-- Measurements Data modal -->
<div id="addNewMeasurementDataModal" class="dialog">
    <div id="modaleHeader">
        <h1>New measurement Data</h1>
        <a class="modal_close" href="#"></a>
    </div>
    <div class="error" id="addMeasurementDataDisplayError" style="display: none;">
    </div>
    <div id="usersSearchForm">  
        <form id="addImages">
            <span style="padding-bottom: 5px;">
                <label>File: </label>
                <input name="file" id="file" type="file" />
            </span>
            <hr style="border-top: 1px solid #ccc;">
            <br/>
            <span style="padding-bottom: 5px;">
                <label>File Type<span class="mandatorySign">*</span>: </label>
                <select id="fileTypeSelection">
                    <option value="Report">Report</option>
                    <option value="Measurement_data">Measurement data</option>
                    <option value="Measurement_pdata">Measurement pdata</option>
                </select>
            </span>
            <hr style="border-top: 1px solid #ccc;">
            <input type="button" id="uploadMeasurementDataBtn" class="button" value="Save" />
            <input type="reset" class="button" value="Clear" id="clearMeasurementDocsForm" />   
        </form> 
    </div>
</div> 


<script type="text/javascript" src="<%=request.getContextPath()%>/resources/javascript/measurementDetailsCtrlr.js"></script>
<script type="text/javascript">
    measurementDetailsCtrlr.initializer("<%=request.getContextPath()%>");
    measurementDetailsCtrlr.setDateMeasurementMade("${measurement.getMeasurementDate()}"); //set default datebox value for the date measurement made
</script>
<script type="text/javascript">
    $('.addNewMeasurementUsersTriggerForm').leanModal({closeButton: ".modal_close"});
    $('.addNewMeasurementPlaceTriggerForm').leanModal({closeButton: ".modal_close"});
    $('.addNewMeasurementDataTriggerForm').leanModal({closeButton: ".modal_close"});
    
    measurementDetailsCtrlr.getInitialMeasurementValues(); //get initial form values
    measurementDetailsCtrlr.participantsInMeasurementDataGrid(); //get values for measurements participants daragrid
    measurementDetailsCtrlr.measurementDocumentsDataGrid(); //get values for measurements documents daragrid
    measurementDetailsCtrlr.checkIfUserCanEditInputFields(${measurement.isUserRole()});
    measurementDetailsCtrlr.loadMeasurementPlaces();
</script>
</body>
</html>
