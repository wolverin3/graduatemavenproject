/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.util;

import com.university.graduatemavenproject.services.Impl.UserServiceImpl;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Spyropoulos Sotiris
 */
public class UtilityFunctions {
    
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    
    public static String datetoString(Date date, String formatter){
        if(date == null){
            return "";
        }
        /** Create an instance of SimpleDateFormat used for formatting  */
        DateFormat df = new SimpleDateFormat(formatter);
     
        /**  Using DateFormat format method we can create a string 
         representation of a date with the defined format. */
        return df.format(date);
    }
    
    /**
     * Converts the provided String to Date using the formatter
     * 
     * @param date
     * @param formatter
     * @return
     * @throws ParseException
     */
    public static Date stringToDate(String date, String formatter) throws ParseException{
    	if(StringUtils.isEmpty(date)){
    		return null;
    	}
        SimpleDateFormat myFormat = new SimpleDateFormat(formatter);
        
        try {
 
            Date formattedDate = myFormat.parse(date);
            System.out.println(date);
            
            return formattedDate;
 
	} catch (ParseException e) {
            logger.error("Error while formatting date: " , e);
	}
        
        
        return null;
    	
    }
    
}
