/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.services;

import com.university.graduatemavenproject.db.entities.Images;
import com.university.graduatemavenproject.db.entities.Place;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sotiris
 */
public interface ImageService {
    
    /**
     * This method retrieve a image object from database searching by its id
     * @param id
     * @return 
     */
    @Transactional
    Images findImageById(Integer id);
    
    /**
     * This method save a new image object in database
     * 
     * @param image
     * @return 
     */
    @Transactional
    Images registerImage(Images image);
    
    /**
     * This method return all images linked with a specific Place object.
     * 
     * @param place
     * @return 
     */
    @Transactional
    List<Images> findImagesByPlace(Place place);
    
    /**
     * This method return all images id's linked with a specific Place
     * 
     * @param place
     * @return 
     */
    @Transactional
    List<Integer> findImagesIdsByPlace(Place place);
    
    /**
     * This method return all images names linked with a specific Place
     * 
     * @param place
     * @return 
     */
    @Transactional
    List<String> findImagesNameByPlace(Place place);
    
    /**
     * This method retrieve image data from database.
     * 
     * @param imageId
     * @return 
     */
    @Transactional
    byte[] getPlaceImageByID(Integer imageId);
    
    /**
     * Delete a list of images in database
     * @param images 
     */
    @Transactional
    Boolean deleteImagesList(List<Images> images);
}
