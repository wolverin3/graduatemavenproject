/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services;

import com.university.graduatemavenproject.db.entities.MeasurementData;
import com.university.graduatemavenproject.db.entities.MeasurementDataType;
import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.GlobalRequestWrapper;
import com.university.graduatemavenproject.model.transactional.DocumentDTO;
import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author sotiris
 */
public interface MeasurementsService {
    
    /**
     * This function finds all measurements stored in database
     * @return 
     */
    @Transactional
    public List<Measurements> findAllMeasurements();
    
    /**
     * Searching a measurements deppending on its id
     * @param id
     * @return 
     */
    @Transactional
    public Measurements findMeasurementById(Integer id);
    
    /**
     * This function converts a Measurements object to MeasurementDTO
     * @param measurement
     * @param user
     * @return 
     */
    public MeasurementDTO convertMeasurementsToMeasurementDTO(Measurements measurement, Users user);
    
    /**
     * This function search for the measurements which their data match with those of measurementsData object
     * @param measurementsData
     * @param page
     * @param pageSize
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper searchMeasurements(MeasurementDTO measurementsData, int page, int pageSize);
    
    /**
     * This function retrieve all measurements from database and convert them to MeasurementDTO
     * to display in admin page
     * @param page
     * @param pageSize
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper getAllMeasurementsForAdmin(int page, int pageSize);
    
    /**
     * This function deletes a specific measurement
     * @param id
     * @return 
     */
    @Transactional
    public Boolean deleteMeasurementById(Integer id);
    
    /**
     * This method's functionality is to make all the proper actions to delete 
     * a measurement from database.
     * 
     * @param id
     * @return 
     */
    @Transactional 
    public Boolean deleteMeasurementFunctionality(Integer id);
    
    /**
     * This function find all user's related measurements
     * @param user
     * @param page
     * @param pageSize
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper findUsersMeasurementsByUser(Users user, int page, int pageSize);
    
    /**
     * This function is being call when a user try to register a new measurement.
     * Its purpose is to create a new Measurement, Location and to bind the user/s to the new measurement
     * @param creator
     * @param users
     * @param newMeasurementFormData
     * @return 
     */
    @Transactional
    public Integer createNewMeasurementAction(Users creator, List<Users> users, MeasurementDTO newMeasurementFormData);
    
    /**
     * Save new Measurement
     * @param measurement
     * @return 
     */
    @Transactional
    public Measurements registerMeasurement(Measurements measurement);
    
    /**
     * This function is responsible to make all the proper actions to update a measurement 
     * @param measurement
     * @return 
     */
    @Transactional
    public Measurements updateMeasurement(Measurements measurement);
    
    /**
     * This function is responsible to make all the proper actions to register a new measurement and add it corresponding it 
     * with the proper users
     * @param newMeasurementData
     * @param forUpdate
     * @return 
     */
    @Transactional
    public Measurements configureMeasurement(MeasurementDTO newMeasurementData, Boolean forUpdate);
    
    /**
     * This function return measurement's Place object, searching by measurement's id
     * @param measurementID
     * @return 
     */
    @Transactional
    public Integer findPlaceByMeasurementID(Integer measurementID);
    
    /**
     * This function save a new UserMeasurements object in database, as a binding between a user and a measurement
     * @param userMeasurements
     * @return 
     */
    @Transactional
    public UserMeasurements registerNewMeasurementForUser(UserMeasurements userMeasurements);
    
    /**
     * This function save a list of UserMeasurements objects in database
     * @param userMeasurementsList
     */
    @Transactional
    public void registerAListOfUserMeasurements(List<UserMeasurements> userMeasurementsList);
    
    /**
     * Find all user's measurements. It takes as parameter the user object.
     * @param user
     * @return 
     */
    @Transactional
    public List<UserMeasurements> findMeasurementsSearchingByUser(Users user);
    
    /**
     * This function delete a Place record from database
     * @param place
     */
    @Transactional
    public void deletePlace(Integer place);
    
    /**
     * This method's aim is to unbind a user from a measurement, in other words
     * it delete the UsersMeasurement object which has as elements the specific 
     * user and the measurement
     * @param usersList
     * @param measurement 
     */
    @Transactional
    public void unbindUsersFromMeasurement(List<Users> usersList, Measurements measurement);
    
    /**
     * This method delete all userMeasurements containing into the list
     * @param userMeasurementsList 
     */
    @Transactional
    public void deleteAListOfUserMeasurements(List<UserMeasurements> userMeasurementsList);
    
    /**
     * This method update the rights a user has on a measurement (read, write)
     * 
     * @param user
     * @param measurement 
     * @return  
     */
    @Transactional
    public GlobalRequestWrapper updateMeasurementsRightsForAUsersList(Users user, Measurements measurement);
    
    /**
     * This function return the UserMeasurements object from database searching 
     * by user and measurement
     * @param user
     * @param measurement
     * @return 
     */
    @Transactional
    public UserMeasurements findUserMeasurementByUserAndMeasurement(Users user, Measurements measurement);
    
    /**
     * This method return the rights where a user has on a measurement  
     * @param user
     * @param measurement
     * @return 
     */
    @Transactional
    public Boolean getUserRightsOnAMeasurement(Users user, Measurements measurement);
    
    /**
     * This method return the method data type searching by name
     * 
     * @param measurementDataType
     * @return 
     */
    @Transactional
    public MeasurementDataType getMeasurementDataTypeByName(String measurementDataType);
    
    /**
     * This method return the method data type searching by measurementData
     * 
     * @param measurementData
     * @return 
     */
    @Transactional
    public MeasurementDataType getMeasurementDataTypeByMeasurementData(MeasurementData measurementData);
    
    /**
     * This method register a MeasurementData object in database.
     * 
     * @param mesurementData
     * @return 
     */
    @Transactional
    public MeasurementData registerMeasurementData(MeasurementData mesurementData);
    
    /**
     * This method delete all MeasurementData contained into the given list
     * 
     * @param measurementDataIdList 
     */
    @Transactional
    public void deleteMeasurementDataSearchingByIdList(List<Integer> measurementDataIdList);
    
    /**
     * This method return a list with all documents linked with a specific
     * measurement.
     * 
     * @param measurementId
     * @param pageable
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper getMeasurementDocuments(Integer measurementId, Pageable pageable);
    
    /**
     * This method convert a MeasurementData object to DocumentDTO.
     * 
     * @param measurementData
     * @return 
     */
    public DocumentDTO convertMeasurementDataToDocumentDTO(MeasurementData measurementData);
    
    /**
     * This method retrieve a MeasurementData object from database with the
     * specific id
     * 
     * @param measurementDataId
     * @return 
     */
    @Transactional
    public MeasurementData fingMeasurementDataById(Integer measurementDataId);
    
    /**
     * 
     * @param measurement
     * @return 
     */
    @Transactional
    public List<UserMeasurements> findMeasurementUserMeasurements(Measurements measurement);
}
