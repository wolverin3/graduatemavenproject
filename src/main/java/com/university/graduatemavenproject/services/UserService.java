/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.transactional.UsersDTO;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wolverine
 */
public interface UserService {
    
    /**
     * Finds a user by username
     * @param username
     * @return 
     */
    @Transactional
    public Users findUser(String username);
    
    /**
     * This function finds a list of Users objects searching by username
     * @param userNames
     * @return 
     */
    @Transactional
    public List<Users> findAListOfUsers(List<String> userNames);
    
    /**
     * This method returns all users contained in database
     * @return 
     */
    @Transactional
    public List<Users> findAllUsers();
    
    /**
     * Return all usernames stored in database
     * @return 
     */
    @Transactional
    public List<String> findAllaUsernames();
    
    /**
     * This function finds all users with role = "ROLE_USER"
     * @return 
     */
    @Transactional
    public List<Users> findAllUserWithRoleUser();
    
    /**
     * This function save a new user in database
     * @param newUser
     * @return 
     */
    @Transactional 
    public Boolean saveNewUser(Users newUser);
    
    /**
     * This function converts a list of users object into a list of usersDTO objects.
     * It converts all informations we need to display at front-end part except passwords(for security reasons).
     * @param page
     * @param pageSize
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper displayUsers(String username, int page, int pageSize);
        
    /**
     * This function search for the users fulfill the given data
     * @param user
     * @param page
     * @param pageSize
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper searchUser(UsersDTO user, int page, int pageSize);
    
    /**
     * This function finds the role for the given user
     * @param userName
     * @return 
     */
    @Transactional
    public String findUsersRole(String userName);
    
    /**
     * This function return user's password depending on username
     * @param userName
     * @return 
     */
    @Transactional
    public String findUsersPasswordByUserName(String userName);
    
    /**
     * This function change user status (enable/disable). It takes as parameters:
     * @param username
     * @param status
     * @return 
     */
    @Transactional
    public Boolean changeUserStatis(String username, Boolean status);
    
    /**
     * This function is used to delete a user
     * @param username
     * @return 
     */
    @Transactional
    public Boolean deleteUser(String username);
    
    /**
     * This function update a user
     * @param user
     * @return 
     */
    @Transactional 
    public Boolean  updateUser(Users user);
    
    /**
     * This function update's a list of users
     * @param users 
     */
    @Transactional
    public void updateAListOfUsers(List<Users> users);
    
    /**
     * This function call when a new measurement is created. The purpose is to give the proper rights to users.
     * @param creator
     * @param users
     * @param measurements
     * @return 
     */
    @Transactional
    public Boolean bindUsersWithAMeasurement(Users creator, List<Users> users, Measurements measurements);
    
    /**
     * This function finds all users with Role = user and enabled = true, exept from the current logged-in user
     * @param username
     * @param pageable
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper findAllEnabledUsersExceptCurrentUserAndAdministrators(String username, Pageable pageable);
    
    /**
     * This function behave like the one above except that search by matching 
     * the given searchData value with the username, if searchData is like 
     * username the get username.
     * @param currentUser
     * @param searchData
     * @param pageable
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper findAllEnabledUsersExceptCurrentUserAndAdministratorsWithUserNameLike(String currentUser, String searchData, Pageable pageable);
    
    /**
     * Return all users linked with a specific measurement
     * @param measurement
     * @param pageable
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper findAllUsersLinkedWithAMeasurement(Measurements measurement, Pageable pageable);
    
    /**
     * Return all users who are not linked with the given measurement
     * @param measurement
     * @param searchData
     * @param pageable
     * @return 
     */
    @Transactional
    public GlobalRequestDataGridWrapper findAllUsersWhoAreNotLinkedWithAMeasurement(Measurements measurement, String searchData, Pageable pageable);
}
