/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services.Impl;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.db.repositories.UserMeasurementsRepository;
import com.university.graduatemavenproject.db.repositories.UsersRepository;
import com.university.graduatemavenproject.db.repositories.specs.UserSearchFormSpecs;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.transactional.UsersDTO;
import com.university.graduatemavenproject.services.MeasurementsService;
import com.university.graduatemavenproject.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author sotiris
 */
@Service("userService")
public class UserServiceImpl implements UserService{
    
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Autowired
    UsersRepository usersRepository;
    
    @Autowired
    MeasurementsService measurementsService;
    
    @Autowired
    UserMeasurementsRepository userMeasurementsRepository;
    
    /**
     * 
     * @param username
     * @return 
     */
    @Override
    public Users findUser(String username) {
        logger.info("Start UserService.findAllUsers");
        logger.info("End UserService.findAllUsers");
        return usersRepository.findOne(username);
    }
    
    /**
     * 
     * @param userNames
     * @return 
     */
    @Override
    public List<Users> findAListOfUsers(List<String> userNames) {
        logger.info("Start UserService.findAListOfUsers");
        List<Users> users = new ArrayList<Users>();
        try {
            users = usersRepository.findAll(userNames);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return users;
        }
        logger.info("End UserService.findAListOfUsers");
        return users;
    }
    /**
     * 
     * @return 
     */
    @Override
    public List<Users> findAllUsers() {
        logger.info("Start UserService.findAllUsers");
        logger.info("End UserService.findAllUsers");
        return usersRepository.findAll();
    }
    
    
    /**
     * 
     * @return 
     */
    @Override
    public List<String> findAllaUsernames() {
        logger.info("Start UserService.findAllaUsernames");
        logger.info("End UserService.findAllaUsernames");
        return usersRepository.findAllUserNames();
    }
    
    /**
     * 
     * @return 
     */
    @Override
    public List<Users> findAllUserWithRoleUser() {
        List<Users> usersList = new ArrayList<Users>();
        try {
            usersList = usersRepository.findByRole("ROLE_USER");
        } catch(Exception e) {
            logger.error("Error: " + e.getMessage());
            return usersList;
        }
        return usersList;
    }
    
    /**
     * 
     * @param user
     * @return 
     */
    @Override
    public Boolean saveNewUser(Users user) {
        logger.info("Start UserService.saveNewUser");
        try {
            usersRepository.saveAndFlush(user);
        }catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return false;
        }
        logger.info("End UserService.saveNewUser");
        return true;
    }
    
    /**
     * 
     * @param page
     * @param pageSize
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper displayUsers(String username, int page, int pageSize) {
       logger.info("Start UserService.displayUsers");
       //create the response object
       GlobalRequestDataGridWrapper data = new GlobalRequestDataGridWrapper();
       
       int firstResult = (page == 1) ? 0 : (page - 1) * pageSize;
       
       //sorting options
       Sort sort =  new Sort(Sort.Direction.ASC, "userName");
       //create pageable object
       Pageable pageable = new PageRequest((page - 1), pageSize, sort);
       
       Page<Users> usersPage = usersRepository.findByUserNameNot(username, pageable);
       
       if (usersPage == null || usersPage.getContent().isEmpty()) {
           data.setRows(new ArrayList());
           data.setTotal(0);
           return data;
       }
       
       data.setTotal((int) usersPage.getTotalElements());
       
       List<UsersDTO> rowsData = new ArrayList<UsersDTO>();
       int i = 0;
       for (i = 0; i < usersPage.getNumberOfElements(); i++) {
           UsersDTO user = new UsersDTO();
           user.setUserName(usersPage.getContent().get(i).getUserName());
           user.setEnabled(String.valueOf(usersPage.getContent().get(i).getEnabled()));
           user.setRole(usersPage.getContent().get(i).getRole());
           rowsData.add(user);
       }
       data.setRows(rowsData);
       
       logger.info("End UserService.displayUsers");
       return data;
    }
    
    /**
     * 
     * @param user
     * @param page
     * @param pageSize
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper searchUser(UsersDTO user, int page, int pageSize) {
        logger.info("Start UserService.searchUser");
        GlobalRequestDataGridWrapper dataBaseResponse = new GlobalRequestDataGridWrapper();
        
        int firstResult = (page == 1) ? 0 : (page - 1) * pageSize;

        //sorting options
        Sort sort =  new Sort(Sort.Direction.ASC, "userName");
        //create pageable object
        Pageable pageable = new PageRequest((page - 1), pageSize, sort);
        //get the data
        Page<Users> usersPage = usersRepository.findAll(UserSearchFormSpecs.searchUser(user), pageable);
        //if null the return the GlobalRequestDataGridWrapper with no data
        if (usersPage == null || usersPage.getContent().isEmpty()) {
            dataBaseResponse.setRows(new ArrayList());
            dataBaseResponse.setTotal(0);
            return dataBaseResponse;
        }
        //else set the approprite data in it
        dataBaseResponse.setTotal((int) usersPage.getTotalElements());

        List<UsersDTO> rowsData = new ArrayList<UsersDTO>();
        int i = 0;
        for (i = 0; i < usersPage.getNumberOfElements(); i++) {
            UsersDTO users = new UsersDTO();
            users.setUserName(usersPage.getContent().get(i).getUserName());
            users.setEnabled(String.valueOf(usersPage.getContent().get(i).getEnabled()));
            users.setRole(usersPage.getContent().get(i).getRole());
            rowsData.add(users);
        }
         dataBaseResponse.setRows(rowsData);
        logger.info("End UserService.searchUser");
        return dataBaseResponse;
    }

    /**
     * 
     * @param userName
     * @return 
     */
    @Override
    public String findUsersRole(String userName) {
        logger.info("Start UserService.findUsersRole");
        logger.info("End UserService.findUsersRole");
        return usersRepository.findUsersRole(userName);
    }

    /**
     * 
     * @param username
     * @param status
     * @return 
     */
    @Override
    public Boolean changeUserStatis(String username, Boolean status) {
        logger.info("Start UserService.changeUserStatis for username: " + username);
        try {
            //find the user
            Users user = findUser(username);
            user.setEnabled(status);
            usersRepository.saveAndFlush(user);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return false;
        }
        logger.info("End UserService.changeUserStatis for username: " + username);
        return true;
    }

    /**
     * 
     * @param username
     * @return 
     */
    @Override
    public Boolean deleteUser(String username) {
        logger.info("Start UserService.deleteUser for username: " + username);
        try {
            usersRepository.delete(username);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return false;
        }
        logger.info("End UserService.deleteUser for username: " + username);
        return true;
    }

    /**
     * 
     * @param userName
     * @return 
     */
    @Override
    public String findUsersPasswordByUserName(String userName) {
        logger.info("Start UserService.fundUsersPasswordByUserName for username: " + userName);
        logger.info("End UserService.fundUsersPasswordByUserName for username: " + userName);
        return usersRepository.findUsersPassword(userName);
    }

    /**
     * 
     * @param user
     * @return 
     */
    @Override
    public Boolean updateUser(Users user) {
        logger.info("Start UserService.updateUser for username: " + user.getUserName());
        try {
            usersRepository.saveAndFlush(user);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return false;
        }
        logger.info("End UserService.updateUser for username: " + user.getUserName());
        return true;
    } 
    
    @Override
    public void updateAListOfUsers(List<Users> users) {
        logger.info("Strat UserService.updateAListOfUsers");
        logger.info("End UserService.updateAListOfUsers");
        usersRepository.save(users);
    }

    @Override
    public Boolean bindUsersWithAMeasurement(Users creator, List<Users> users, Measurements measurements) {
        logger.info("Start UserService.bindUsersWithAMeasurement");
        
        UserMeasurements newAssignment = new UserMeasurements();
        
        if (creator != null) {
            //At first asign the creator user with the new measurement record
            //the four lines of code bellow create a new UserMeasurements object
            newAssignment.setUser(creator);
            newAssignment.setHasAuth(Boolean.TRUE);
            newAssignment.setMeasurement(measurements);

            //and assign it on creator 
            List<UserMeasurements> creatorMeasurements = measurementsService.findMeasurementsSearchingByUser(creator);
            if (CollectionUtils.isEmpty(creatorMeasurements)) {
                creator.setUserMeasurementsList(new ArrayList<UserMeasurements>());
            } else {
                creator.setUserMeasurementsList(creatorMeasurements);
            }

            creator.getUserMeasurementsList().add(newAssignment);

            //then save it
            newAssignment = measurementsService.registerNewMeasurementForUser(newAssignment);

            //update creator user
            this.updateUser(creator);

            measurements.setUserMeasurementsList(new ArrayList<UserMeasurements>());
        }
        
        //then follow the same prosces to assign measurement on the other user, if exists 
        if (users.size() > 0) {
            List<UserMeasurements> userMeasurementsList = new ArrayList<UserMeasurements>();
            for(Users user : users) {
                
                UserMeasurements newUserMeasurement = new UserMeasurements();
                newUserMeasurement.setUser(user);
                newUserMeasurement.setHasAuth(Boolean.FALSE);
                newUserMeasurement.setMeasurement(measurements);
                
                userMeasurementsList.add(newUserMeasurement);
                
                //intilize user's measurements list
                List<UserMeasurements> userMeasurements = measurementsService.findMeasurementsSearchingByUser(user);
                if(CollectionUtils.isEmpty(userMeasurements)) {
                    user.setUserMeasurementsList(new ArrayList<UserMeasurements>());
                } else {
                     user.setUserMeasurementsList(userMeasurements);
                }
                
                user.getUserMeasurementsList().add(newUserMeasurement);
            }
            measurementsService.registerAListOfUserMeasurements(userMeasurementsList);
            
            this.updateAListOfUsers(users);
            
            measurements.setUserMeasurementsList(userMeasurementsList);
        }
        
        if (creator != null) {
            measurements.getUserMeasurementsList().add(newAssignment);
        }
        
        measurementsService.registerMeasurement(measurements);
        
        logger.info("End UserService.bindUsersWithAMeasurement");
        return true;
    }

    /**
     * 
     * @param username
     * @param pageable
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper findAllEnabledUsersExceptCurrentUserAndAdministrators(String username, Pageable pageable) {
        logger.info("Start UserService.findAllEnabledUsersExceptCurrentUser");
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        
        Page<Users> usersPage = usersRepository.findByRoleAndEnabledAndUserNameNot("ROLE_USER", true, username, pageable);
        
        if (usersPage.getTotalElements() == 0) {
            response.setTotal(0);
            response.setRows(new ArrayList());
            return response;
        }
        
        response.setTotal((int) usersPage.getTotalElements());
        
        List<UsersDTO> usersDTOList = new ArrayList<UsersDTO>();
        for (Users user : usersPage.getContent()) {
            UsersDTO userDTO = new UsersDTO();
            userDTO.setUserName(user.getUserName());
            usersDTOList.add(userDTO);
        }
        
        response.setRows(usersDTOList);
        
        logger.info("End UserService.findAllEnabledUsersExceptCurrentUser");
        return response;
    }
    
    @Override
    public GlobalRequestDataGridWrapper findAllEnabledUsersExceptCurrentUserAndAdministratorsWithUserNameLike(String currentUser, String searchData, Pageable pageable) {
        logger.info("Start UserService.findAllEnabledUsersExceptCurrentUserAndAdministratorsWithUserNameLike");
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        Page<Users> usersPage = usersRepository.findAll(new UserSearchFormSpecs().searchNewMeasurementUsers(currentUser, searchData), pageable);
        
        if (usersPage.getTotalElements() == 0) {
            response.setTotal(0);
            response.setRows(new ArrayList());
            return response;
        }
        
        response.setTotal((int) usersPage.getTotalElements());
        
        List<UsersDTO> usersDTOList = new ArrayList<UsersDTO>();
        for (Users user : usersPage.getContent()) {
            UsersDTO userDTO = new UsersDTO();
            userDTO.setUserName(user.getUserName());
            usersDTOList.add(userDTO);
        }
        
        response.setRows(usersDTOList);
        
        logger.info("End UserService.findAllEnabledUsersExceptCurrentUserAndAdministratorsWithUserNameLike");
        return response;
    }
    
    
    @Override
    public GlobalRequestDataGridWrapper findAllUsersLinkedWithAMeasurement(Measurements measurement, Pageable pageable) {
        logger.info("Start UserService.findAllUsersLinkedWithAMeasurement");
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        
        Page<Users> usersPage = usersRepository.findAll(UserSearchFormSpecs.getMeasurementUsers(measurement), pageable);
        
        if (usersPage.getTotalElements() == 0) {
            response.setTotal(0);
            response.setRows(new ArrayList());
            return response;
        }
        
        response.setTotal((int) usersPage.getTotalElements());
        
        List<UsersDTO> usersDTOList = new ArrayList<UsersDTO>();
        for (Users user : usersPage.getContent()) {
            UsersDTO userDTO = new UsersDTO();
            userDTO.setUserName(user.getUserName());
            
            for (UserMeasurements userMeasurement : user.getUserMeasurementsList()) {
                if (userMeasurement.getMeasurement().getId().equals(measurement.getId())) {
                    userDTO.setHasRightsOnMeasurement(userMeasurement.getHasAuth());
                }
            }
            
            usersDTOList.add(userDTO);
        }
        
        response.setRows(usersDTOList);
        
        logger.info("End UserService.findAllUsersLinkedWithAMeasurement");
        return response;
    }

    @Override
    public GlobalRequestDataGridWrapper findAllUsersWhoAreNotLinkedWithAMeasurement(Measurements measurement, String searchData, Pageable pageable) {
        logger.info("Start UserService.findAllUsersLinkedWithAMeasurement");
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        
        //get the userMeasurements list of objects that contain the measurement
        //given above
        List<UserMeasurements> userMeasurementsList = userMeasurementsRepository.findByMeasurement(measurement);
        
        Page<Users> usersPage = usersRepository.findAll(UserSearchFormSpecs.getNotRegisteredMeasurementUsers(userMeasurementsList, searchData), pageable);
        
        if (usersPage.getTotalElements() == 0) {
            response.setTotal(0);
            response.setRows(new ArrayList());
            return response;
        }
        
        response.setTotal((int) usersPage.getTotalElements());
        
        List<UsersDTO> usersDTOList = new ArrayList<UsersDTO>();
        for (Users user : usersPage.getContent()) {
            UsersDTO userDTO = new UsersDTO();
            userDTO.setUserName(user.getUserName());
            usersDTOList.add(userDTO);
        }
        
        response.setRows(usersDTOList);
        
        logger.info("End UserService.findAllUsersWhoAreNotLinkedWithAMeasurement");
        return response;
    }
}
