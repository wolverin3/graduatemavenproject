/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services.Impl;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.db.repositories.PlaceRepository;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.transactional.PlaceDTO;
import com.university.graduatemavenproject.services.PlaceService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Sotiris
 */
@Service("placeService")
public class PlaceServiceImpl implements PlaceService{
    
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Autowired
    PlaceRepository placeRepository;
    
    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public Place findMeasurementPlaceByID(Integer id) {
        logger.info("Start MeasurementsService.findMeasurementPlaceByName");
        logger.info("End MeasurementsService.findMeasurementPlaceByName");
        return placeRepository.findOne(id);
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    @Override
    public Place findMeasurementPlaceByName(String name) {
        logger.info("Start MeasurementsService.findMeasurementPlaceByName");
        logger.info("End MeasurementsService.findMeasurementPlaceByName");
        return placeRepository.findByName(name);
    }
    
    @Override
    public void deleteAPlaceByItsId(Integer placeID) {
        logger.info("Start MeasurementsService.deleteAPlaceByItsId");
        logger.info("End MeasurementsService.deleteAPlaceByItsId");
        placeRepository.delete(placeID);
    }
    
    @Override
    public void deleteAListOfPlacesId(List<Integer> placesId) {
        logger.info("MeasurementsService.deleteAListOfPlacesId");
        for (Integer id : placesId) {
            placeRepository.delete(id);
        }
    }
    
    /**
     * 
     * @param place
     * @return 
     */
    @Override
    public Place registetNewPlace(Place place) {
        logger.info("Start MeasurementsService.registetNewPlace");
        logger.info("End MeasurementsService.registetNewPlace");
        return placeRepository.saveAndFlush(place);
    }

    /**
     * 
     * @param measurement
     * @return 
     */
    @Override
    public List<Place> findPlacesByMeasurement(Measurements measurement) {
        logger.info("PlaceService.findPlacesByMeasurement for measurement with id: " + measurement.getId().toString());
        return placeRepository.findByMeasurement(measurement);
    }

    /**
     * 
     * @param measurement
     * @param pageable
     * @return 
     */
    @Override
    public Page<Place> findPlacesPageByMeasurement(Measurements measurement, Pageable pageable) {
        logger.info("PlaceService.findPlacesPageByMeasurement for measurement with id: " + measurement.getId().toString());
        return placeRepository.findByMeasurement(measurement, pageable);
    }
    
    @Override
    public PlaceDTO convertPlaceToPlaceDTO(Place place) {
        logger.info("Start PlaceService.convertPlaceToPlaceDTO");
        PlaceDTO placeDTO = new PlaceDTO();
        
        placeDTO.setPlaceId(place.getId());
        placeDTO.setPlaceName(place.getName());
        placeDTO.setCoordinatesX(place.getCoordinatesX() != null ? place.getCoordinatesX().floatValue() : 0);
        placeDTO.setCoordinatesY(place.getCoordinatesY() != null ? place.getCoordinatesY().floatValue() : 0);
        logger.info("End PlaceService.convertPlaceToPlaceDTO");
        return placeDTO;
    }

    /**
     * 
     * @param measurement
     * @param pageable
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper getMeasurementPlacesForDataGrid(Measurements measurement, Pageable pageable) {
        logger.info("Start PlaceService.getMeasurementPlacesForDataGrid for measurement with id: " + measurement.getId().toString());
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        
        //get palces page
        Page<Place> placesPage = this.findPlacesPageByMeasurement(measurement, pageable);
        
        //if there are no data in database then:
        if (placesPage == null || placesPage.getTotalElements() == 0) {
            response.setTotal(0);
            response.setRows(new ArrayList<PlaceDTO>());
            return response;
        }
        
        //set the total element
        response.setTotal((int) placesPage.getTotalElements());
        
        List<PlaceDTO> placesList = new ArrayList<PlaceDTO>();
        
        for (Place place : placesPage.getContent()) {
            placesList.add(this.convertPlaceToPlaceDTO(place));
        }
        
        response.setRows(placesList);
        
        logger.info("End PlaceService.getMeasurementPlacesForDataGrid for measurement with id: " + measurement.getId().toString());
        return response;
    }

}
