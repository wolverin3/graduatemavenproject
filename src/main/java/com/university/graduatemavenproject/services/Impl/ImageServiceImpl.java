/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services.Impl;

import com.university.graduatemavenproject.db.entities.Images;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.db.repositories.ImageRepository;
import com.university.graduatemavenproject.services.ImageService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Sotiris
 */
@Service("imageService")
public class ImageServiceImpl implements ImageService{

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Autowired
    ImageRepository imageRepository;
    
    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public Images findImageById(Integer id) {
        logger.info("ImageService.findImageById by id: " + id);
        return imageRepository.findOne(id);
    }

    /**
     * 
     * @param image
     * @return 
     */
    @Override
    public Images registerImage(Images image) {
        logger.info("ImageService.registerImage");
        return imageRepository.saveAndFlush(image);
    }

    @Override
    public List<Images> findImagesByPlace(Place place) {
        logger.info("ImageService.findImagesByPlace");
        return imageRepository.findByPlaceID(place);
    }
    
    @Override
    public List<Integer> findImagesIdsByPlace(Place place) {
        logger.info("ImageService.findImagesIdsByPlace");
        return imageRepository.findImagesIdsByPlace(place);
    }
    
    @Override
    public List<String> findImagesNameByPlace(Place place) {
        logger.info("ImageService.findImagesNameByPlace");
        return imageRepository.findImagesNameByPlace(place);
    }
    
    @Override
    public byte[] getPlaceImageByID(Integer imageId) {
        logger.info("ImageService.getPlaceImageByID");
        Images image = this.findImageById(imageId);
        
        return image.getImage();
    }

    @Override
    public Boolean deleteImagesList(List<Images> images) {
        logger.info("ImageService.deleteImagesList");
        try {
            for (Images image : images) {
                imageRepository.delete(image);
            }
        } catch (Exception e) {
            logger.error("Error while system was trying to delete images: ", e);
            return false;
        }
        return true;
    }

}
