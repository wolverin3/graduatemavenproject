/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.services.Impl;

import com.university.graduatemavenproject.db.entities.MeasurementData;
import com.university.graduatemavenproject.db.entities.MeasurementDataType;
import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.db.repositories.MeasurementDataRepository;
import com.university.graduatemavenproject.db.repositories.MeasurementDataTypeRepository;
import com.university.graduatemavenproject.db.repositories.MeasurementsRepository;
import com.university.graduatemavenproject.db.repositories.UserMeasurementsRepository;
import com.university.graduatemavenproject.db.repositories.specs.MeasurementsSearchFormSpecs;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.GlobalRequestWrapper;
import com.university.graduatemavenproject.model.transactional.DocumentDTO;
import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import com.university.graduatemavenproject.services.ImageService;
import com.university.graduatemavenproject.services.MeasurementsService;
import com.university.graduatemavenproject.services.PlaceService;
import com.university.graduatemavenproject.services.UserService;
import com.university.graduatemavenproject.util.UtilityFunctions;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author sotiris
 */
@Service("measurementsService")
public class MeasurementsServiceImpl implements MeasurementsService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    
    @Autowired
    UserService userService;
    
    @Autowired
    MeasurementsRepository measurementsRepository;
    
    @Autowired
    UserMeasurementsRepository userMeasurementsRepository;
    
    @Autowired
    MeasurementDataTypeRepository measurementDataTypeRepository;
    
    @Autowired
    MeasurementDataRepository measurementDataRepository;
    
    @Autowired
    PlaceService placeService;
    
    @Autowired
    ImageService imageService;
    
    /**
     * 
     * @return 
     */
    @Override
    public List<Measurements> findAllMeasurements() {
        logger.info("Start MeasurementsService.findAllMeasurements");
        List<Measurements> measurementsList = new ArrayList<Measurements>();
        try{
            measurementsList = measurementsRepository.findAll();
        } catch (Exception e) {
           logger.error("Error: " + e.getMessage());
           return measurementsList;
        }
        logger.info("End MeasurementsService.findAllMeasurements");
        return measurementsList;
    }
        
    @Override
    public Measurements findMeasurementById(Integer id) {
        logger.info("MeasurementsService.findMeasurementById");
        return measurementsRepository.findOne(id);
    }

    /**
     * 
     * @param measurement
     * @return 
     */
    @Override
    public MeasurementDTO convertMeasurementsToMeasurementDTO(Measurements measurement, Users user) {
        logger.info("Start MeasurementsService.convertMeasurementsToMeasurementDTO");
        MeasurementDTO measurementDTO = new MeasurementDTO();
        
        measurementDTO.setMeasuremenetID(measurement.getId());
        measurementDTO.setMeasurementName(measurement.getName());
        measurementDTO.setProviders(measurement.getProviders());
        measurementDTO.setCity(measurement.getCity());
//        measurementDTO.setLocationName(measurement.getLocation().getName());
//        if (measurement.getLocation().getCoordinatesX() != null) {
//            measurementDTO.setCoordinatesX(measurement.getLocation().getCoordinatesX().floatValue());
//        }
//        if (measurement.getLocation().getCoordinatesY() != null) { 
//            measurementDTO.setCoordinatesY(measurement.getLocation().getCoordinatesY().floatValue());
//        }
        
        //format the dates to strings
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String creationDateString = formatter.format(measurement.getDateCreated());
        String measurementDateString = formatter.format(measurement.getDateMessurementBe());
        
        measurementDTO.setDateCreated(creationDateString);
        measurementDTO.setMeasurementDate(measurementDateString);
        
        if (user != null) {
            Boolean userRights = this.getUserRightsOnAMeasurement(user, measurement);
            measurementDTO.setUserRole(userRights);
        }
        
        //measurementDTO.setDateCreated(measurement.);
        logger.info("End MeasurementsService.convertMeasurementsToMeasurementDTO");
        return measurementDTO;
    }
    
    @Override
    public GlobalRequestDataGridWrapper searchMeasurements(MeasurementDTO measurementsData, int page, int pageSize) {
        logger.info("Start MeasurementsService.searchMeasurements");
        
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper(); //response object
        List<MeasurementDTO> measurements = new ArrayList<MeasurementDTO>();
        try {
            //compose the pagination object
            //sorting options
            Sort sort =  new Sort(Sort.Direction.ASC, "name");
            //create pageable object
            Pageable pageable = new PageRequest((page - 1), pageSize, sort);

            Page<Measurements> measurementPage = measurementsRepository.findAll(MeasurementsSearchFormSpecs.searchMeasurements(measurementsData), pageable);

            response.setTotal((int) measurementPage.getTotalElements());
            
            if (response.getTotal() == 0) {
                response.setRows(new ArrayList<MeasurementDTO>());
                return response;
            }
            
            if (measurementPage.hasContent()) {
                for (Measurements measurement : measurementPage.getContent()) {
                    measurements.add(this.convertMeasurementsToMeasurementDTO(measurement, measurementsData.getCurrentUser()));
                }

                response.setRows(measurements);
            }
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            response.setTotal(0);
            response.setRows(new ArrayList<MeasurementDTO>());
            return response;
            
        }
        
        logger.info("Start MeasurementsService.searchMeasurements");
        return response;
    }

    /**
     * 
     * @param page
     * @param pageSize
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper getAllMeasurementsForAdmin(int page, int pageSize) {
        logger.info("Start MeasurementsService.getAllMeasurementsForAdmin");
        
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        List<MeasurementDTO> measurements = new ArrayList<MeasurementDTO>();
        
        try {
            //sorting options
            Sort sort =  new Sort(Sort.Direction.ASC, "name");
            //create pageable object
            Pageable pageable = new PageRequest((page - 1), pageSize, sort);
            
            Page<Measurements> measurementPage = measurementsRepository.findAll(pageable);
            
            response.setTotal((int) measurementPage.getTotalElements());
            if (measurementPage.hasContent()) {
                for (Measurements measurement : measurementPage.getContent()) {
                    measurements.add(this.convertMeasurementsToMeasurementDTO(measurement, null));
                }
                
                response.setRows(measurements);
            }
            
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            response.setTotal(0);
            return response;
            
        }
        if (response.getTotal() == 0) {
            response.setRows(new ArrayList<Measurements>());
        }
        logger.info("End MeasurementsService.getAllMeasurementsForAdmin");
        return response;
    }

    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public Boolean deleteMeasurementById(Integer id) {
        logger.info("Start MeasurementsService.deleteMeasurementById with id: " + id);
        try {
            measurementsRepository.delete(id);
        } catch (Exception e) {
            logger.error("Error: " + e.getMessage());
            return false;
        }
        logger.info("End MeasurementsService.deleteMeasurementById with id: " + id);
        return true;
    }
    
    @Override
    public Boolean deleteMeasurementFunctionality(Integer id) {
        try {
            //get measurement
            Measurements measurement = this.findMeasurementById(id);
            //get measurement places
            List<Place> measurementPlacesList = placeService.findPlacesByMeasurement(measurement);
            //remove measurement places
            List<Integer> placesIds = new ArrayList<Integer>();
            for (Place measurementPlace : measurementPlacesList) {
                placesIds.add(measurementPlace.getId());
            }

            if (!placesIds.isEmpty()) {
                placeService.deleteAListOfPlacesId(placesIds);
            }

            this.deleteMeasurementById(id);
        
        } catch (Exception e) {
            logger.error("An error occured while deleting measurement with id: " + id + " and its data");
            return false;
        }
        
        return true;
    }

    /**
     * 
     * @param user
     * @param page
     * @param pageSize
     * @return 
     */
    @Override
    public GlobalRequestDataGridWrapper findUsersMeasurementsByUser(Users user, int page, int pageSize) {
        logger.info("Start MeasurementsService.findUsersMeasurementsByUser");
        
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper(); //response object
        
        //compose the pagination object
        //sorting options
        Sort sort =  new Sort(Sort.Direction.ASC, "id");
        //create pageable object
        Pageable pageable = new PageRequest((page - 1), pageSize, sort);
        
         
        
        try{
            Page<UserMeasurements> userMeasurementsPage = userMeasurementsRepository.findByUser(user, pageable);
            
            response.setTotal((int) userMeasurementsPage.getTotalElements());
            
            //if there are no any data return 
            if(response.getTotal() == 0) {
                response.setRows(new ArrayList<MeasurementDTO>());
                return response;
            } 
            //else
            
            //get the data 
            List<MeasurementDTO> rowsData = new ArrayList<MeasurementDTO>();
            //format the dates to strings
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            
            for (UserMeasurements userMeasurement : userMeasurementsPage.getContent()) {
                MeasurementDTO measurement = new MeasurementDTO();
                
                Measurements userMeas = userMeasurement.getMeasurement();
                
                measurement.setMeasuremenetID(userMeas.getId());
                measurement.setMeasurementName(userMeas.getName());
                measurement.setDateCreated(formatter.format(userMeas.getDateCreated()));
                measurement.setMeasurementDate(formatter.format(userMeas.getDateMessurementBe()));
                measurement.setProviders(userMeas.getProviders());
                measurement.setUserRole(userMeasurement.getHasAuth());
                
                rowsData.add(measurement);
            }
            
            response.setRows(rowsData);
            
            
        } catch (Exception e) {
           logger.error("Error: " + e.getMessage());
           response.setTotal(0);
           return response;
        }
        logger.info("End MeasurementsService.findUsersMeasurementsByUser");
        return response;
    }
    
    @Override
    public Integer createNewMeasurementAction(Users creator, List<Users> users, MeasurementDTO newMeasurementFormData) {
        logger.info("Start MeasurementsService.createNewMeasurementAction");
        Measurements registeredMeasurements = this.configureMeasurement(newMeasurementFormData, false);
        
        //save new Measurement
        if (registeredMeasurements != null) {
            userService.bindUsersWithAMeasurement(creator, users, registeredMeasurements);
        }
        logger.info("End MeasurementsService.createNewMeasurementAction");
        return registeredMeasurements.getId();
    }
    
    /**
     * 
     * @param measurement
     * @return 
     */
    @Override
    public Measurements registerMeasurement(Measurements measurement) {
        logger.info("Start MeasurementsService.registerMeasurement");
        logger.info("End MeasurementsService.registerMeasurement");
        return measurementsRepository.save(measurement);
    }
    
    /**
     * 
     * @param measurement
     * @return 
     */
    @Override
    public Measurements updateMeasurement(Measurements measurement) {
        logger.info("Start MeasurementsService.updateMeasurement");
        logger.info("End MeasurementsService.updateMeasurement");
        return measurementsRepository.saveAndFlush(measurement);
    }

    /**
     * 
     * @param newMeasurementData
     * @return 
     */
    @Override
    public Measurements configureMeasurement(MeasurementDTO newMeasurementData, Boolean forUpdate) {
        logger.info("Start MeasurementsService.registerNewMeasurement");
        Measurements registeredMeasurement = new Measurements();
        Measurements newMeasurement;
        Place measurementPlace = new Place();
        try {
            if (!forUpdate) {
                newMeasurement = new Measurements();
                newMeasurement.setDateCreated(new Date());
                measurementPlace = new Place();
                measurementPlace.setName(newMeasurementData.getLocationName());
                if (newMeasurementData.getCoordinatesX() != null) {
                    measurementPlace.setCoordinatesX(new BigDecimal(newMeasurementData.getCoordinatesX()));
                }
                if (newMeasurementData.getCoordinatesY() != null) {
                    measurementPlace.setCoordinatesY(new BigDecimal(newMeasurementData.getCoordinatesY()));
                }
                newMeasurement.setPlaceList(new ArrayList<Place>());
                newMeasurement.getPlaceList().add(measurementPlace);
                measurementPlace.setMeasurement(newMeasurement);
                
            } else {
                newMeasurement = this.findMeasurementById(newMeasurementData.getMeasuremenetID());
            }
            //create the new measurement object 
            
            newMeasurement.setName(newMeasurementData.getMeasurementName());
            newMeasurement.setDateMessurementBe(UtilityFunctions.stringToDate(newMeasurementData.getMeasurementDate(), "dd/MM/yyyy"));

            newMeasurement.setCity(newMeasurementData.getCity());
            newMeasurement.setProviders(newMeasurementData.getProviders());

            if (forUpdate) {
                registeredMeasurement = this.updateMeasurement(newMeasurement);
            } else {
                registeredMeasurement = this.registerMeasurement(newMeasurement);
                placeService.registetNewPlace(measurementPlace);
            }
        } catch (Exception e) {
            logger.error("Error while saving new measurement: " + e.getMessage());
            return registeredMeasurement;
        }
        
        logger.info("End MeasurementsService.registerNewMeasurement");
        return registeredMeasurement;
    }

    

    /**
     * 
     * @param userMeasurements
     * @return 
     */
    @Override
    public UserMeasurements registerNewMeasurementForUser(UserMeasurements userMeasurements) {
        logger.info("Start MeasurementsService.registerNewMeasurementForUser");
        logger.info("End MeasurementsService.registerNewMeasurementForUser");
        return userMeasurementsRepository.save(userMeasurements);
    }

    @Override
    public void registerAListOfUserMeasurements(List<UserMeasurements> userMeasurementsList) {
        logger.info("Start MeasurementsService.registerAListOfUserMeasurements");
        userMeasurementsRepository.save(userMeasurementsList);
        logger.info("End MeasurementsService.registerAListOfUserMeasurements");
    }

    /**
     * 
     * @param user
     * @return 
     */
    @Override
    public List<UserMeasurements> findMeasurementsSearchingByUser(Users user) {
        logger.info("Start MeasurementsService.findMeasurementsSearchingByUser");
        List<UserMeasurements> usersMeasurements = userMeasurementsRepository.findByUser(user);
        logger.info("End MeasurementsService.findMeasurementsSearchingByUser");
        return usersMeasurements;
    }

    @Override
    public void deletePlace(Integer placeID) {
        logger.info("Start MeasurementsService.deletePlace");
        logger.info("End MeasurementsService.deletePlace");
        placeService.deleteAPlaceByItsId(placeID);
    }

    @Override
    public Integer findPlaceByMeasurementID(Integer measurementID) {
        logger.info("Start MeasurementsService.findPlaceByMeasurementID");
        logger.info("End MeasurementsService.findPlaceByMeasurementID");
        return measurementsRepository.getMeasurementPlaceIDByMeasurementID(measurementID);
    }

    @Override
    public void unbindUsersFromMeasurement(List<Users> usersList, Measurements measurement) {
        logger.info("Start MeasurementsService.unbindUsersFromMeasurement");
        //find appropiate UserMeasurement for each User
        List<UserMeasurements> userMeasurementList = new ArrayList<UserMeasurements>();
        
        //populate the list above with data
        for (Users user : usersList) {
            UserMeasurements userMeasurement = userMeasurementsRepository.findByUserAndMeasurement(user, measurement);
            userMeasurementList.add(userMeasurement);
        }
        
        //then delete userMeasurementList's data from db
        this.deleteAListOfUserMeasurements(userMeasurementList);
        
        //delete action
        logger.info("End MeasurementsService.unbindUsersFromMeasurement");
    }
    
    @Override
    public void deleteAListOfUserMeasurements(List<UserMeasurements> userMeasurementsList) {
        logger.info("MeasurementsService.deleteAListOfUserMeasurements");
        userMeasurementsRepository.delete(userMeasurementsList);
    }

    @Override
    public GlobalRequestWrapper updateMeasurementsRightsForAUsersList(Users user, Measurements measurement) {
        logger.info("Start MeasurementsService.deleteAListOfUserMeasurements");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //retrive UserMeasurements from database and change the hasAuth variable
        UserMeasurements userMeasurement;
        
        try {
            
            //get UserMeasurement for the user
            userMeasurement = userMeasurementsRepository.findByUserAndMeasurement(user, measurement);
            //change rights
            userMeasurement.setHasAuth(userMeasurement.getHasAuth() ? Boolean.FALSE : Boolean.TRUE);
            
            userMeasurementsRepository.saveAndFlush(userMeasurement);
                
        } catch (Exception e) {
            logger.error("Exceptio while chane users tights in a measurement: ", e.toString());
            response.setSuccess(Boolean.FALSE);
            response.setMessage("A server error occured while commited the transaction.");
            return response;
        } 
        
        response.setSuccess(Boolean.TRUE);
        response.setData(userMeasurement.getHasAuth());
        
        logger.info("End MeasurementsService.deleteAListOfUserMeasurements");
        return response;
    }

    @Override
    public UserMeasurements findUserMeasurementByUserAndMeasurement(Users user, Measurements measurement) {
        logger.info("MeasurementsService.findUserMeasurementByUserAndMeasurement");
        return userMeasurementsRepository.findByUserAndMeasurement(user, measurement);
    }
    
    @Override
    public Boolean getUserRightsOnAMeasurement(Users user, Measurements measurement) {
        logger.info("MeasurementsService.findUserMeasurementByUserAndMeasurement");
        return userMeasurementsRepository.getUserRightsOnMeasurement(user, measurement);
    }

    @Override
    public MeasurementDataType getMeasurementDataTypeByName(String measurementDataType) {
        logger.info("MeasurementsService.getMeasurementDataTypeByName");
        return measurementDataTypeRepository.findByName(measurementDataType);
    }
    
    @Override
    public MeasurementDataType getMeasurementDataTypeByMeasurementData(MeasurementData measurementData) {
        logger.info("MeasurementsService.getMeasurementDataTypeByMeasurementData");
        return measurementDataTypeRepository.findmesurementDataTypeByMeasurementData(measurementData);
    }

    @Override
    public MeasurementData registerMeasurementData(MeasurementData mesurementData) {
        logger.info("MeasurementsService.registerMeasurementData");
        return measurementDataRepository.saveAndFlush(mesurementData);
    }
    
    @Override
    public void deleteMeasurementDataSearchingByIdList(List<Integer> measurementDataIdList) {
        logger.info("MeasurementsService.deleteMeasurementDataSearchingByIdList");
        measurementDataRepository.deleteFromMeasurementDataByIdList(measurementDataIdList);
    }
    
    @Override
    public GlobalRequestDataGridWrapper getMeasurementDocuments(Integer measurementId, Pageable pageable) {
        logger.info("Satrt MeasurementsService.getMeasurementDocuments for measurement with id: " + measurementId);
        GlobalRequestDataGridWrapper response = new GlobalRequestDataGridWrapper();
        
        Measurements measurement = this.findMeasurementById(measurementId);
        
        Page<MeasurementData> measurementsDocuments = measurementDataRepository.findByMesurementID(measurement, pageable);
        
        //if there are no data yet in database then return
        if (measurementsDocuments.getTotalElements() == 0L) {
            response.setTotal(0);
            response.setRows(new ArrayList<DocumentDTO>());
            return response;
        }
        
        //else
        response.setTotal((int) measurementsDocuments.getTotalElements());
        response.setRows(new ArrayList<DocumentDTO>());
        
        for (MeasurementData document : measurementsDocuments) {
            response.getRows().add(convertMeasurementDataToDocumentDTO(document));
        }
        
        logger.info("End MeasurementsService.getMeasurementDocuments");
        return response;
    }
    
    
    @Override
    public DocumentDTO convertMeasurementDataToDocumentDTO(MeasurementData measurementData) {
        logger.info("Start MeasurementsService.convertMeasurementDataToDocumentDTO");
        DocumentDTO doc = new DocumentDTO();
        
        doc.setId(measurementData.getId());
        doc.setName(measurementData.getFileName());
        doc.setFileType(measurementData.getFileType());
        
        doc.setDocumentType(measurementData.getMeasurementDataType().getName());
        logger.info("End MeasurementsService.convertMeasurementDataToDocumentDTO");
        return doc;
    }
    
    @Override
    public MeasurementData fingMeasurementDataById(Integer measurementDataId) {
        logger.info("MeasurementsService.fingMeasurementDataById");
        return measurementDataRepository.findOne(measurementDataId);
    }

    @Override
    public List<UserMeasurements> findMeasurementUserMeasurements(Measurements measurement) {
        return userMeasurementsRepository.findByMeasurement(measurement);
    }
}
