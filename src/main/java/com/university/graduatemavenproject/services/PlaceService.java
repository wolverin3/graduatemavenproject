/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.services;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.transactional.PlaceDTO;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sotiris
 */
public interface PlaceService {
    
    /**
     * This function returns a place object from database, searching by id
     * @param id
     * @return 
     */
    @Transactional
    public Place findMeasurementPlaceByID(Integer id);
    
    /**
     * This function returns a place object from database, searching by name
     * @param name
     * @return 
     */
    @Transactional
    public Place findMeasurementPlaceByName(String name);
    
    /**
     * This function register a new Place record in database
     * @param place
     * @return 
     */
    @Transactional
    public Place registetNewPlace(Place place);
    
    /**
     * Delete a Place from database searching by its ID
     * @param placeID 
     */
    @Transactional
    public void deleteAPlaceByItsId(Integer placeID);
    
    /**
     * Delete a list of places searching by id from database
     * @param places 
     */
    @Transactional
    public void deleteAListOfPlacesId(List<Integer> placesId);
    
    /**
     * This function return a List of Place objects, which are linked with the
     * Measurement given
     *
     * @param measurement
     * @return
     */
    @Transactional
    List<Place> findPlacesByMeasurement(Measurements measurement);

    /**
     * This function return a specific number of Place objects, which are linked
     * with the Measurement given
     *
     * @param measurement
     * @param pageable
     * @return
     */
    @Transactional
    Page<Place> findPlacesPageByMeasurement(Measurements measurement, Pageable pageable);
    
    /**
     * This method convert a Place object to PlaceDTO
     * @param place
     * @return 
     */
    PlaceDTO convertPlaceToPlaceDTO(Place place);
    
    /**
     * This method return a response an object GlobalRequestDataGridWrapper, 
     * which contains the number of Places and the PlaceDTO object that are 
     * going to store on the data-grid
     * 
     * @param measurement
     * @param pageable
     * @return 
     */
    GlobalRequestDataGridWrapper getMeasurementPlacesForDataGrid(Measurements measurement, Pageable pageable);
}
