/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author sotiris
 */
@Controller
@RequestMapping("/index")
public class LogInController {
    
    private static final Logger logger = LoggerFactory.getLogger(LogInController.class);
    
   @RequestMapping(method = RequestMethod.GET) 
   public String logIn(Model model) {
       logger.info("Start WinR Application logIn");
       logger.info("End WinR Application logIn");
       return "index";      
   } 
    
}
