/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.web;

import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.GlobalRequestWrapper;
import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import com.university.graduatemavenproject.model.transactional.UserConfigurationDTO;
import com.university.graduatemavenproject.model.transactional.UsersDTO;
import com.university.graduatemavenproject.services.MeasurementsService;
import com.university.graduatemavenproject.services.UserService;
import com.university.graduatemavenproject.web.validators.AddUserValidator;
import com.university.graduatemavenproject.web.validators.ConfigureUserFormValidator;
import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author wolverine
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    
    private static final Logger logger = LoggerFactory.getLogger(LogInController.class);
    
    @Autowired
    UserService userService;
    
    @Autowired
    AddUserValidator addUserValidator;
    
    @Autowired
    ConfigureUserFormValidator configureUserFormValidator;
    
    @Autowired
    MeasurementsService measurementsService;
    //This variable is used to store the current user's username
    String username;
    
    /**
     * 
     * This function initialize/update the data-grid with users info
     * @param searchUserData
     * @param page
     * @param pageNum
     * @return 
     * @throws java.io.IOException 
     */
    @RequestMapping(value = "/getUsersInfos", method = RequestMethod.POST)
    public @ResponseBody String getUsersInfos(@ModelAttribute("searchUserFormData") UsersDTO searchUserData,
            @RequestParam (value="page", required = true, defaultValue = "1") int page, 
            @RequestParam (value="rows", required = true, defaultValue = "10") int pageNum) throws IOException {
        logger.info("Start UserPageController.getUsersInfos");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        //set the response data
        GlobalRequestDataGridWrapper responseData = new GlobalRequestDataGridWrapper();
        if (searchUserData.isSearch()) {
            responseData = userService.searchUser(searchUserData, page, pageNum);
        } else {
            responseData = userService.displayUsers(username, page, pageNum); 
        }
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UserPageController.getUsersInfos");
        return ow.writeValueAsString(responseData);
    }
    
    /**
     * this controller is to reply on request from admin user to enable or disable a user
     * @param userName
     * @param status
     * @return 
     */
    @RequestMapping(value="/changeUserStatus",  method = RequestMethod.POST)
    public @ResponseBody String changeUserState(@RequestParam (value= "username") String userName,
            @RequestParam(value="status") String status) throws IOException {
        logger.info("Start UserPageController.changeUserState");
        GlobalRequestWrapper response = new GlobalRequestWrapper(); //create the response object where the data are store
        
        Boolean currentUserStatus = true;
        if(status.equals("Enabled")) {
            currentUserStatus = false;
        }
        //call the service
        Boolean dataBaseExchange = userService.changeUserStatis(userName, currentUserStatus);
        
        response.setSuccess(dataBaseExchange);
        response.setData((dataBaseExchange && !currentUserStatus)? "Disabled" : "Enabled");
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("Start UserPageController.changeUserState");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This function receive a request from an admin user to delete a user
     * @param username
     * @return 
     */
    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
   public @ResponseBody String deleteSelectedUser(@RequestParam (value= "username") String username) {
       logger.info("Start UserPageController.deleteSelectedUser"); 
       //seperate username from = and delete the user with the specific name
       Boolean deleteUserDataBaseResponse = false;
       deleteUserDataBaseResponse = userService.deleteUser(username);
       //finaly return the value
       logger.info("Start UserPageController.deleteSelectedUser");
       return String.valueOf(deleteUserDataBaseResponse);
   }
   
   /**
    * 
    * @param newUserConfig
    * @return
    * @throws IOException 
    */
   @RequestMapping(value = "/configureUser", method = RequestMethod.POST)
    public @ResponseBody String configureUser(@ModelAttribute (value = "newUserConfig") UserConfigurationDTO newUserConfig) throws IOException {
        logger.info("Start UserPageController.configureUser");
        GlobalRequestWrapper response = new GlobalRequestWrapper(); //create the response object where the data are store
        
        response = configureUserFormValidator.validate(newUserConfig); //this variable is used for response messages
        
        if (response.getSuccess()) {
            Users user = userService.findUser(newUserConfig.getSelectedUser());
            
            if (!newUserConfig.getNewPassword().equals("") && newUserConfig.getUserRole().equals("")) {
                user.setPasswd(newUserConfig.getNewPassword());
            } else if (newUserConfig.getNewPassword().equals("") && !newUserConfig.getUserRole().equals("")) {
                user.setRole(newUserConfig.getUserRole());
            } else {
                user.setPasswd(newUserConfig.getNewPassword());
                        user.setRole(newUserConfig.getUserRole());
            }
            
            Boolean updateSucceed = userService.updateUser(user);
            
            if (!updateSucceed) {
                response.setSuccess(Boolean.FALSE);
                response.setMessage("warning");
                response.setData("An error occured!");
            }
        }
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        logger.info("End UserPageController.configureUser");
        return ow.writeValueAsString(response);
    }
    
    /**
     * Initialize add new user form, in admin panel
     * @param model
     * @return 
     */
    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public String createNewUser(Model model) {
        logger.info("Start UserPageController.userPageController");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        model.addAttribute("newUser", new UserConfigurationDTO());
        model.addAttribute("user", username);
        logger.info("End UserPageController.userPageController");
        return "addUser"; 
    }
    
    /**
     * Here come the post request contains the new user data
     * @param newUser
     * @param errors
     * @return 
     */
    @RequestMapping(value= "/addUser", method = RequestMethod.POST)
    public @ResponseBody String saveNewUser(@ModelAttribute ("newUsersData") UserConfigurationDTO newUser, Errors errors) throws IOException {
        logger.info("Start UserPageController.saveNewUser");
        GlobalRequestWrapper response = new GlobalRequestWrapper(); //the response variable back on front-end piece
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        addUserValidator.validate(newUser, errors);
        if (errors.hasErrors()) {
            response.setSuccess(Boolean.FALSE);
            response.setMessage("Error");
            List<String> errorList = new ArrayList<String>();
            int i = 0;
            for(i = 0; i < errors.getAllErrors().size(); i++) {
                errorList.add(errors.getAllErrors().get(i).getCode().toString());
            }
            response.setData(errorList);
            
            logger.info("End UserPageController.saveNewUser with errors");
            return ow.writeValueAsString(response);
        }
        Users user = new Users();
        user.setUserName(newUser.getSelectedUser());
        user.setPasswd(newUser.getNewPassword());
        user.setRole(newUser.getUserRole());
        user.setEnabled(true);
        response.setSuccess(userService.saveNewUser(user));
        response.setData("User has been added succesfully");
        
        logger.info("End UserPageController.saveNewUser");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller triggered when an admin user access the "View measurements" tab
     * @param model
     * @return 
     */
    @RequestMapping(value = "/viewAllMeasurements", method = RequestMethod.GET)
    public String adminViewAllMeasurements(Model model) {
        logger.info("Start AdminController.adminViewAllMeasurements");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        model.addAttribute("user", username);
        model.addAttribute("searchMeasurements", new MeasurementDTO());
        logger.info("End AdminController.adminViewAllMeasurements");
        return "allMeasurementsView"; 
    }
    
    /**
     * This function is an ajax-call request and retrieves all measurements data, used in data-grid
     * @param searchMeasuresFormData
     * @param page
     * @param pageNum
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/getAllMeasurements", method = RequestMethod.POST)
    public @ResponseBody String getAllMeasurementsInfos(@ModelAttribute("searchMeasuresFormData") MeasurementDTO searchMeasuresFormData,
            @RequestParam (value="page", required = true, defaultValue = "1") int page, 
            @RequestParam (value="rows", required = true, defaultValue = "10") int pageNum) throws IOException {
        logger.info("Start AdminController.getAllMeasurementsInfos");
        
        //set the response data
        GlobalRequestDataGridWrapper responseData = new GlobalRequestDataGridWrapper();
        if (searchMeasuresFormData.isSearch()) {
            responseData = measurementsService.searchMeasurements(searchMeasuresFormData, page, pageNum);
        } else {
            responseData = measurementsService.getAllMeasurementsForAdmin(page, pageNum);
        }
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End AdminController.getAllMeasurementsInfos");
        return ow.writeValueAsString(responseData);
    }
    
    @RequestMapping(value = "/deleteMeasurement", method = RequestMethod.POST)
    public @ResponseBody String deleteSelectedMeasure(@RequestParam (value= "measurementId") Integer measurementId) {
       logger.info("Start AdminController.deleteSelectedMeasure"); 
       Boolean deleteMeasureDataBaseResponse = false;
       deleteMeasureDataBaseResponse = measurementsService.deleteMeasurementFunctionality(measurementId);
       //finaly return the value
       logger.info("Start AdminController.deleteSelectedMeasure");
       return String.valueOf(deleteMeasureDataBaseResponse);
   }
    
}
