/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.web;

import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import com.university.graduatemavenproject.model.transactional.UserConfigurationDTO;
import com.university.graduatemavenproject.model.transactional.UsersDTO;
import com.university.graduatemavenproject.services.UserService;
import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author sotiris
 */
@Controller
@RequestMapping("/userPage")
public class UserPageController {
    
    private static final Logger logger = LoggerFactory.getLogger(LogInController.class);
    
    @Autowired
    UserService userService;
    
    //we will use this variable to store the username of the user
    String username;
    
    
     /**
     * This function triggered after users log-in and is responsible for users page display 
     * @param model
     * @param principal
     * @param request
     * @return 
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView userPageController(ModelAndView model, Principal principal, HttpServletRequest request) {
        logger.info("Start UserPageController.userPageController");
        username = principal.getName();
        
        model = new ModelAndView();
        
        String role = userService.findUsersRole(username).equals("ROLE_ADMIN") ? "admin" : "user";
        
        request.getSession().setAttribute("user_role", role);
        request.getSession().setAttribute("user_name", username);
        
        //initialize form attribute
        model.addObject("user", username);
        model.addObject("role", role);
        
        if (role.equals("admin")) {
            model.addObject("searchUser", new UsersDTO()); //instantiate a model for search users form
            model.addObject("configUser", new UserConfigurationDTO()); 
        } else {
            model.addObject("searchMeasurements", new MeasurementDTO());
            logger.info("End MainPageController.userPageController");
            model.setViewName("userPage");
            return model;
        }
        model.setViewName("adminPage");
        logger.info("End UserPageController.userPageController");
        return model;
    }
}   
