package com.university.graduatemavenproject.web;

import com.sun.org.apache.xerces.internal.impl.dv.xs.DecimalDV;
import com.university.graduatemavenproject.common.enums.FileTypeEnum;
import com.university.graduatemavenproject.db.entities.Images;
import com.university.graduatemavenproject.db.entities.MeasurementData;
import com.university.graduatemavenproject.db.entities.MeasurementDataType;
import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.model.GlobalRequestDataGridWrapper;
import com.university.graduatemavenproject.model.GlobalRequestWrapper;
import com.university.graduatemavenproject.model.transactional.ImageDTO;
import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import com.university.graduatemavenproject.model.transactional.PlaceDTO;
import com.university.graduatemavenproject.services.ImageService;
import com.university.graduatemavenproject.services.MeasurementsService;
import com.university.graduatemavenproject.services.PlaceService;
import com.university.graduatemavenproject.services.UserService;
import com.university.graduatemavenproject.web.validators.NewMeasurementValidator;
import com.university.graduatemavenproject.web.validators.UpdateMeasurementData;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author sotiris
 */
@Controller
@RequestMapping("/user")
public class UsersController {
    
    private static final Logger logger = LoggerFactory.getLogger(LogInController.class);
    
    @Autowired
    MeasurementsService measurementsService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    PlaceService placeService;
    
    @Autowired
    ImageService imageService;
    
    @Autowired
    NewMeasurementValidator newMeasurementValidator;
    
    @Autowired
    UpdateMeasurementData updateMeasurementData;

    
    //This variable is used to store the current user's username
    String username;
    
    /**
     * Get the data for the user's data-grid where displayed user's measurements
     * @param searchMeasuresFormData
     * @param page
     * @param pageNum
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/getUserMeasurements", method = RequestMethod.POST)
    public @ResponseBody String getUserMeasurementsInfos(@ModelAttribute("searchMeasuresFormData") MeasurementDTO searchMeasuresFormData,
            @RequestParam (value="page", required = true, defaultValue = "1") int page, 
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageNum) throws IOException {
        logger.info("Start UsersController.getUserMeasurementsInfos");
        //set the response data
        GlobalRequestDataGridWrapper responseData;
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        
        Users curretnUser = userService.findUser(username);
        
        searchMeasuresFormData.setCurrentUser(curretnUser);
        
        if (searchMeasuresFormData.isSearch()) {
            responseData = measurementsService.searchMeasurements(searchMeasuresFormData, page, pageNum);
        } else {
            responseData = measurementsService.findUsersMeasurementsByUser(curretnUser, page, pageNum);
        }
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.getUserMeasurementsInfos");
        return ow.writeValueAsString(responseData);
    }
    
    /**
     * initialise the form
     * @param model
     * @param hasError
     * @param isNotNew
     * @param infoMessage
     * @return 
     */ 
    @RequestMapping(value = "/addProject", method = RequestMethod.GET)
    public String addMeasurement(Model model, boolean hasError, boolean isNotNew, String infoMessage) {
        logger.info("Start UsersController.addMeasurement");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        
        model.addAttribute("newMeasurement", new MeasurementDTO());
        model.addAttribute("user", username);
        model.addAttribute("isNotNew", isNotNew);
        model.addAttribute("hasError", hasError);
        model.addAttribute("infoMessage", infoMessage);
        logger.info("End UsersController.addMeasurement");
        return "newMeasurement";
    }
    
    /**
     * Populate data-grid in "New measurement" page with all users available,
     * except administrators, current and disabled. 
     * @param searchData
     * @param page
     * @param isSearch
     * @param pageSize
     * @param sortColumn
     * @param sortDirection
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/findClientsForNewMeasurement", method = RequestMethod.POST)
    public @ResponseBody String getNewMeasurementUsers(@RequestParam (value="searchData", required = false, defaultValue = "") String searchData,
            @RequestParam (value="search", required = false) Boolean isSearch,
            @RequestParam (value="page", required = true, defaultValue = "1") int page,
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageSize,
            @RequestParam (value = "sort", required = false, defaultValue = "userName") String sortColumn,
            @RequestParam(value = "order", required = false, defaultValue = "ASC") String sortDirection) throws IOException{
        logger.info("Start UsersController.getNewMeasurementUsers");
        GlobalRequestDataGridWrapper response ;
        //get all users except admins and current user;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        
        isSearch = isSearch == null ? false : isSearch;
        
        //create a sort instance
        Sort sort = new Sort(sortColumn);
        //define paging
        PageRequest paging = new PageRequest((page == 0 ? page : page - 1), pageSize, sort);
        Pageable pageable = paging;
        
        if (isSearch) {
            response = userService.findAllEnabledUsersExceptCurrentUserAndAdministratorsWithUserNameLike(username, searchData, pageable);
        } else {
            response = userService.findAllEnabledUsersExceptCurrentUserAndAdministrators(username, pageable); // all enbled users with user-role "ROLE_USER"
        }
            
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        logger.info("End UsersController.getNewMeasurementUsers");
        return ow.writeValueAsString(response);
    }
    
    
    /**
     * When the user click the "Add" button in "New measurement" page then,
     * fire-up this method. It's usage is to create a new measurement and bind
     * on it the creator and the selected users form data-grid
     * 
     * @param newMeasurementFormData
     * @param result
     * @param model
     * @return
     * @throws java.io.IOException
     */
    @RequestMapping(value = "/addProject", method = RequestMethod.POST)
    public @ResponseBody  String saveNewMeasurement(@ModelAttribute("newMeasurementFormData") MeasurementDTO newMeasurementFormData,
            BindingResult result, Model model) throws IOException {
        logger.info("Start UsersController.saveNewMeasurement");
        //set a value to log the response from database
        Boolean databaseResponse;
        
        //set a response variable
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //get current user
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        Integer measurementId = 0;
        //validate
        newMeasurementValidator.validate(newMeasurementFormData, result);
        //if result has errors re-order the values for "new measurement" window as the above method  ("addMeasurement")
        if(result.hasErrors()) {
            response.setSuccess(false);
            response.setMessage("Mandatory fileds should not be empty");
        } else {
            //get current user
            Users user = userService.findUser(username);
            //get the list of users who have been appended on the measurement project
            List<Users> usersList = userService.findAListOfUsers(newMeasurementFormData.getUsersIDs());
            measurementId = measurementsService.createNewMeasurementAction(user, usersList, newMeasurementFormData);
            databaseResponse = (measurementId != null && measurementId != 0);
            
            response.setSuccess(databaseResponse);
            response.setMessage(databaseResponse ? "/winr/user/measurement/" + String.valueOf(measurementId) + ".action" : "A server error ocured");
        }
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.saveNewMeasurement");//format response data to json form
        return ow.writeValueAsString(response);
    }
    
    /**
     * Trigger delete measurement functionality. It gets as parameter a measurement id.
     * 
     * @param measurementId
     * @return 
     */
    @RequestMapping(value = "/deleteMeasurement", method = RequestMethod.POST)
    public @ResponseBody String deleteSelectedMeasure(@RequestParam (value= "measurementId") Integer measurementId) {
       logger.info("Start UsersController.deleteSelectedMeasure"); 
       Boolean deleteMeasureDataBaseResponse = false;
       deleteMeasureDataBaseResponse = measurementsService.deleteMeasurementFunctionality(measurementId);
       //finaly return the value
       logger.info("End UsersController.deleteSelectedMeasure");
       return String.valueOf(deleteMeasureDataBaseResponse);
    }
    
    /**
     * This controller's method navigate the user into the selected measurements
     * details page
     * @param id
     * @param model
     * @param hasError
     * @param isNotNew
     * @param infoMessage
     * @param request
     * @return 
     */
    @RequestMapping(value = "/measurement/{id}", method = RequestMethod.GET)
    public String measurementDetails(@PathVariable Integer id, Model model, boolean hasError, boolean isNotNew,
            String infoMessage, HttpServletRequest request) {
        logger.info("Start UsersController.measurementDetails");
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        
        Users currentUser = userService.findUser(username);
        
        //fins measurement
        Measurements measurement = measurementsService.findMeasurementById(id);
        MeasurementDTO measurementDTO = measurementsService.convertMeasurementsToMeasurementDTO(measurement, currentUser);
        
        request.getSession().setAttribute("USER_MEASUREMENT_RIGHTS", measurementDTO.isUserRole());
        
        model.addAttribute("user", username);
        model.addAttribute("measurement", measurementDTO);
        logger.info("End UsersController.measurementDetails");
        return "measurementDetails";
    }
    
    /**
     * This method is responsible to change a measurements basic info, such as
     * name, dates, coordinates e.t.c
     * @param newMeasurementFormData
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/updateMeasurement", method = RequestMethod.POST)
    public @ResponseBody String updateSelectedMeasure(@ModelAttribute("newMeasurementFormData") MeasurementDTO newMeasurementFormData, BindingResult results) throws IOException {
        logger.info("Start AdminController.updateSelectedMeasure"); 
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        updateMeasurementData.validate(newMeasurementFormData, results);
        
        if (results.hasErrors()) {
            response.setSuccess(Boolean.FALSE);
            response.setMessage("error");
            response.setData("Mandatory fields should not be empty!");
        } else {

            //update measurement in database
            Measurements updatedMeasurement = measurementsService.configureMeasurement(newMeasurementFormData, Boolean.TRUE);

            if (updatedMeasurement != null) {
                response.setSuccess(Boolean.TRUE);
                response.setMessage("success");
                response.setData(newMeasurementFormData);
            } else {
                response.setSuccess(Boolean.FALSE);
                response.setMessage("warning");
                response.setData("A server error occured!");
            }
        }

        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.updateSelectedMeasure");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller method retrieve all users who are bound with a given 
     * measurement
     * @param formData
     * @param page
     * @param pageSize
     * @param sortColumn
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/getMeasurementUsers", method = RequestMethod.POST)
    public @ResponseBody String getUsersInfos(@ModelAttribute("formData") MeasurementDTO formData,
            @RequestParam (value="page", required = true, defaultValue = "1") int page, 
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageSize,
            @RequestParam (value = "sort", required = false, defaultValue = "userName") String sortColumn) throws IOException {
        logger.info("Start UsersController.getUsersInfos");
        
        GlobalRequestDataGridWrapper response ;
        
        //create a sort instance
        Sort sort = new Sort(sortColumn);
        //define paging
        PageRequest paging = new PageRequest((page == 0 ? page : page - 1), pageSize, sort);
        Pageable pageable = paging;
        
        Measurements currentMeasurement = measurementsService.findMeasurementById(formData.getMeasuremenetID());
        
        response = userService.findAllUsersLinkedWithAMeasurement(currentMeasurement, pageable);
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.getUsersInfos");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller method get the rest of users, who are not bound with the 
     * current measurement that a current user browse in instantly and who are 
     * enabled and not with admin role
     * @param searchData
     * @param measurementId
     * @param page
     * @param pageSize
     * @param sortColumn
     * @param sortDirection
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/getRestOfTheUsers", method = RequestMethod.POST)
    public @ResponseBody String getRestOfUsersForMeasurement(@RequestParam (value="searchData", required = false, defaultValue = "") String searchData,
            @RequestParam (value="currentMeasurementId", required = true) Integer measurementId,
            @RequestParam (value="page", required = true, defaultValue = "1") int page,
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageSize,
            @RequestParam (value = "sort", required = false, defaultValue = "userName") String sortColumn,
            @RequestParam(value = "order", required = false, defaultValue = "ASC") String sortDirection) throws IOException{
        logger.info("Start UsersController.getRestOfUsersForMeasurement");
        GlobalRequestDataGridWrapper response;
        
        //create a sort instance
        Sort sort = new Sort(sortColumn);
        //define paging
        PageRequest paging = new PageRequest((page == 0 ? page : page - 1), pageSize, sort);
        Pageable pageable = paging;
        //retrieve current measurement from db
        Measurements currentMeasurement = measurementsService.findMeasurementById(measurementId);
        //and then get all users who are not admin, are enabled and they are not
        //bound yet with the measurement retieved above
        response = userService.findAllUsersWhoAreNotLinkedWithAMeasurement(currentMeasurement, searchData, pageable);
        
        //format response data to json form
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        logger.info("End UsersController.getRestOfUsersForMeasurement");
        //finaly return the results
        return ow.writeValueAsString(response);
    }
    
    /**
     * When a user with rights wants to add users on a measurement and click on 
     * add button of "Add" of modal window in "measurement details" page, is 
     * triggered the controller method
     * 
     * @param newMeasurementUsers
     * @param measurementId
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/updateMeasurementUsers", method = RequestMethod.POST)
    public @ResponseBody String updateSelectedMeasurementUsers(@RequestParam(value = "usersSelect") List<String> newMeasurementUsers,
            @RequestParam(value = "currentMeasurementId") Integer measurementId) throws IOException {
        logger.info("Start UsersController.updateSelectedMeasurementUsers");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //check if there are any selected data
        if (newMeasurementUsers.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("Select at least one from the users");
        } else {
        
            //get current Measurement
            Measurements measurements = measurementsService.findMeasurementById(measurementId);
            //retrive selected users from db
            List<Users> selectedUsers = userService.findAListOfUsers(newMeasurementUsers);
            //make the appropriate actions to link the users with the measurement
            //and return a boolean response
            Boolean commitedData = userService.bindUsersWithAMeasurement(null, selectedUsers, measurements);

            response.setSuccess(commitedData);
            //if previous db exchange was not successfull the set a message into the
            //the return data
            if (!response.getSuccess()) {
                response.setMessage("A server error occured");
            }
        }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.updateSelectedMeasurementUsers");
        //and return the response
        return ow.writeValueAsString(response);
    }
    
    /**
     * When a user with rights wants to delete users from a measurement then,
     * select the users from data-grid and clicks on delete button. This action
     * trigger this controller method
     * 
     * @param newMeasurementUsers
     * @param measurementId
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/deleteMeasurementUsers", method = RequestMethod.POST)
    public @ResponseBody String deleteSelectedMeasurementUsers(@RequestParam(value = "usersSelect") List<String> newMeasurementUsers,
            @RequestParam(value = "currentMeasurementId") Integer measurementId) throws IOException {
        logger.info("Start UsersController.deleteSelectedMeasurementUsers");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //check if there are any selected data
         if (newMeasurementUsers.isEmpty()) {
            response.setSuccess(false);
            response.setMessage("Select at least one from the users");
        } else {
            try {
                //get current Measurement
                Measurements measurements = measurementsService.findMeasurementById(measurementId);
                if (measurementsService.findMeasurementUserMeasurements(measurements).size() == 1) {
                    response.setSuccess(false);
                    response.setMessage("You are the only user, you cannot delete the measurement");
                    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                    return ow.writeValueAsString(response);
                }
                //retrive selected users from db
                List<Users> selectedUsers = userService.findAListOfUsers(newMeasurementUsers);
                
                //delete action
                measurementsService.unbindUsersFromMeasurement(selectedUsers, measurements);
            } catch (Exception e) {
                logger.error("Error while delete UserMeasurements in DB: ", e.toString());
                response.setSuccess(Boolean.FALSE);
                response.setMessage("A server error occured");
            }
            
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if(newMeasurementUsers.contains(auth.getName())) {
                response.setData(true);
            }
            
            response.setSuccess(Boolean.TRUE);
         }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.deleteSelectedMeasurementUsers");
        //and return the response
        return ow.writeValueAsString(response);
    }
    
    /**
     * When a user clicks on a user's measurement rights then an ajax request 
     * send back and change the right value (read, read/write). 
     * 
     * @param userName
     * @param userMeasurementRigts
     * @param measurementId
     * @param <error>
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/changeUserMeasurementRights", method = RequestMethod.POST)
    public @ResponseBody String modifyUserMeasurementRights(@RequestParam(value = "username") String userName,
            @RequestParam(value = "currentRights") Boolean userMeasurementRigts, @RequestParam(value = "currentMeasurementId") Integer measurementId,
            HttpServletRequest request) throws IOException {
        logger.info("Start UsersController.modifyUserMeasurementRights");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //get current Measurement
        Measurements measurements = measurementsService.findMeasurementById(measurementId);
        //retrive selected users from db
        Users selectedUser = userService.findUser(userName);
        
        //commit the change and get the response
        response = measurementsService.updateMeasurementsRightsForAUsersList(selectedUser, measurements);
        
        //update user measurement rights on system variable
        Boolean x = (Boolean) request.getSession().getAttribute("USER_MEASUREMENT_RIGHTS");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName().equals(userName)){
            request.getSession().setAttribute("USER_MEASUREMENT_RIGHTS", response.getData());
        }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.modifyUserMeasurementRights");
        //finaly return the response
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller return all measurement places displayed in data-grid
     * 
     * @param search
     * @param measurementId
     * @param page
     * @param pageSize
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/getMeasurementPlaces", method = RequestMethod.POST)
    public @ResponseBody String getMeasurementPlaces(@RequestParam (value="search", required = false, defaultValue = "false") boolean search,
            @RequestParam (value="currentMeasurementId", required = true) Integer measurementId,
            @RequestParam (value="page", required = true, defaultValue = "1") int page,
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageSize) throws IOException{
        logger.info("Start UsersController.getMeasurementPlaces");
        GlobalRequestDataGridWrapper responseData = null;
        //create a sort instance
        Sort sort = new Sort("name");
        //define paging
        PageRequest paging = new PageRequest((page == 0 ? page : page - 1), pageSize, sort);
        Pageable pageable = paging;
        
        //get current measurement
        Measurements currentMeasurement = measurementsService.findMeasurementById(measurementId);
        if (!search) {
            responseData = placeService.getMeasurementPlacesForDataGrid(currentMeasurement, pageable);
        }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.getMeasurementPlaces");
        return responseData != null ? ow.writeValueAsString(responseData) : "";
    }
    
    /**
     * This controller is responsible to get the data come from the front-end
     * part which represent a new measurement-place, call the appropriate 
     * functions to create and save this new place object in database. 
     * 
     * @param measurementId
     * @param newPlaceFormData
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/measurement/{measurementId}/newPlace", method = RequestMethod.POST)
    public @ResponseBody String createNewMeasurementPlace(@PathVariable (value="measurementId") Integer measurementId,
            @ModelAttribute("placeData") PlaceDTO newPlaceFormData) throws IOException {
        logger.info("Start UsersController.createNewMeasurementPlace");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //retrieve current measurement
        Measurements measurement = measurementsService.findMeasurementById(measurementId);
        
        //Set the new place
        Place newPlace = new Place();
        newPlace.setMeasurement(measurement);
        newPlace.setName(newPlaceFormData.getPlaceName());
        newPlace.setCoordinatesX(newPlaceFormData.getCoordinatesX() != null ? new BigDecimal(newPlaceFormData.getCoordinatesX()) : new BigDecimal(0));
        newPlace.setCoordinatesY(newPlaceFormData.getCoordinatesY() != null ? new BigDecimal(newPlaceFormData.getCoordinatesY()) : new BigDecimal(0));
        
        List<Place> measurementPlaces = placeService.findPlacesByMeasurement(measurement);
        
        if(measurementPlaces.isEmpty()) {
            measurementPlaces = new ArrayList<Place>();
        }
        measurementPlaces.add(newPlace);
        
        measurement.setPlaceList(measurementPlaces);
        
        //save place
        placeService.registetNewPlace(newPlace);
        
        //update measurement places
        measurementsService.updateMeasurement(measurement);
        
        response.setSuccess(Boolean.TRUE);
        response.setMessage("success");
        response.setData("New place has been created");
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.createNewMeasurementPlace");
        
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller triggered when a user click the delete button on
     * measurement places section
     * 
     * @param selectedPlaces
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/measurement/deletePlaces", method = RequestMethod.POST)
    public @ResponseBody String deletePlaces (@RequestParam(value = "selectedPlaces") List<Integer> selectedPlaces) throws Exception {
        logger.info("Start UsersController.deletePlaces");
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        //then delete places from database
        try {
            placeService.deleteAListOfPlacesId(selectedPlaces);
        } catch (Exception e) {
            logger.error("Error while deleting places: ", e.getMessage());
            response.setSuccess(Boolean.TRUE);
            response.setMessage("warning");
            response.setData("A server error occured.");
            return ow.writeValueAsString(response);
        }
        
        response.setSuccess(Boolean.TRUE);
        response.setMessage("success");
        response.setData("Places deleted successfully.");
        
        logger.info("End UsersController.deletePlaces");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller method is responsible to display properly the measurement's
     * place display page
     * 
     * @param measurementId
     * @param placeId
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping(value = "/measurement/{measurementId}/place/{placeId}", method = RequestMethod.GET)
    public ModelAndView placeDetails(@PathVariable (value="measurementId") Integer measurementId, @PathVariable (value="placeId") Integer placeId, ModelAndView model, HttpServletRequest request) {
        logger.info("Start UsersController.displayLocationOnMaps");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName(); //get logged in username
        
        model = new ModelAndView("measurementPlaceDetails");
        
        Place place = placeService.findMeasurementPlaceByID(placeId);
        
        List<Integer> imagesIds = imageService.findImagesIdsByPlace(place);
        
        if (!imagesIds.isEmpty()) {
        
            List<String> imagesName = imageService.findImagesNameByPlace(place);

            List<ImageDTO> imagesData = new ArrayList<ImageDTO>();

            int counter = 0;

            for(Integer imageId : imagesIds) {
                ImageDTO image = new ImageDTO();
                image.setId(imageId);
                image.setImageName(imagesName.get(counter++));

                imagesData.add(image);
            }
            
            model.addObject("imageName", imagesName);
            model.addObject("imagesData", imagesData);
            model.addObject("hasImages", true);
        } else {
            model.addObject("hasImages", false);
        }
        
        PlaceDTO placeDTO = placeService.convertPlaceToPlaceDTO(place);
        
        
        model.addObject("user", username);
        model.addObject("userMeasurementRights", request.getSession().getAttribute("USER_MEASUREMENT_RIGHTS"));
        model.addObject("placeDetailsForm", placeDTO);
        model.addObject("imagesIds", imagesIds);
        model.addObject("deleteImageFormObjct", new ImageDTO());
        model.addObject("measurementId", measurementId);
        
        logger.info("End UsersController.displayLocationOnMaps");
        return model;
    }
    
    /**
     * This controller handles the request for an image. When a request arrives
     * this controller is responsible to return image data back
     * 
     * @param req
     * @param response
     * @param imageId
     * @throws IOException
     * @throws SQLException 
     */
    @RequestMapping(value = "/measurement/place/image/{imageId}", method = RequestMethod.GET)
    public void getImages(HttpServletRequest req, HttpServletResponse response, @PathVariable (value = "imageId") Integer imageId) throws IOException, SQLException {
        logger.info("Start OrdersController.getImages");
        Images image = imageService.findImageById(imageId);
        byte[] imageData = image.getImage();
        //set header
        response.setHeader("Content-Disposition", "attachment; filename=\"" + imageData.toString() + image.getFileType() + "\"");
        //set content type
        response.setContentType("image/jpeg");
        //response.sendRedirect("")
        response.setContentLength(imageData.length);
        //write the file
        response.getOutputStream().write(imageData);
        //return to view
        response.flushBuffer();
        
        logger.info("End OrdersController.getImages");
    }
    
    /**
     * This method is being triggered when a user tries to change the values of  
     * a Place object.
     * 
     * @param newPlaceFormData
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/measurement/place/{placeId}/update", method = RequestMethod.POST)
    public @ResponseBody String updateMeasurementPlaceData(@ModelAttribute("formData") PlaceDTO newPlaceFormData) throws IOException {
        logger.info("Start UsersController.updateMeasurementPlaceData");
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        //retrieve Place object from database
        Place place = placeService.findMeasurementPlaceByID(newPlaceFormData.getPlaceId());
        
        //validate data:
        if (place == null) { 
            response.setSuccess(Boolean.FALSE);
            response.setMessage("error");
            response.setData("No such place in database, perhaps it has been deleted.");
            
            return ow.writeValueAsString(response);
        }
        
        if (!StringUtils.isNotBlank(newPlaceFormData.getPlaceName())) {
            response.setSuccess(Boolean.FALSE);
            response.setMessage("error");
            response.setData("Place name is a mandatory field!");
            
            return ow.writeValueAsString(response);
        }
        
        if (place.getCoordinatesX() == null) {
            place.setCoordinatesX(BigDecimal.ZERO);
        }
        
        if (place.getCoordinatesY() == null) {
            place.setCoordinatesY(BigDecimal.ZERO);
        }
        
        if(place.getName().equals(newPlaceFormData.getPlaceName()) && place.getCoordinatesX().floatValue() == newPlaceFormData.getCoordinatesX() 
                && place.getCoordinatesY().floatValue() == newPlaceFormData.getCoordinatesY()) {
            response.setSuccess(Boolean.FALSE);
            response.setMessage("warning");
            response.setData("No changes on Place data have been made.");
            
            return ow.writeValueAsString(response);
        }
        
        //if there are no errors:
        place.setName(newPlaceFormData.getPlaceName());
        place.setCoordinatesX(new BigDecimal(newPlaceFormData.getCoordinatesX()));
        place.setCoordinatesY(new BigDecimal(newPlaceFormData.getCoordinatesY()));

        //update object in database
        try {
            placeService.registetNewPlace(place);
        } catch (Exception e) {
            logger.error("Error while update place object: ", e.toString());
            response.setSuccess(Boolean.FALSE);
            response.setMessage("warning");
            response.setData("A server error occured.");
            
            return ow.writeValueAsString(response);
        }
        
        //set the response object
        response.setSuccess(Boolean.TRUE);
        response.setMessage("success");
        response.setData("Place data have been update sucessfuly.");
        
        
        logger.info("End UsersController.updateMeasurementPlaceData");
        //and return
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller handle the image upload. When a user upload an image this
     * controller is responsible to create a new image object and save it 
     * in database
     * 
     * @param request
     * @param placeId
     * @param resp
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/measurement/place/{placeId}/uploadImages", method = RequestMethod.POST)
    public @ResponseBody String handleImageUpload(MultipartHttpServletRequest request, @PathVariable (value = "placeId") Integer placeId,
            HttpServletResponse resp) throws Exception {
        logger.info("Start UsersController.handleImageUpload");
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        ObjectMapper mapper=new ObjectMapper();
        StringBuilder builder=new StringBuilder();
        
        Iterator<String> itr =  request.getFileNames();
        
        String fileType = "";
        
        List<MultipartFile> multiFileList = new ArrayList<MultipartFile>();
        //firstly validate that the file is jpeg or png
        while (itr.hasNext()) {
            MultipartFile multiFile = request.getFile(itr.next());
            
            fileType = multiFile.getContentType().split("/")[1];
            if (!fileType.equals("jpg") && !fileType.equals("jpeg") && !fileType.equals("png")) {
                response.setSuccess(false);
                response.setMessage("error");
                response.setData("Your images data type is wrong. Only jpg and png types are valid");
                return ow.writeValueAsString(response);
            } else {
                multiFileList.add(multiFile);
            }
        }
       
        try {
            //if data validate correctly, create the correnspoding image objects:
            List<Images> imagesList = new ArrayList<Images>();
            for (MultipartFile multipartFile : multiFileList) {
                Images newImage = new Images();

                newImage.setName(multipartFile.getOriginalFilename());
                newImage.setImage(multipartFile.getBytes());
                newImage.setFileType(fileType);

                Place currentPlace = placeService.findMeasurementPlaceByID(placeId);
                newImage.setPlaceID(currentPlace);
                
                //get the images list (if exist) for the current place
                List<Images> placeImages = imageService.findImagesByPlace(currentPlace);
                
                if (placeImages.isEmpty()) {
                    placeImages = new ArrayList<Images>();
                }
                
                //update the values into the list
                placeImages.add(newImage);
                currentPlace.setImagesList(imagesList);
                
                //then save object in datbase
                imageService.registerImage(newImage);
                
                //and update the place object in database
                placeService.registetNewPlace(currentPlace);
            }
        } catch (Exception e) {
            logger.error("An error occured while save an image: ", e);
            response.setSuccess(false);
            response.setMessage("warning");
            response.setData(e.getMessage());
            return builder.toString();
        }
        
        response.setSuccess(true);
        response.setMessage("success");
        response.setData("Image(s) saved successfully");
        
        logger.info("End UsersController.handleImageUpload");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller handle the "Delete image" action.
     *
     * @param placeId
     * @param imagesIds
     * @param resp
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/measurement/place/{placeId}/deleteImage", method = RequestMethod.POST)
    public @ResponseBody String deleteImages (@PathVariable (value = "placeId") Integer placeId,
           @RequestParam (value="selections") List<Integer> imagesIds, HttpServletResponse resp) throws Exception {
        logger.info("Start UsersController.deleteImages");
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        //retrieve the images selected by the user, from database
        List<Images> selectedImages = new ArrayList<Images>();
        
        Boolean databaseTransaction = false;
        
        for (Integer imageId : imagesIds) {
           selectedImages.add(imageService.findImageById(imageId));
        }
        
        //then delete the images
        databaseTransaction = imageService.deleteImagesList(selectedImages);
        
        response.setSuccess(databaseTransaction);
        response.setMessage(databaseTransaction ? "success" : "warning");
        response.setData(databaseTransaction ? "Selected image(s) deleted" : "A server error occured");
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        logger.info("End UsersController.deleteImages");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller handles the file upload (report, data, p_data).
     * @param request
     * @param measuremenetID
     * @param fileMeasurementType
     * @param resp
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/measurement/{measuremenetId}/dataUploadFile/{fileType}", method = RequestMethod.POST)
    public @ResponseBody String handleFileUpload(MultipartHttpServletRequest request, @PathVariable (value = "measuremenetId") Integer measuremenetID,
            @PathVariable (value = "fileType") String fileMeasurementType, HttpServletResponse resp) throws Exception {
        logger.info("Start UsersController.handleFileUpload");
        
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        Iterator<String> itr =  request.getFileNames();
        
        String fileType = null;
        
        List<MultipartFile> multiFileList = null;
        
        //firstly validate that the file is pdf or doc or xls
        MultipartFile multiFile = request.getFile(itr.next());
        
        fileType = multiFile.getContentType().split("/")[1];
        
        if (!fileType.equals(FileTypeEnum.PDF.description()) && !fileType.equals(FileTypeEnum.DOC.description()) && !fileType.equals(FileTypeEnum.DOCX.description())
                && !fileType.equals(FileTypeEnum.XLS.description()) && !fileType.equals(FileTypeEnum.XLSX.description())) {
            response.setSuccess(false);
            response.setMessage("error");
            response.setData("Your file type is wrong. Only pdf, doc, docx, xlsx and xls documents are valid");
            return ow.writeValueAsString(response);
        }
        
        MeasurementData measurementData = new MeasurementData();
        
        measurementData.setFileName(multiFile.getOriginalFilename());
        measurementData.setDData(multiFile.getBytes());
        if (fileType.equals(FileTypeEnum.PDF.description())) {
            measurementData.setFileType("pdf");
        } else if (fileType.equals(FileTypeEnum.DOC.description()) || fileType.equals(FileTypeEnum.DOCX.description())) {
            measurementData.setFileType("doc");
        } else {
            measurementData.setFileType("xls");
        }
        
        
        MeasurementDataType measurementDataType = measurementsService.getMeasurementDataTypeByName(fileMeasurementType);
        measurementData.setMeasurementDataType(measurementDataType);
        
        Measurements measurement = measurementsService.findMeasurementById(measuremenetID);
        measurementData.setMesurementID(measurement);
        
        measurementsService.registerMeasurementData(measurementData);
        
        response.setSuccess(true);
        response.setMessage("success");
        response.setData("New file has been upload successfully.");
        
        logger.info("End UsersController.handleFileUpload");
        return ow.writeValueAsString(response);
    }
    
    @RequestMapping(value = "/measurement/deleteDocuments", method = RequestMethod.POST)
    public @ResponseBody String deleteDocuments (@RequestParam(value = "selectedDocs") List<Integer> selectedDocs) throws Exception {
        logger.info("Start UsersController.deleteDocuments");
        GlobalRequestWrapper response = new GlobalRequestWrapper();
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        
        //delete data from database
        measurementsService.deleteMeasurementDataSearchingByIdList(selectedDocs);
        
        response.setSuccess(Boolean.TRUE);
        response.setMessage("success");
        response.setData("Documents has been removed successfully.");
        logger.info("End UsersController.deleteDocuments");
        return ow.writeValueAsString(response);
    }
    
    /**
     * This controller fill the measurement-documents data-grid with data.
     * Data considered all documents(reports, data, p-data) linked with the 
     * current measurement.
     * 
     * @param measurementId
     * @param page
     * @param pageSize
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/measurement/{measurementId}/getMeasurementDocs", method = RequestMethod.POST)
    public @ResponseBody String getMeasurementDocuments(@PathVariable (value = "measurementId") Integer measurementId,
            @RequestParam (value="page", required = true, defaultValue = "1") int page,
            @RequestParam (value="rows", required = true, defaultValue = "5") int pageSize) throws IOException{
        logger.info("Start UsersController.getMeasurementDocuments");
        
        //set the response object
        GlobalRequestDataGridWrapper response;
        
        //create a sort instance
        Sort sort = new Sort("fileName");
        //define paging
        PageRequest paging = new PageRequest((page == 0 ? page : page - 1), pageSize, sort);
        Pageable pageable = paging;
        
        //get the data
        response = measurementsService.getMeasurementDocuments(measurementId, pageable);
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        logger.info("End UsersController.getMeasurementDocuments");
        return ow.writeValueAsString(response);
    }

    /**
     * Triggered when a user click to download a measurement document
     * @param req
     * @param response
     * @param orderID 
     */
    @RequestMapping(value = "/measurement/downloadDocument/{documentId}")
    public void downloadMeasurementDocument(HttpServletRequest req, HttpServletResponse response, 
            @PathVariable(value = "documentId") Integer orderID) {
        logger.info("End UsersController.downloadMeasurementDocument");
        //retieve the Measurement-Data object from db with the specified id
        MeasurementData measurementData = measurementsService.fingMeasurementDataById(orderID);
        
        OutputStream output = null;
        
        try {
            
            output = response.getOutputStream();
            
            final byte[] documentBytes = measurementData.getDData();
            
            response.setContentType("application/" + measurementData.getFileType());
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename=\"en" + measurementData.getFileName() + "\"");
            
            output.write(documentBytes);
            
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        
        logger.info("End UsersController.downloadMeasurementDocument");
    }
}
