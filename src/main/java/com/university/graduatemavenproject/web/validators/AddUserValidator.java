/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.web.validators;

import com.university.graduatemavenproject.model.transactional.UserConfigurationDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author wolverine
 */
@Component
public class AddUserValidator implements Validator {
    
    /**
     * 
     * @param clazz
     * @return 
     */
    @Override
    public boolean supports(Class clazz) {
        return UserConfigurationDTO.class.isAssignableFrom(clazz);
    }
    
    /**
     * 
     * @param object
     * @param errors 
     */
    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "selectedUser", "A username is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "A password is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reTypePassword", "Re type password!");
        
        UserConfigurationDTO newUser = (UserConfigurationDTO) object;
        
        if(!newUser.getNewPassword().equals("") && !newUser.getReTypePassword().equals("")) {
            if(!newUser.getNewPassword().equals(newUser.getReTypePassword())) {
                errors.rejectValue("newPassword", "Passwords do not match!");
            }
        }
    } 
    
}
