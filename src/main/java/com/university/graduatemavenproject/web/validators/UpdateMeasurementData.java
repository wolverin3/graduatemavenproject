/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.web.validators;

import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * 
 * @author Sotiris
 */
@Component
public class UpdateMeasurementData implements Validator {
    
    /**
     * 
     * @param clazz
     * @return 
     */
    @Override
    public boolean supports(Class clazz) {
        return MeasurementDTO.class.isAssignableFrom(clazz);
    }
    
    /**
     * 
     * @param object
     * @param errors 
     */
    @Override
    public void validate(Object object, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "measurementName", "Measurement Name field is mandatory!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "measurementDate", "Measurement field is madatory!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "city field is mandatory!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "providers", "Providers field is mandatory!");   
    }

}
