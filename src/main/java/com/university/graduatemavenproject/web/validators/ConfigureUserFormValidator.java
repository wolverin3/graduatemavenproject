/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.web.validators;

import com.university.graduatemavenproject.model.GlobalRequestWrapper;
import com.university.graduatemavenproject.model.transactional.UserConfigurationDTO;
import com.university.graduatemavenproject.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author sotiris
 */
@Component
public class ConfigureUserFormValidator {
    
    @Autowired
    UserService userService;
    
    
    
    /**
     * This validators method bellow checks if the old password field of the form is valid.
     * Also check if the new password field is the same with the re-type password field.
     * 
     * 
     * @param userConfiguration
     * @return 
     */
    
    public  GlobalRequestWrapper validate(UserConfigurationDTO userConfiguration) {
        
        GlobalRequestWrapper response = new  GlobalRequestWrapper();
        response.setSuccess(Boolean.TRUE);
        List<String> responseMessages = new ArrayList<String>();
        String currentPassword;
        try {
        currentPassword = userService.findUsersPasswordByUserName(userConfiguration.getSelectedUser());
        } catch (Exception e) {
            response.setSuccess(Boolean.FALSE);
            response.setMessage("warning");
            return response;
        } 
        if (userConfiguration.getNewPassword().equals("") && userConfiguration.getOldPassword().equals("") 
                && userConfiguration.getReTypePassword().equals("") && userConfiguration.getUserRole().equals("")) {
            responseMessages.add("Empty form!");
            response.setSuccess(Boolean.FALSE);
            response.setMessage("error");
            response.setData(responseMessages);
            return response;
        }
        if (!userConfiguration.getNewPassword().equals("") || !userConfiguration.getOldPassword().equals("") 
                || !userConfiguration.getReTypePassword().equals("")) {
            if (!currentPassword.equals(userConfiguration.getOldPassword())) {
                responseMessages.add("Wrong current password");
                response.setSuccess(Boolean.FALSE);
                response.setMessage("error");
            } 
            if(userConfiguration.getOldPassword().equals(userConfiguration.getNewPassword()) || userConfiguration.getOldPassword().equals(userConfiguration.getReTypePassword())) {
                responseMessages.add("New password ti's the same with the old password");
                response.setSuccess(Boolean.FALSE);
                response.setMessage("error");
            }
            if (!userConfiguration.getNewPassword().equals(userConfiguration.getReTypePassword())) {
                responseMessages.add("Passwords does not mach");
                response.setSuccess(Boolean.FALSE);
                response.setMessage("error");
            }
            if (!userConfiguration.getNewPassword().equals("") && userConfiguration.getReTypePassword().equals("")) {
                responseMessages.add("You forgot to insert new password");
                response.setSuccess(Boolean.FALSE);
                response.setMessage("error");
            }
        }
        
        if (!response.getSuccess()) {
            response.setData(responseMessages);
        }
        
        return response;
    }
    
}
