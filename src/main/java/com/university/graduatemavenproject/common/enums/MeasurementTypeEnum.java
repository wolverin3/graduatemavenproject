package com.university.graduatemavenproject.common.enums;

/**
 *
 * @author Sotiris
 */
public enum MeasurementTypeEnum {
    
    REPORT(1, "Report"),
    DATA(2, "Measurement_data"),
    PDATA(3, "Measurement_pdata");

    // The code
    private Integer code;

    // The description
    private String description;

    /**
     * Instantiates a new client role
     *
     * @param code the code
     * @param description the description
     */
    MeasurementTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int value() {
        return code;
    }

    /**
     * Gets the long code.
     *
     * @return the long code
     */
    public Long getLongCode() {
        return code.longValue();
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String description() {
        return description;
    }
    
}
