/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.common.enums;

/**
 *
 * @author Sotiris
 */
public enum FileTypeEnum {
    PDF(1, "pdf"),
    DOC(2, "msword"),
    DOCX(3, "vnd.openxmlformats-officedocument.wordprocessingml.document"),
    XLS(4, "vnd.ms-excel"),
    XLSX(5, "vnd.openxmlformats-officedocument.spreadsheetml.sheet");

    // The code
    private Integer code;

    // The description
    private String description;

    /**
     * Instantiates a new client role
     *
     * @param code the code
     * @param description the description
     */
    FileTypeEnum(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public int value() {
        return code;
    }

    /**
     * Gets the long code.
     *
     * @return the long code
     */
    public Long getLongCode() {
        return code.longValue();
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String description() {
        return description;
    }
}
