/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.model.transactional;

import com.university.graduatemavenproject.db.entities.Users;
import java.util.List;

/**
 *
 * @author wolverine
 */
public class MeasurementDTO {
    
    private Integer measuremenetID;
    
    private String measurementName;
    
    private String dateCreated;
    
    private String dateCreatedFrom;
    
    private String dateCreatedTo;
    
    private String measurementDate;
    
    private String measurementDateFrom;
    
    private String measurementDateTo;
    
    private String city;
    
    private String providers;
    
    private String locationName;
    
    private Float coordinatesX;
    
    private Float coordinatesY;
    
    private String correlatedUser;
    
    private Boolean userRole;
    
    private List<String> usersIDs;
    
    private List<String> usersRights;
    
    private Users currentUser;
    
    private Boolean search;
    
    //Constructor
    public MeasurementDTO() {
        this.search = false;
    }
    
    //getters and setters
    public Integer getMeasuremenetID() {
        return measuremenetID;
    }

    public void setMeasuremenetID(Integer measuremenetID) {
        this.measuremenetID = measuremenetID;
    }

    public String getMeasurementName() {
        return measurementName;
    }

    public void setMeasurementName(String measurementName) {
        this.measurementName = measurementName;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateCreatedFrom() {
        return dateCreatedFrom;
    }

    public void setDateCreatedFrom(String dateCreatedFrom) {
        this.dateCreatedFrom = dateCreatedFrom;
    }

    public String getDateCreatedTo() {
        return dateCreatedTo;
    }

    public void setDateCreatedTo(String dateCreatedTo) {
        this.dateCreatedTo = dateCreatedTo;
    }

    public String getMeasurementDate() {
        return measurementDate;
    }

    public void setMeasurementDate(String measurementDate) {
        this.measurementDate = measurementDate;
    }

    public String getMeasurementDateFrom() {
        return measurementDateFrom;
    }

    public void setMeasurementDateFrom(String measurementDateFrom) {
        this.measurementDateFrom = measurementDateFrom;
    }

    public String getMeasurementDateTo() {
        return measurementDateTo;
    }

    public void setMeasurementDateTo(String measurementDateTo) {
        this.measurementDateTo = measurementDateTo;
    }
    
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProviders() {
        return providers;
    }

    public void setProviders(String providers) {
        this.providers = providers;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Float getCoordinatesX() {
        return coordinatesX;
    }

    public void setCoordinatesX(Float coordinatesX) {
        this.coordinatesX = coordinatesX;
    }

    public Float getCoordinatesY() {
        return coordinatesY;
    }

    public void setCoordinatesY(Float coordinatesY) {
        this.coordinatesY = coordinatesY;
    }

    public String getCorrelatedUser() {
        return correlatedUser;
    }

    public void setCorrelatedUser(String correlatedUser) {
        this.correlatedUser = correlatedUser;
    }

    public Boolean isUserRole() {
        return userRole;
    }

    public void setUserRole(Boolean userRole) {
        this.userRole = userRole;
    }

    public List<String> getUsersIDs() {
        return usersIDs;
    }

    public void setUsersIDs(List<String> usersIDs) {
        this.usersIDs = usersIDs;
    }

    public List<String> getUsersRights() {
        return usersRights;
    }

    public void setUsersRights(List<String> usersRights) {
        this.usersRights = usersRights;
    }

    public Users getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Users currentUser) {
        this.currentUser = currentUser;
    }

    public Boolean isSearch() {
        return search;
    }

    public void setSearch(Boolean search) {
        this.search = search;
    }

}
