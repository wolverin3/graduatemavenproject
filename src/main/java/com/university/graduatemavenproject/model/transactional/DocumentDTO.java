/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.model.transactional;

/**
 *
 * @author sotiris
 */
public class DocumentDTO {
    
    private long id;
    
    private String name;
    
    //represent if the file is doc or pdf or xls
    private String fileType;
    
    //represent if the file is report or data or p-data
    private String documentType;

    public DocumentDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    
}
