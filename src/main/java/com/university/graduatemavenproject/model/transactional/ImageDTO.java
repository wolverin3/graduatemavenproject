/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.model.transactional;

import java.util.List;

/**
 * 
 * @author Sotiris
 */
public class ImageDTO {
    
    private Integer id;
    
    private String imageName;
    
    private String imageType;
    
    private List<Integer> selectedImages;

    public ImageDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public List<Integer> getSelectedImages() {
        return selectedImages;
    }

    public void setSelectedImages(List<Integer> selectedImages) {
        this.selectedImages = selectedImages;
    }
    
}
