/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.model.transactional;

/**
 *
 * @author wolverine
 */
public class UsersDTO {
    
    private String userName;
     
    private String role;
     
    private String enabled;
    
    private Boolean search;
    
    private Boolean hasRightsOnMeasurement;
    
    //Constructor
    public UsersDTO() {
        this.search = false;
    }
    
    //Getters and Setters
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public Boolean isSearch() {
        return search;
    }

    public void setSearch(Boolean search) {
        this.search = search;
    }

    public Boolean getHasRightsOnMeasurement() {
        return hasRightsOnMeasurement;
    }

    public void setHasRightsOnMeasurement(Boolean hasRightsOnMeasurement) {
        this.hasRightsOnMeasurement = hasRightsOnMeasurement;
    }
    
    
}
