/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.model.transactional;

/**
 * 
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class PlaceDTO {
    private Integer placeId;
    
    private String placeName;
    
    private Float coordinatesX;
    
    private Float coordinatesY;

    public PlaceDTO() {
    }

    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Float getCoordinatesX() {
        return coordinatesX;
    }

    public void setCoordinatesX(Float coordinatesX) {
        this.coordinatesX = coordinatesX;
    }

    public Float getCoordinatesY() {
        return coordinatesY;
    }

    public void setCoordinatesY(Float coordinatesY) {
        this.coordinatesY = coordinatesY;
    }
    
    
}
