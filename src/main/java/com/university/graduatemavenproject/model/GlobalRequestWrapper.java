package com.university.graduatemavenproject.model;

import java.io.Serializable;


/**
 * This class is used as a wrapper for any http response.
 * It is used in ajax requests to standardize ajax communication and application asynchronous error handling and display
 * it is used aslo on REST services to encapsulate the response of a called web service.
 *
 */

public class GlobalRequestWrapper<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Boolean success;		
	private String message;	
	private T data;
		
//	public Boolean isSuccess() {
//		return success;
//	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public Boolean getSuccess(){
		return success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}	
	
	@Override
	public String toString() {
		return "GlobalErrorWrapper [" 
				+ "success=" + success 
				+ ", message=" + message
				+ ", data=" + data 
				+ "]";
	}

}
