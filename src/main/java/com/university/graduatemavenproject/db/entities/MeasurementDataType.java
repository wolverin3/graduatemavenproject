/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Sotiris
 */
@Entity
@Table(name = "measurement_data_type")
@NamedQuery(name = "MeasurementDataType.findAll", query = "SELECT m FROM MeasurementDataType m")
public class MeasurementDataType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Name")
    private String name;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "measurementDataType")
    private List<MeasurementData> mesurementDataList;

    public MeasurementDataType() {
    }

    public MeasurementDataType(Integer id) {
        this.id = id;
    }

    public MeasurementDataType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MeasurementData> getMesurementDataList() {
        return mesurementDataList;
    }

    public void setMesurementDataList(List<MeasurementData> mesurementDataList) {
        this.mesurementDataList = mesurementDataList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MeasurementDataType)) {
            return false;
        }
        MeasurementDataType other = (MeasurementDataType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.university.graduatemavenproject.db.entities.MeasurementDataType[ id=" + id + " ]";
    }

}
