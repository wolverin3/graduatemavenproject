/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sotiris
 */
@Entity
@Table(name = "measurements")
public class Measurements implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Name")
    private String name;
    
    @Column(name = "DateCreated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date_Messurement_Be")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMessurementBe;
    
    @Size(max = 45)
    @Column(name = "City")
    private String city;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Providers")
    private String providers;
    
    @OneToMany(mappedBy = "measurement")
    private List<Place> placeList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mesurementID")
    private List<MeasurementData> mesurementDataList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "measurement")
    private List<UserMeasurements> userMeasurementsList;

    public Measurements() {
    }

    public Measurements(Integer id) {
        this.id = id;
    }

    public Measurements(Integer id, String name, Date dateMessurementBe, String providers) {
        this.id = id;
        this.name = name;
        this.dateMessurementBe = dateMessurementBe;
        this.providers = providers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateMessurementBe() {
        return dateMessurementBe;
    }

    public void setDateMessurementBe(Date dateMessurementBe) {
        this.dateMessurementBe = dateMessurementBe;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProviders() {
        return providers;
    }

    public void setProviders(String providers) {
        this.providers = providers;
    }

    public List<UserMeasurements> getUserMeasurementsList() {
        return userMeasurementsList;
    }

    public void setUserMeasurementsList(List<UserMeasurements> userMeasurementsList) {
        this.userMeasurementsList = userMeasurementsList;
    }

    public List<Place> getPlaceList() {
        return placeList;
    }

    public void setPlaceList(List<Place> placeList) {
        this.placeList = placeList;
    }
    
    
    public List<MeasurementData> getMesurementDataList() {
        return mesurementDataList;
    }

    public void setMesurementDataList(List<MeasurementData> mesurementDataList) {
        this.mesurementDataList = mesurementDataList;
    }

    @Override
    public String toString() {
        return "com.university.graduatemavenproject.db.Measurements[ id=" + id + " ]";
    }
    
}
