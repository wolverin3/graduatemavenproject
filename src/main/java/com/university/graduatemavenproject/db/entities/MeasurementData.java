/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.db.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sotiris
 */
@Entity
@Table(name = "measurement_data")
@NamedQueries({
    @NamedQuery(name = "MeasurementData.findAll", query = "SELECT m FROM MeasurementData m"),
    @NamedQuery(name = "MeasurementData.findById", query = "SELECT m FROM MeasurementData m WHERE m.id = :id"),
    @NamedQuery(name = "MeasurementData.findByFileName", query = "SELECT m FROM MeasurementData m WHERE m.fileName = :fileName"),
    @NamedQuery(name = "MeasurementData.findByFileType", query = "SELECT m FROM MeasurementData m WHERE m.fileType = :fileType")})
public class MeasurementData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    
    @Size(max = 40)
    @Column(name = "FileName")
    private String fileName;
    
    @Size(max = 10)
    @Column(name = "FileType")
    private String fileType;
    
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "dData")
    private byte[] dData;
    
    @JoinColumn(name = "MeasurementDataType", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private MeasurementDataType measurementDataType;
    
    @JoinColumn(name = "MesurementID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Measurements mesurementID;

    public MeasurementData() {
    }

    public MeasurementData(Integer id) {
        this.id = id;
    }

    public MeasurementData(Integer id, byte[] dData) {
        this.id = id;
        this.dData = dData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Measurements getMesurementID() {
        return mesurementID;
    }

    public void setMesurementID(Measurements mesurementID) {
        this.mesurementID = mesurementID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MeasurementData)) {
            return false;
        }
        MeasurementData other = (MeasurementData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.university.graduatemavenproject.db.MesurementData[ id=" + id + " ]";
    }


    public MeasurementDataType getMeasurementDataType() {
        return measurementDataType;
    }

    public void setMeasurementDataType(MeasurementDataType measurementDataType) {
        this.measurementDataType = measurementDataType;
    }

    public byte[] getDData() {
        return dData;
    }

    public void setDData(byte[] dData) {
        this.dData = dData;
    }
    
}
