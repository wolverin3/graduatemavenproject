/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author wolverine
 */
@Entity
@Table(name = "place")
@NamedQuery(name = "Place.findAll", query = "SELECT p FROM Place p")
public class Place implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CoordinatesX")
    private BigDecimal coordinatesX;
    
    @Column(name = "CoordinatesY")
    private BigDecimal coordinatesY;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "placeID", fetch = FetchType.LAZY)
    private List<Images> imagesList;
    
    @JoinColumn(name = "MeasurementId", referencedColumnName = "ID")
    @ManyToOne
    private Measurements measurement;

    public Place() {
    }

    public Place(Integer id) {
        this.id = id;
    }

    public Place(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCoordinatesX() {
        return coordinatesX;
    }

    public void setCoordinatesX(BigDecimal coordinatesX) {
        this.coordinatesX = coordinatesX;
    }

    public BigDecimal getCoordinatesY() {
        return coordinatesY;
    }

    public void setCoordinatesY(BigDecimal coordinatesY) {
        this.coordinatesY = coordinatesY;
    }

    public List<Images> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<Images> imagesList) {
        this.imagesList = imagesList;
    }
    
    public Measurements getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurements measurement) {
        this.measurement = measurement;
    }

    @Override
    public String toString() {
        return "com.university.graduatemavenproject.db.Place[ id=" + id + " ]";
    }
    
}
