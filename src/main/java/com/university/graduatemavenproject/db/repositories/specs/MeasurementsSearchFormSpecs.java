/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.repositories.specs;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Measurements_;
import com.university.graduatemavenproject.db.entities.Place;
import com.university.graduatemavenproject.db.entities.Place_;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements_;
import com.university.graduatemavenproject.model.transactional.MeasurementDTO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author wolverine
 */
public class MeasurementsSearchFormSpecs {
    
    private final Logger logger = LoggerFactory.getLogger(MeasurementsSearchFormSpecs.class);
    
    public static Specification<Measurements> searchMeasurements(final MeasurementDTO searchMeasurementData) {
        return new Specification<Measurements>() {
            @Override
            public Predicate toPredicate(Root<Measurements> root,  CriteriaQuery<?> query, CriteriaBuilder cb) {
                
                Predicate predicatesQuery = cb.conjunction();
                
                //a date formatter is needed
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date searchCreationDateFrom = new Date();
                Date searchCreationDateTo = new Date();
                try {
                    searchCreationDateFrom = searchMeasurementData.getDateCreatedFrom().equals("") ? searchCreationDateFrom : formatter.parse(searchMeasurementData.getDateCreatedFrom());
                    searchCreationDateTo = searchMeasurementData.getDateCreatedTo().equals("") ? searchCreationDateTo : formatter.parse(searchMeasurementData.getDateCreatedTo());
                } catch (ParseException ex) {
                    java.util.logging.Logger.getLogger(MeasurementsSearchFormSpecs.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                //current User
                //if current users attribute is not null (not an admin user) then
                if(searchMeasurementData.getCurrentUser() != null) {
                    Join<Measurements, UserMeasurements> currentUserMeasurements = root.join(Measurements_.userMeasurementsList);
                    predicatesQuery.getExpressions().add(cb.equal(currentUserMeasurements.get(UserMeasurements_.user), searchMeasurementData.getCurrentUser()));
                }
                
                //measurement name
                if(!searchMeasurementData.getMeasurementName().equals("")) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.get(Measurements_.name)), "%" + searchMeasurementData.getMeasurementName().toLowerCase() + "%"));
                }
                
                //city measurement took place
                if(!searchMeasurementData.getCity().equals("")) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.get(Measurements_.city)), "%" + searchMeasurementData.getCity().toLowerCase() + "%"));
                }
                
                //location measurement took place
                if(!searchMeasurementData.getLocationName().equals("")) {
                    //if it is not empty then join the Place entity
                    Join<Measurements, Place> place = root.join(Measurements_.placeList);
                    //and construct the predicate question
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(place.get(Place_.name)), "%" + searchMeasurementData.getLocationName().toLowerCase() + "%"));
                }
                
                //providers
                if(!searchMeasurementData.getProviders().equals("")) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.get(Measurements_.providers)), "%" + searchMeasurementData.getProviders().toLowerCase() + "%"));
                }
                
                //date creation
                try {
                    //if there are any values in creation date fields then
                    if(!searchMeasurementData.getDateCreatedFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getDateCreatedFrom()) ||
                            !searchMeasurementData.getDateCreatedTo().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getDateCreatedTo())) {
                        
                        //if creation date from and creation date to fields are fulfilled then
                        if(!searchMeasurementData.getDateCreatedFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getDateCreatedFrom()) &&
                            !searchMeasurementData.getDateCreatedTo().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getDateCreatedTo())) {
                            
                            
                            predicatesQuery.getExpressions().add(cb.between(root.get(Measurements_.dateCreated), searchCreationDateFrom, searchCreationDateTo));
                        
                            //else if only the creation date from field has value 
                        } else if(!searchMeasurementData.getDateCreatedFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getDateCreatedFrom()) &&
                            searchMeasurementData.getDateCreatedTo().isEmpty() && !StringUtils.isNotBlank(searchMeasurementData.getDateCreatedTo())) {
                            
                            //Date searchCreationDateFrom = formatter.parse(searchMeasurementData.getDateCreatedFrom());
                            predicatesQuery.getExpressions().add(cb.greaterThanOrEqualTo(root.get(Measurements_.dateCreated), searchCreationDateFrom));
                            
                            //else
                        } else {
                            
                            //Date searchCreationDateTo = formatter.parse(searchMeasurementData.getDateCreatedTo());
                            predicatesQuery.getExpressions().add(cb.lessThanOrEqualTo(root.get(Measurements_.dateCreated), searchCreationDateTo));
                        }

                    }
                    
                    //if there are any values in date measurement be fields then
                    if(!searchMeasurementData.getMeasurementDateFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateFrom()) ||
                            !searchMeasurementData.getMeasurementDateTo().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateTo())) {
                        
                        //if measurement date from and measurement date to fields are fulfilled then
                        if(!searchMeasurementData.getMeasurementDateFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateFrom()) &&
                            !searchMeasurementData.getMeasurementDateTo().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateTo())) {
                            
                            Date searchMeasurementDateFrom = formatter.parse(searchMeasurementData.getMeasurementDateFrom());
                            Date searchMeasurementDateTo = formatter.parse(searchMeasurementData.getMeasurementDateTo());
                            predicatesQuery.getExpressions().add(cb.between(root.get(Measurements_.dateMessurementBe), searchMeasurementDateFrom, searchMeasurementDateTo));
                        
                            //else if only the measurement date from field has value 
                        } else if(!searchMeasurementData.getMeasurementDateFrom().isEmpty() && StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateFrom()) &&
                            searchMeasurementData.getMeasurementDateTo().isEmpty() && !StringUtils.isNotBlank(searchMeasurementData.getMeasurementDateTo())) {
                            
                            Date searchMeasurementDateFrom = formatter.parse(searchMeasurementData.getMeasurementDateFrom());
                            predicatesQuery.getExpressions().add(cb.greaterThanOrEqualTo(root.get(Measurements_.dateMessurementBe), searchMeasurementDateFrom));
                            
                            //else
                        } else {
                            
                            Date searchMeasurementDateTo = formatter.parse(searchMeasurementData.getMeasurementDateTo());
                            predicatesQuery.getExpressions().add(cb.lessThanOrEqualTo(root.get(Measurements_.dateMessurementBe), searchMeasurementDateTo));
                        }

                    }
                    
                } catch (ParseException e) {
                    System.out.println("Date formatter exception: " + e);
                }
                
                
                return predicatesQuery;
            }
        };
    }
    
}
