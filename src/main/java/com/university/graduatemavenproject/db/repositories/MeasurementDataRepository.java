package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.MeasurementData;
import com.university.graduatemavenproject.db.entities.Measurements;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sotiris
 */
@Repository
public interface MeasurementDataRepository extends JpaRepository<MeasurementData, Integer> {
    
    /**
     * Searching measurement data by measurement and retrieve a page object
     * @param measurement
     * @param pageable
     * @return 
     */
    public Page<MeasurementData> findByMesurementID(Measurements measurement, Pageable pageable);
    
    /**
     * Delete all MeasurementData which their ids equal with the ids contained
     * into the list
     * 
     * @param idList 
     */
    @Modifying
    @Transactional
    @Query(value = "delete from MeasurementData md where md.id in ?1")
    public void deleteFromMeasurementDataByIdList(List<Integer> idList);
    
}
