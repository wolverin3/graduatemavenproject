/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.repositories;


import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.Users;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wolverine
 */
@Repository
public interface UserMeasurementsRepository extends JpaRepository<UserMeasurements, Integer> {
    
    /**
     * This function finds all UserMeasurements object and get as parameter the user
     * @param users
     * @param pageable
     * @return 
     */
    Page<UserMeasurements> findByUser(Users users, Pageable pageable);
    
    /**
     * 
     * @param user
     * @return 
     */
    List<UserMeasurements> findByUser(Users user);
    
    /**
     * Retrieve a list of UserMeasurements, contain all the userMeasurements,
     * which enclose the measurement given as element
     * @param measurement
     * @return 
     */
    List<UserMeasurements> findByMeasurement(Measurements measurement);
    
    /**
     * This method retrieve a UserMeasurements searching by the User and 
     * Measurement
     * 
     * @param user
     * @param measurement
     * @return 
     */
    UserMeasurements findByUserAndMeasurement(Users user, Measurements measurement);
    
    
    @Query(value = "select um.hasAuth from UserMeasurements um where um.user = ?1 and um.measurement = ?2")
    Boolean getUserRightsOnMeasurement(Users user, Measurements measurement);
    
}