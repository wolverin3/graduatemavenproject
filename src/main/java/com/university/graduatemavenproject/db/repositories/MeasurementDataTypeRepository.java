/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.MeasurementData;
import com.university.graduatemavenproject.db.entities.MeasurementDataType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sotiris
 */
@Repository
public interface MeasurementDataTypeRepository extends JpaRepository<MeasurementDataType, Integer> {
    
    /**
     * Find a measurement data type searching by name
     * 
     * @param name
     * @return 
     */
    MeasurementDataType findByName(String name);
    
    /**
     * Get a MeasurementData, MeasurementDataType from database
     * 
     * @param measurementData
     * @return 
     */
    @Query(value = "from MeasurementDataType m where ?1 in m.mesurementDataList")
    MeasurementDataType findmesurementDataTypeByMeasurementData(MeasurementData measurementData);
}
