/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wolverine
 */
@Repository
public interface PlaceRepository extends JpaRepository< Place, Integer> {

    /**
     * This function find a place searching by name
     *
     * @param name
     * @return
     */
    Place findByName(String name);

    /**
     * This method return all Places linked with the same measurement.
     *
     * @param measurement
     * @return
     */
    List<Place> findByMeasurement(Measurements measurement);

    /**
     * This method return a specific number of Places linked with the same
     * measurement.
     *
     * @param measurement
     * @param pageable
     * @return
     */
    Page<Place> findByMeasurement(Measurements measurement, Pageable pageable);

}
