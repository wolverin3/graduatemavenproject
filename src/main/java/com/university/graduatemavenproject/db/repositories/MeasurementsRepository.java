/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.Place;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wolverine
 */
@Repository
public interface MeasurementsRepository extends JpaRepository<Measurements, Integer>, JpaSpecificationExecutor {
    
    /**
     * This function return measurement's place id searching by measurement id
     * @param measurementId
     * @return 
     */
    @Query(value = "select Location. from measurements where ID = ?1", nativeQuery = true)
    Integer getMeasurementPlaceIDByMeasurementID(Integer measurementId); 
    
}
