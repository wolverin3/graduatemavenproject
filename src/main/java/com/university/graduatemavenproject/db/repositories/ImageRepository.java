/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.Images;
import com.university.graduatemavenproject.db.entities.Place;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sotiris
 */
@Repository
public interface ImageRepository extends JpaRepository<Images, Integer>, JpaSpecificationExecutor{
    
    /**
     * Retrieve all images linked with a specific place
     * 
     * @param place
     * @return 
     */
    List<Images> findByPlaceID(Place place);
    
    /**
     * Retrieve all images ids linked with a specific place
     * 
     * @param place
     * @return 
     */
    @Query(value = "select im.id from Images im where im.placeID = ?1")
    List<Integer> findImagesIdsByPlace(Place place);
    
    /**
     * Retrieve all images Names linked with a specific place
     * 
     * @param place
     * @return 
     */
    @Query(value = "select im.name from Images im where im.placeID = ?1")
    List<String> findImagesNameByPlace(Place place);
    
}
