/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.university.graduatemavenproject.db.repositories;

import com.university.graduatemavenproject.db.entities.Users;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wolverine
 */
@Repository
public interface UsersRepository extends JpaRepository<Users, String>, JpaSpecificationExecutor{
    
    /**
     * Return all usernames from database
     * @return 
     */
    @Query(value="select u.userName from Users u")
    List<String> findAllUserNames();
    
    /**
     * This function return users role value
     * @param userName
     * @return 
     */
    @Query(value="select u.role from Users u where u.userName = ?1")
    String findUsersRole(String userName);
    
    /**
     * Return user's password
     * @param userName
     * @return 
     */
    @Query(value="select u.passwd from Users u where u.userName = ?1")
    String findUsersPassword(String userName);
    
    /**
     * This function return all users with role equals to the given 
     * @param role
     * @return 
     */
    List<Users> findByRole(String role);
    
    /**
     * Return all users except current
     * @param userName
     * @param pageable
     * @return 
     */
    Page<Users> findByUserNameNot(String userName, Pageable pageable);
    
    
    /**
     * This function return all users, except from current logged-in user, 
     * with role equals to the given
     * @param role
     * @param enabled
     * @param userName
     * @param pageable
     * @return 
     */
    Page<Users> findByRoleAndEnabledAndUserNameNot(String role, boolean enabled, String userName, Pageable pageable);
    
    /**
     * This function return all users, except from current logged-in user, 
     * with role equals to the given and their username match with 
     * searchData's value
     * @param role
     * @param enabled
     * @param userName
     * @param searchData
     * @param pageable
     * @return 
     */
    Page<Users> findByRoleAndEnabledAndUserNameNotAndUserNameLike(String role, boolean enabled, String userName, String searchData, Pageable pageable);
    
}
