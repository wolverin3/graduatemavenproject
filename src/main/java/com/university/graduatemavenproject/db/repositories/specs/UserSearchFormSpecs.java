/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.university.graduatemavenproject.db.repositories.specs;

import com.university.graduatemavenproject.db.entities.Measurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements;
import com.university.graduatemavenproject.db.entities.UserMeasurements_;
import com.university.graduatemavenproject.db.entities.Users;
import com.university.graduatemavenproject.db.entities.Users_;
import com.university.graduatemavenproject.model.transactional.UsersDTO;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author Sotiris
 */
public class UserSearchFormSpecs {

    private final Logger logger = LoggerFactory.getLogger(UserSearchFormSpecs.class);

    /**
     *This specification method create the query to search users by their username,
     * role and if they are enabled or disabled
     * 
     * @param searchData
     * @return
     */
    public static Specification<Users> searchUser(final UsersDTO searchData) {
        return new Specification<Users>() {
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate predicatesQuery = cb.conjunction();

                //username
                if (!searchData.getUserName().isEmpty()) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.get(Users_.userName)), "%" + searchData.getUserName().toLowerCase() + "%"));
                }

                if (!(searchData.getRole().equals(""))) {
                    predicatesQuery.getExpressions().add(cb.equal(root.get(Users_.role), searchData.getRole()));
                }

                if (!(searchData.getEnabled().equals(""))) {
                    if (searchData.getEnabled().equals("Enabled")) {
                        predicatesQuery.getExpressions().add(cb.isTrue(root.get(Users_.enabled)));
                    } else if (searchData.getEnabled().equals("Disabled")) {
                        predicatesQuery.getExpressions().add(cb.isFalse(root.get(Users_.enabled)));
                    } else {
                    }
                }
                return predicatesQuery;
            }
        };
    }

    /**
     * This specifications method search for all users that this username match
     * with the searchData's value and are not admins or is not the current user
     *
     * @param currentUser
     * @param searchData
     * @return
     */
    public static Specification<Users> searchNewMeasurementUsers(final String currentUser, final String searchData) {
        return new Specification<Users>() {
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate predicatesQuery = cb.conjunction();

                if (searchData != null && !searchData.isEmpty()) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.get(Users_.userName)), "%" + searchData.toLowerCase() + "%"));
                }

                predicatesQuery.getExpressions().add(cb.notEqual(cb.lower(root.get(Users_.userName)), "%" + currentUser.toLowerCase() + "%"));

                predicatesQuery.getExpressions().add(cb.isTrue(root.get(Users_.enabled)));

                predicatesQuery.getExpressions().add(cb.equal(root.get(Users_.role), "ROLE_USER"));

                return predicatesQuery;
            }

        };
    }

    /**
     * This method create the specifications for database to return all users
     * linked with the given measurement
     *
     * @param currentMeasurement
     * @return
     */
    public static Specification<Users> getMeasurementUsers(final Measurements currentMeasurement) {
        return new Specification<Users>() {
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate predicatesQuery = cb.conjunction();

                //select enabled users only
                predicatesQuery.getExpressions().add(cb.isTrue(root.get(Users_.enabled)));

                //join UserMeasurements
                Join<Users, UserMeasurements> userMeasurements = root.join(Users_.userMeasurementsList);

                predicatesQuery.getExpressions().add(cb.equal(userMeasurements.get(UserMeasurements_.measurement), currentMeasurement));

                return predicatesQuery;
            }
        };
    }

    /**
     * This specifications create the appropriate query for the system to get
     * all users with role user, are enabled and who are not already linked with
     * the given measurement
     *
     * @param userMeasurements
     * @param searchData
     * @return
     */
    public static Specification<Users> getNotRegisteredMeasurementUsers(final List<UserMeasurements> userMeasurements, final String searchData) {
        return new Specification<Users>() {
            @Override
            public Predicate toPredicate(Root<Users> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Predicate predicatesQuery = cb.conjunction();

                if (searchData != null && !searchData.isEmpty()) {
                    predicatesQuery.getExpressions().add(cb.like(cb.lower(root.<String>get(Users_.userName)), "%" + searchData.toLowerCase() + "%"));
                } else {
                    for (UserMeasurements userMeasurement : userMeasurements) {
                        predicatesQuery.getExpressions().add(cb.isNotMember(userMeasurement, root.get(Users_.userMeasurementsList)));
                    }
                }

                predicatesQuery.getExpressions().add(cb.isTrue(root.get(Users_.enabled)));

                predicatesQuery.getExpressions().add(cb.equal(root.get(Users_.role), "ROLE_USER"));

                return predicatesQuery;
            }
        };
    }
}
